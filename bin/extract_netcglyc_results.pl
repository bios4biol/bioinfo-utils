#!/usr/bin/perl -w

=pod

=head1 NAME

extract_netcglyc_results.pl
 
=head1 SYNOPSIS

extract_netcglyc_results.pl -arg1 <> -arg2 <> -arg3 <> -arg4 <>

=head1 OPTIONS

  --infile string, the path to the protein sequence query file - !!!the sequence name should be the short name!!!
  --encodingfile string, the path of the file containing the encoded protein names (long name "..._allele1" <-> short name "_seq")
  --outputfile string, the path of the output file  for parsed results
  --delta string, value between 0 and 1. Discriminant for the score comparison. (0.5 per default)

=head1 DESCRIPTION

extract_netcglyc_results.pl - This program is part of a pipeline of programs for SNP annotation. It uses a program called netcglyc and predicts C-mannosylation sites in mammalian protein on amino acid: http://www.cbs.dtu.dk/services/NetCGlyc/output.php
							if the 2 alleles of a protein show a different signal then there is loss or gain of signal.
							if allele1 has a prediction and allele2 doesn't, then there is loss of signal.
							if allele2 has a prediction and allele1 doesn't, then there is gain of signal.
							Moreover, the delta of the score between allele1 and 2 is measured. If the delta is > to the delta set by
							the user, then loss? or gain? are retrieved.

=head1 DATE

20/02/2012

=head1 AUTHORS

Sabrina Rodriguez
Johann Beghain

=cut

use strict;

# find the absolute path to the local library
use FindBin;
# return the absolute path to the local library
use lib "$FindBin::RealBin/../lib";
#~ use lib '/usr/local/bioinfo/src/ergatisdev/current/bin/AnnotationPipelines/lib';

use Getopt::Long;
use Pod::Usage;
use formatAlleleSeq;
use runnetcglyc;

#~ perl /usr/local/bioinfo/src/ergatisdev/current/bin/AnnotationPipelines/bin/extract_netcglyc_results.pl --infile /home/sigenae/work/Sabrina/fic/list_snps_coded_seq5_3.SR_netcglyc.res --delta 0.5 --encodingfile /home/sigenae/work/Sabrina/fic/saved_names.txt --outputfile /home/sigenae/work/Sabrina/fic/ncglyc_test_results.txt

#~ perl /usr/local/bioinfo/src/ergatisdev/current/bin/AnnotationPipelines/bin/extract_netcglyc_results.pl --infile /home/sigenae/work/Sabrina/TEST_suite/output.netcglyc --delta 0.5 --encodingfile /work/sigenae/vmergatisdev/output_repository/SR_formatSNPeffect-protseq/94_default/names_encoding.txt --outputfile /home/sigenae/work/Sabrina/TEST_suite/heho.txt

#~ perl /usr/local/bioinfo/src/ergatisdev/current/bin/AnnotationPipelines/bin/extract_netcglyc_results.pl --infile /home/sigenae/work/Sabrina/horse_outputs/netcglyc_res.txt --delta 0.5 --encodingfile /home/sigenae/work/Sabrina/horse_outputs/names_encoding.txt --outputfile /home/sigenae/work/Sabrina/horse_outputs/heho.txt

############################ OPTIONS / PARAMETERS ############################

my @getopt_args = (
                    '-infile=s'  ,
                    '-encodingfile=s'  ,
                    '-outputfile=s'  ,    
                    '-delta=s'
                  );

my %options = ();

unless ( GetOptions( \%options, @getopt_args ) ) {
  usage();
}

sub usage {
  exec "pod2text $0";
  exit( 1 );
}

usage() if ( !exists $options{'infile'} );
usage() if ( !exists $options{'encodingfile'} );
usage() if ( !exists $options{'outputfile'} );
usage() if ( !exists $options{'delta'} );

############################ PROGRAM ############################

my $encodingfile = $options{'encodingfile'};
my $infile = $options{'infile'};
my $outputfile = $options{'outputfile'};
my $delta = $options{'delta'};


netcglyc_extract_results($delta,$infile,$encodingfile,$outputfile);




#! /usr/bin/perl
# $Id$

=pod

=head1 NAME

 blastSelection.pl

=head1 DESCRIPTION

 Filter the blast outputs and display Query Id, Ref hit name, Hit start,
 Hit end, Bits score, No match, Identity, Conserved).

=head1 SYNOPSIS

 blastSelection.pl ALN_FILE -f ALN_FORMAT -c MIN_CONSERVED -i MIN_IDENTICAL -hi MAX_HIT_RANK -hs MAX_HSP_RANK

=head1 ARGUMENTS

 1-    Alignment File.

=head1 OPTIONS

 -aln_format|f        File format (blast, fasta, blasttable, blastxml, 
                      erpin, sim4 ... see Bio::SearchIO) [Default : 
                      blast].
 -min_conserved|c     Minimal value of 'conserved' to display alignment
                      [Default : 0].
 -min_identical|i     Minimal value of 'identity' to display alignment
                      [Default : 0].
 -hit_rank_limit|hi   Maximum rank display for hits [Default : 1].
 -hsp_rank_limit|hs   Maximum rank display for hsp [Default : 1].

=head1 EXAMPLE

  blastSelection.pl sample1.blast -c 0.9 -i 0.9 -hi 1 -hs 2

=head1 KEYWORDS

 blast filter

=head1 DATE

 21/05/2013

=head1 VERSION

 1.1

=head1 DEPENDENCIES

 Pod::Usage
 Bio::SearchIO

=head1 AUTHORS

 Escudie Frederic - Plateforme genomique Toulouse (get-plage.bioinfo@genotoul.fr)

=cut


#############################################################################################################################
#
#		LIBRAIRIES
#
#############################################################################################################################
use strict ;
use warnings ;
use Bio::SearchIO ;
use Getopt::Long ;
use Pod::Usage ;


#############################################################################################################################
#
#		FONCTION PRINCIPALE
#
#############################################################################################################################
MAIN:
{
	##blastall -p blastn -d ralstonia.db -i QUERY -m 7

	my $file = $ARGV[0] ;
	my $aln_format = 'blast' ;
	my $min_conserved_fraction = 0 ;
	my $min_identical_fraction = 0 ;
	my $hit_rank_limit = 1 ;
	my $hsp_rank_limit = 1 ;
	my $help = 0 ;

	#Gestion des paramètres
	GetOptions(
				"aln_format|f=s"      => \$aln_format,
				"min_conserved|c=s"   => \$min_conserved_fraction,
				"min_identical|i=s"   => \$min_identical_fraction,
				"hit_rank_limit|hi=i" => \$hit_rank_limit,
				"hsp_rank_limit|hs=i" => \$hsp_rank_limit,
				"help|h"              => \$help
			  );
		
	if( $help || @ARGV != 1 )
	{
		pod2usage(
		           -sections => "SYNOPSIS|ARGUMENTS|OPTIONS|DESCRIPTION|VERSION",
		           -verbose => 99
		) ;
	}

	display_selected_hsp($file, $aln_format, $min_conserved_fraction, $min_identical_fraction, $hit_rank_limit, $hsp_rank_limit) ;
}


#############################################################################################################################
#
#		SOUS-FONCTIONS
#
#############################################################################################################################
sub display_selected_hsp
{
	my ($aln_file, $aln_format, $min_conserved_fraction, $min_identical_fraction, $hit_rank_limit, $hsp_rank_limit) = @_ ;

	#Créer un parser pour les résultats de la recherche de similarité
	my $searchin = new Bio::SearchIO(
	                                   -tempfile => 1,
	                                   -format   => $aln_format,
	                                   -file     => $aln_file ) ;
	
	#Afficher le header
	print "\#Query Id\t\#Ref hit name\t\#Hit start\t\#Hit end\t\#Bits score\t\#No match\t\#Identity\t\#Conserved\n" ;

   	#Pour chaque résultat
	while( my $result = $searchin->next_result() )
	{
		my $hit_rank = 1 ;
		my $prec_hit_bits = 0 ;

		while( my $hit = $result->next_hit() )
		{
			if( $prec_hit_bits > $hit->bits )
			{
				$hit_rank++ ;
			}

			$prec_hit_bits = $hit->bits  ;

			if( $hit_rank <= $hit_rank_limit )
			{
				#Pour chaque hsp
				my $hsp_rank = 1 ;
				my $prec_hsp_bits = 0 ;
				
				while( my $hsp = $hit->next_hsp() )
				{
					if( $prec_hsp_bits > $hsp->bits )
					{
						$hsp_rank++ ;
					}

					$prec_hsp_bits = $hsp->bits  ;

					if( $hsp_rank <= $hsp_rank_limit )
					{
						#Si le hsp correspond aux critères de similarité
						if($hsp->frac_conserved() >= $min_conserved_fraction && $hsp->frac_identical() >= $min_identical_fraction)
						{
							my $no_match = $result->query_length() - $hsp->length('query') ;

							#query Id | Ref hit name | hit start | hit end | no match | identity | conserved
							print $result->query_name()."\t".$hit->name."\t".min($hsp->start('hit'), $hsp->end('hit'))."\t".max($hsp->start('hit'), $hsp->end('hit'))."\t".$hsp->bits."\t".$no_match."\t".$hsp->frac_identical()."\t".$hsp->frac_conserved()."\n" ;
						}	
					}  
				}
			}
		}
	}
}

sub min
{
	my ($A, $B) = @_ ;
	return( $A < $B ? $A : $B  ) ;
}

sub max
{
        my ($A, $B) = @_ ;
        return( $A > $B ? $A : $B  ) ;
}


#!/usr/local/bioinfo/src/python/current/bin/python




from optparse import *
import os
import sys
import numpy as np
from pylab import *
import gzip
try:
    from Bio.Sequencing import Ace
except:
    print "Import Bio.Sequencing.Ace : Biopython is required to use ace_statistics script !"
try:
    import matplotlib.pyplot as plt
except:
    print "Import Matplot: Matplot is required to use ace_statistics script !"

__name__ = "ace_stats.py"
__synopsis__ = "ace_stats.py -i assembly.ace -o assembly_stat"
__date__ = "05/2012"
__authors__ = "Celine Noirot"
__keywords__ = "454 assembly"
__description__ = "Compute length and depth statistics and generate graphics on a ace file (zipped accepted)"
__version__ = '1.0'

"""
 AS <number of contigs> <total number of reads in ace file>
 CO <contig name> <#bases> <#reads>  <#base segments> <U or C> 
 lines of sequence data

 BQ
 lines of sequence quality data

 AF <read name> <C or U> <padded start consensus position>
 BS <start position> <end position> <read name>

 RD <read name> <# of padded bases> <# of read  info items> <# of read tags>
 QA <start> <end> <align start> <align end>

 DS CHROMAT_FILE: <name > PHD_FILE:  <name > TIME: <date/time phd file>
 WR { <tag type> <program >  <YYMMDD:HHMMSS> }
 RT{ <tag type> <program > < start>  <end> <YYMMDD:HHMMSS> }
 CT{ <contig name> <tag  type>  <program> <start> <end> <YYMMDD>  (info) }
 WA{ <tag type> <program> <YYMMDD:HHMMSS> 1 or more lines of data }
"""


if __name__ == '__main__':
    usage_msg = "Generate into output directory 5 files : \n"
    usage_msg += "\t-file.length.png \t length graph image\n"
    usage_msg += "\t-file.length.summary \t min, max, median, average length values \n"
    usage_msg += "\t-file.prof.png \t depth plot image\n"
    usage_msg += "\t-file.prof.summary \t  min, max, median, average depth values \n"
    usage_msg += "\t-file.ace.stat \t raw values\n"
    parser = OptionParser(usage="Usage: %prog -i FILE -o DIRECTORY\n"+usage_msg)
                                                              
    
    parser.add_option("-i", "--input", dest="input",
                      help="The input ace file (accept gz files)", metavar="FILE")
    parser.add_option("-o", "--output", dest="output",
                      help="The output directory\n", metavar="DIRECTORY")
    (options, args) = parser.parse_args()
    if options.input == None or options.output == None: 
        parser.print_help()
        sys.exit(1)
    
    try :
        if (not os.path.exists(options.output)) :
            os.mkdir(options.output)
        stat_file =open (os.path.join(options.output, os.path.basename(options.input) + ".stat"), 'wr')
    except:
        print "erreur dans l'ouverture du fichier "+ str(os.path.join(options.output, os.path.basename(options.input))) + ".stat \n"
        exit(1)


    try :
        if options.input.endswith(".gz"):
            acefilerecord = Ace.read(open(gzip.open(options.input), 'r'))
        else:
            acefilerecord = Ace.read(open(options.input, 'r'))
    except :
        print "erreur dans l'ouverture du fichier "+ options.input + " \n"
        exit(1)        
        
    contig_name=[]
    contig_len=[]
    prof=[]
    sum_len_read=[]
    nb_read=[]
    stat_file.write("#id_contig\tcontig_len\tnb_read\tsum_len_read\n")
    for contig in acefilerecord.contigs:
        #id_contig    contig_len    prof    sum_len_read    nb_read
        read_sum=0
        for read in contig.reads:
            read_sum=read_sum+len(read.rd.sequence)
        stat_file.write( contig.name+"\t"+str(contig.nbases)+"\t"+str(contig.nreads)+"\t"+str(read_sum)+"\n")    
        contig_name.append(contig.name)
        contig_len.append(contig.nbases)
        sum_len_read.append(read_sum)
        nb_read.append(contig.nreads)
	prof.append (read_sum/contig.nbases)
    stat_file.close
    
    #Longueur des contig    
    plt.clf()
    n, bins, patches = plt.hist(contig_len, np.max(contig_len)/10, normed=0, facecolor='blue')
    plt.xlabel('Longueur des contigs')
    plt.ylabel('Nombre de contigs')
    plt.title('Repartition des contigs selon leur taille')
    plt.grid(True)
    plt.savefig(os.path.join(options.output, os.path.basename(options.input) + ".length.png"))
        
    #profondeur des contigs
    plt.clf()
    plot(sum_len_read, contig_len, 'o')
    plt.xlabel('Somme de la longueur des lectures')
    plt.ylabel('Longueur du contig')
    plt.title('Representation de la profondeur des contigs')
    plt.grid(True)
    savefig(os.path.join(options.output, os.path.basename(options.input) + ".prof.png"))
	
    #contig = ace_gen.next()
    summary_file =open (os.path.join(options.output, os.path.basename(options.input) + ".length.summary"), 'wr')
    summary_file.write("Nombre de contig : " + str(acefilerecord.ncontigs) + "\n")
    summary_file.write("Nombre de lecture : " + str(acefilerecord.nreads ) + "\n")
    summary_file.write("Longueur minimale : " + str(np.min(contig_len)) + " pb\n")
    summary_file.write("Longueur maximale : " + str(np.max(contig_len)) + " pb\n")
    summary_file.write("Longueur mediane :"+ str(round(np.median(contig_len),2))+" pb\n")
    summary_file.write("Moyenne des longueurs : " + str(round(np.mean(contig_len),2)) + " pb\n")
    summary_file.close

    summary_file =open (os.path.join(options.output, os.path.basename(options.input) + ".prof.summary"), 'wr')
    summary_file.write("\nProfondeur minimale : " + str(np.min(prof)) + " \n")
    summary_file.write("Profondeur maximale : " + str(np.max(prof)) + " \n")
    summary_file.write("Profondeur mediane : " + str(round(np.median(prof),2)) + " \n")
    summary_file.write("Profondeur moyenne : " + str(round(np.mean(prof),2)) + " \n")
    summary_file.close
    
    
        
    

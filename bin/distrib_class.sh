#!/bin/sh

##NAME = "distrib_class.sh" 
##SYNOPSIS = "distrib_class.sh [-h] [<SIZE_OF_BIN>INT(default 10)] [<COLUMN_INDEX>INT(default 1)]" 
##DATE = "2012"
##AUTHORS = "Jean Marc Aury, Genoscope CEA. Maria Bernard"
##KEYWORDS = "compute histogram distribution on integer column"
##DESCRIPTION = "distrib_class.sh <window : int> <field number : int>"

pg=`basename $0`

usage() {
    echo USAGE :
    echo "       $pg <window : int> <field number : int> <field value : int>"
    echo "       Return a bin distribution (y axes = number of cases) from a set of values (stdin)"
    echo "         First argument is the size of the bin, 10 by default"
    echo "         Second argument is the field where to find the dataset, 1 by default"
    echo "         Third argument is the field of the value for each element, none by default"
    exit 1
}


WIN=
FIELD=
FVALUE=

while [ $1 ]
do
    case $1 in
      "-h") usage ;;
      *) if [ "$WIN" = "" ]; then WIN=$1; elif [ "$FIELD" = "" ]; then FIELD=$1; else FVALUE=$1; fi ;;
    esac 
    shift
done

gawk -v win=$WIN -v field=$FIELD -v fvalue=$FVALUE 'BEGIN { if(win=="") { win=10; } if(field == "") { field=1; } mul=1; while(win < 1) { win=win*10; mul=mul*10; }} { value = 1; if(fvalue != "") { value = $(fvalue); } offset=1; if(val < 0) { offset = -1; } val = int($field*mul); if(val%win ==0 ) { idp[val]+=value; idp2[val]++; } else { idp[int((val/win)+offset)*win]+=value; idp2[int((val/win)+offset)*win]++; } tot++; } END{ for(i in idp) { if(fvalue == "") { idp2[i]=1; } print i/mul, idp[i]/idp2[i]; } }' | sort -k1,1n

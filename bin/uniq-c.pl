#!/usr/bin/perl

use Getopt::Long;
use Pod::Usage;

my( $opt_help, $opt_man, $opt_full, $opt_admins, $opt_choose, $opt_createdb );

GetOptions(
    'help'                 =>      \$opt_help,
    'man'                  =>      \$opt_man,
)
  or pod2usage( "Try '$0 --help' for more information.");

pod2usage( -verbose => 1 ) if $opt_help;
pod2usage( -verbose => 2 ) if $opt_man;

# Main
while (<STDIN>)
  {
    chomp;
    split;
    print $_[1]."\t".$_[0]."\n";
  }

=pod

=head1 NAME

uniq-c.pl

=head1 SYNOPSIS

 uniq-c.pl < stdin

=head1 DESCRIPTION

 reads a uniq -c result as STDIN and reverses the fields

=head1 AUTHORS

Christophe Klopp

=head1 VERSION

1

=head1 DATE

02/2013

=head1 KEYWORDS

uniq

=head1 EXAMPLE

ls -ltr | cut -c52 | sort | uniq -c | perl uniq-c.pl > STDOUT

=cut


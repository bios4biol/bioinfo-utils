#!/usr/bin/perl
#
# Add or replace a readgroup (ID/library/sample/platform/platform unit/center)
#
# Kim Brugger (22 Jul 2010), contact: kim.brugger@easih.ac.uk
# modified by Anis Djari (16 Nov 2011), contact:anis.djari@toulouse.inra.fr

use strict;
use warnings;
use Data::Dumper;
use Getopt::Std;
use File::Temp qw/ tempfile /;


my %opts;
getopts('i:o:r:u:s:l:p:c:a:A:R:', \%opts);
usage() if ( $opts{h});

my $infile    = $opts{i};
my $outfile   = $opts{o};
my $readgroup = $opts{r} || usage();
my $sample    = $opts{s} || usage();
my $library   = $opts{l} || $readgroup;
my $platform  = $opts{p} || usage();
my $center    = $opts{c} || "";

my $aligner   = $opts{a};
my $a_line    = $opts{A};

my $replace   = $opts{R};

## add Platform Unit management
my $unit      = $opts{u} || "";

$infile = $replace if ( ! $infile && $replace );
open (*STDIN, $infile) || die "Could not open '$infile': $!\n" if ( $infile );

my ($tmp_fh, $tmp_file);
if ( $replace) {
  ($tmp_fh, $tmp_file) =  tempfile( DIR => './');
  *STDOUT = *$tmp_fh;
} elsif ( $outfile ) {
  open (*STDOUT, " > $outfile " ) || die "Could not open '$outfile': $!\n" ;
}


my ($written_readgroup, $written_cmd_flags) = (0, 0);

while (<STDIN>) {
  #manage RG in the header
  if ( /^\@RG/) {
    my ( $same_readgroup, $same_sample, $same_library, $same_platform, $seq_center, $platform_unit) = (0, 0, 0, 0, 0);
    foreach my $field ( split("\t", $_)) {
      next if ($field =~ /^\@/);
      my ($key, $value) = split(":", $field);
      $same_readgroup++ if ( $field eq 'ID' && $value eq $readgroup);
      $same_sample++    if ( $field eq 'SM' && $value eq $sample);
      $same_library++   if ( $field eq 'LB' && $value eq $library);
      $same_platform++  if ( $field eq 'PL' && $value eq $platform);
      $seq_center++     if ( $field eq 'CN' && $value eq $center);
      $platform_unit++  if ( $field eq 'PU' && $value eq $unit);


    }

    if ( $same_readgroup && $same_sample && $same_library && $same_platform && $seq_center && $platform_unit ) {
      $written_readgroup++;
    } else {
      next;
    }
  }
  #manage RG foreach read
  if ( ! /^\@/ ) {
    # Replace RG
    if ( $aligner && ! $written_cmd_flags ) {
      my @fields = ("\@PG", "ID:$aligner");
      push @fields, "CL:$a_line" if ( $a_line );
      print join("\t", @fields) . "\n";
      $written_cmd_flags++;
    }
    # Add RG
    if (! $written_readgroup ) {
      print join("\t", "\@RG", "ID:$readgroup","CN:$center", "LB:$library", "PL:$platform", "PU:$unit", "SM:$sample") . "\n";
      $written_readgroup++;
    }

  
    if ( (/\tRG:Z:(\w+)\t/ || /\tRG:Z:(\w+)\Z/) &&
	 (/\tSM:Z:(\w+)\t/ || /\tSM:Z:(\w+)\Z/)) {
      
      s/(.*\tRG:Z:)(.*?)(\t.*)/$1$readgroup$3/;
      s/(.*\tSM:Z:)(.*?)(\t.*)/$1$sample$3/;
      
      s/(.*\tRG:Z:)(.*?)\Z/$1$readgroup/;
      s/(.*\tSM:Z:)(.*?)\Z/$1$sample/;
    } elsif ( /\tRG:Z:(\w+)\t/ || /\tRG:Z:(\w+)\Z/ ) {
      chomp($_);
      s/(.*\tRG:Z:)(.*?)(\t.*)/$1$readgroup$3/;
      s/(.*\tRG:Z:)(.*?)\Z/$1$readgroup/;
      $_ .= "\tSM:$sample\n";
    } elsif ( /\tSM:Z:(\w+)\t/ || /\tSM:Z:(\w+)\Z/ ) {
      chomp($_);
      s/(.*\tSM:Z:)(.*?)(\t.*)/$1$sample$3/;
      s/(.*\tSM:Z:)(.*?)\Z/$1$sample/;
      $_ .= "\tRG:$readgroup\n";
    } else {
      chomp($_);
      $_ .= "\tRG:Z:$readgroup\tSM:Z:$sample\n";
    }
  }

  print;
  
}


if ( $replace) {
  close($tmp_fh);
  system "mv $tmp_file $infile";
}


sub usage {

  $0 =~ s/.*\///;
  print "$0 \n###############\nAdds/Replaces the \@RG header with given ID/library/sample/platform/platform unit/center tags in a sam file/stream\n";
  print "\t-i[nfile (or stdin] \n\t-o[utfile (or stdout)] \n\t-r[eadgroup:ID] \n\t-s[ample:SM] \n\t-l[ibrary:LB] \n\t-p[latform:PL] \n\t-u[nit:PU] \n\t-c[enter:CN] \n\t-a[ligner:PG] \n\t-A[ligner param] \n\t-R[eplace infile with fixed file]\n";

  exit 1;
}

=pod

=head1 NAME
    
    addReadGroup.pl

=head1 SYNOPSIS
    
    addReadGroup.pl [options]

=head1 DESCRIPTION
    
    addReadGroup.pl Adds/Replaces the @RG header with given ID/library/sample/platform/platform unit/center tags in a sam file/stream

=head1 OPTIONS

    -i[nfile (or stdin] 
    -o[utfile (or stdout)] 
    -r[eadgroup:ID] 
    -s[ample:SM]
    -l[ibrary:LB]
    -p[latform:PL]
    -u[nit:PU]
    -c[enter:CN]
    -a[ligner:PG]
    -A[ligner param]
    -R[eplace infile with fixed file]

=head1 AUTHORS

    Kim Brugger
    modified by Anis Djari

=head1 VERSION

    1

=head1 DATE

    07/22/2010
    modified 11/16/2011
    
=head1 KEYWORDS

    Readgroup GATK RG

=head1 EXAMPLE

    perl addReadGroup.pl [options]
    
=cut

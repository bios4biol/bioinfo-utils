#!/usr/local/bioinfo/bin/python2.5

#
# fasta2fastq : Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, sys, gzip, string
from Bio import SeqIO
from optparse import *

__name__ = "fasta2fastq.py"
__synopsis__ = "fasta2fastq.py -i file.fasta -o output"
__date__ = "06/2012"
__authors__ = "Celine Noirot"
__keywords__ = "fasta fastq conversion"
__description__ = "Convert fasta file to fastq file, required the qual file named with same basename (eg : file.qual or file.fasta.qual)"

__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'

def version_string ():
    """
    Return the fasta2fastq.py version
    """
    return __name__+" " + __version__

########## MAIN ###############
desc = "Convert fasta file to fastq file, \nrequired the qual file named with same basename (eg : file.qual or file.fasta.qual) \n"\
       "ex : fasta2fastq.py -i file.fasta -o file.fastq"
parser = OptionParser(usage="Usage: fasta2fastq.py -i file.fasta -o file.fastq", version = version_string(), description = desc)
igroup = OptionGroup(parser, "Input files options","")
igroup.add_option("-i", "--in", dest="input_file",
                  help="The fasta file", metavar="FILE")
parser.add_option_group(igroup)
ogroup = OptionGroup(parser, "Output files options","")
ogroup.add_option("-o", "--output", dest="fastq",
                  help="The output fastq file", metavar="FILE")
parser.add_option_group(ogroup)
(options, args) = parser.parse_args()

if options.input_file != None and options.fastq != None :

    # If we got a quality file
    if os.path.isfile(os.path.splitext(options.input_file)[0]+".qual") :
        quals = {}
        for qual in SeqIO.parse(open(os.path.splitext(options.input_file)[0]+".qual"), "qual") :
            quals[qual.id] = qual
    elif os.path.isfile(options.input_file+".qual") :
        quals = {}
        for qual in SeqIO.parse(open(options.input_file+".qual"), "qual") :
            quals[qual.id] = qual
            
    else:
        sys.stderr.write("Required a qual file named with same input basename (eg : file.qual or file.fasta.qual)\n")
        parser.print_help()
        sys.exit(1)
    seqs = []
    for record in SeqIO.parse(open(options.input_file), "fasta") :
        if len(quals) > 0 :
            record.letter_annotations["phred_quality"] = quals[record.id].letter_annotations["phred_quality"]
        seqs.append(record)
        
    # First write down seqs
    ffile = open(options.fastq, "w")
    SeqIO.write(seqs, ffile, "fastq")
    ffile.close()
    sys.exit(0)
    
else :
    parser.print_help()
    sys.exit(1)
    
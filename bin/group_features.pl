#!/usr/bin/env perl

#
#       group_features.pl
#
#       Copyright 2013 Sylvain Marthey <sylvain.marthey@jouy.inra.fr>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, see <http://www.gnu.org/licenses/>.

use strict;
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;

my $help ;
my $feat_file ;
my $feat_file_type ;
my $custom_label ;
my $export_format ;
my $export_type ;
my $header ;
my $out_file ;


# on recupere les options
GetOptions("help|?" => \$help,
           "feat_file:s" => \$feat_file,
	   "out_file:s" => \$out_file,
	   "custom_label:s" => \$custom_label,
           "export_format:s" => \$export_format,
           "feat_file_type:s" => \$feat_file_type,
           "export_type:s" => \$export_type,
           "header:s" => \$header
           )
or
pod2usage(-message=>"Try `$0' for more information.", -exitval => 2,-verbose=> 0);


pod2usage(-exitval =>1, -verbose => 2) if ($help);
# on test si les parametres ont bien ete rentres
if ($feat_file eq "" || !-f($feat_file))
  {
     pod2usage(-message=>"--feat_file parameter missing or is not a valid file path.\nTry `$0 --help' for more information.",
              -exitval => 2,-verbose=> 0);
  }
if ($export_type ne "count" &&  $export_type ne "merge" && $export_type ne "use_value")
  {
     pod2usage(-message=>"--export_type parameter missing.\nTry `$0 --help' for more information.",
              -exitval => 2,-verbose=> 0);
  }
if (!$feat_file_type){
	$feat_file_type = "bed";
}elsif($feat_file_type ne "gff" &&  $feat_file_type ne "bed")
  {
     pod2usage(-message=>"--feat_file_type parameter can take only this values : \"gff\" or \"bed\".\nTry `$0 --help' for more information.",
              -exitval => 2,-verbose=> 0);
  }
if ($out_file eq "" )
  {
     pod2usage(-message=>"--out_file parameter missing or is not a valid file path.\nTry `$0 --help' for more information.",
              -exitval => 2,-verbose=> 0);
  }
if ($export_format eq "" )
  {
	$export_format = "bed";
  }


my @first;
my @header;
@first = @{ParseTablulateFile(	file => $feat_file,
  				separator => '\t')};

# on retire les headers des fichiers si besoin (et on les garde)
my @header;
if($header > 0){
	for (my $i = 0; $i < $header; $i++){
		push(@header,shift(@first));
	}
}



# on va faire une premiere lecture pour trier les features par label|piste
my %pists;
my $i;
my $cur_lab;
my $cur_seq;
my @cur_tab;
my $max_count;

# cas fichiers beds :
if($feat_file_type eq 'bed'){
	foreach my $ln (@first){
		$i++;
		my @line = @{$ln};	
		# si on a une nouvelle piste
		if($line[0] =~ m/(track name=\")(\w+)\s/){
			# si c'est pas le premier tour, on ajoute
			if($i>1){
				# on ajoute le tableau existant (ou non) par le nouveau
				my @new = @cur_tab;
				$pists{$cur_lab."0-0".$cur_seq} = \@new;
			}
			# on reinitialise les vars tempraires
			$cur_lab = $2;
			$cur_seq = "";
		# si on a une nouvelle sequence:
		}elsif($cur_seq ne $line[0]){
			# si il n'y avait pas de seq avant alors il faut ajouter le tableau existant
			if($cur_seq ne ""){
				my @new = @cur_tab;
				$pists{$cur_lab."0-0".$cur_seq} = \@new;
			}
			$cur_seq = $line[0];
			# on recupere le tableau correspondant si il exite
			if($pists{$cur_lab."0-0".$cur_seq}){
				@cur_tab = @{$pists{$cur_lab."0-0".$cur_seq}};
			}else{
				@cur_tab = ();
			}
			my @temp = (@{$ln}[1],@{$ln}[2],@{$ln}[4]);
			# on ajoute la ligne au tableau
			push(@cur_tab,\@temp);
		# sinon on ajoute la ligne au tableau de lignes courantes
		}else{
			my @temp = (@{$ln}[1],@{$ln}[2],@{$ln}[4]);
			# on ajoute la ligne au tableau
			push(@cur_tab,\@temp);
		}
	}
	# on ajoute le tableau existant (ou non) par le nouveau
	my @new = @cur_tab;
	$pists{$cur_lab."0-0".$cur_seq} = \@new;
# gestion des fichiers gff
}elsif($feat_file_type eq 'gff'){
	foreach my $ln (@first){
		$i++;
		my @line = @{$ln};
		# si on a une nouvelle piste ou un nouvelle sequence
		if($line[2] ne $cur_lab || $cur_seq ne $line[0]){
			# si c'est pas le premier tour, on ajoute
			if($i>1){
				# on ajoute le tableau existant (ou non) par le nouveau
				my @new = @cur_tab;
				$pists{$cur_lab."0-0".$cur_seq} = \@new;
			#	print "on ajoute: ".$cur_lab."-".$cur_seq." : ".Dumper(\@cur_tab);
			}
			# on reinitialise les vars tempraires
			$cur_lab = $line[2];
			$cur_seq = $line[0];
			# on recupere le tableau correspondant si il exite
			if($pists{$cur_lab."0-0".$cur_seq}){
				@cur_tab = @{$pists{$cur_lab."0-0".$cur_seq}};
			}else{
				@cur_tab = ();
			}
			my @temp = (@{$ln}[3],@{$ln}[4],@{$ln}[5]);
			# on ajoute la ligne au tableau
			push(@cur_tab,\@temp);
		# sinon on ajoute la ligne au tableau de lignes courantes
		}else{
			my @temp = (@{$ln}[3],@{$ln}[4],@{$ln}[5]);
			# on ajoute la ligne au tableau
			push(@cur_tab,\@temp);
		}
	}
	# on ajoute le tableau existant (ou non) par le nouveau
	my @new = @cur_tab;
	$pists{$cur_lab."0-0".$cur_seq} = \@new;
}

# print Dumper(\%pists);

# on va maintenant faire les calculs souhaites
my %res;
if($export_type eq "merge"){
	# pour chaque couple label-sequence on va merger les probes
	foreach my $k (keys(%pists)){
		$res{$k} = mergeContigousPositions('positions' => $pists{$k});
	}
}elsif($export_type eq "count"){
	# pour chaque couple label-sequence on va calculer la couverture des bases
	foreach my $k (keys(%pists)){
		$res{$k} = mergeCoverageCalcul('positions' => $pists{$k});
	}	
}elsif($export_type eq "use_value"){
	# pour chaque couple label-sequence on va regrouper les fetures par valeurs
	foreach my $k (keys(%pists)){
		my %values_hash;
		foreach my  $ft (@{$pists{$k}}){
			push(@{$values_hash{@{$ft}[2]}},$ft);
		}
		# pour chaque valeur on va alors grouper les features
		foreach my $va (keys(%values_hash)){
			$res{$k."0-0".$va} = mergeContigousPositions('positions' => $values_hash{$va});
		}
	}
}

# on ecrit les resultats
open (OUT,">$out_file") or die "impossible d'ouvrir le fichier de sortie";
# en fonction du format
if($export_format eq "gff"){
	print OUT "##gff-version	3\n";
	foreach my $k (keys(%res)){
		my @temp = split(/0-0/,$k);
		if($export_type eq "merge"){
			foreach my $ft(@{$res{$k}}){
				if($custom_label){
					print OUT $temp[1]."\tProbes\t".$custom_label."_til\t".@{$ft}[0]."\t".@{$ft}[1]."\t1\t+\t\t@{$ft}[2]\n";
				}else{
					print OUT $temp[1]."\tProbes\t".$temp[0]."_til\t".@{$ft}[0]."\t".@{$ft}[1]."\t1\t+\t\t@{$ft}[2]\n";
				}
			}
		}elsif($export_type eq "count"){
			foreach my $ft(@{$res{$k}}){
				if($custom_label){
					print OUT $temp[1]."\tProbes\t".$custom_label."_cov\t".@{$ft}[0]."\t".@{$ft}[1]."\t".@{$ft}[2]."\t+\t\t\n";
				}else{
					print OUT $temp[1]."\tProbes\t".$temp[0]."_cov\t".@{$ft}[0]."\t".@{$ft}[1]."\t".@{$ft}[2]."\t+\t\t\n";
				}
			}
		}elsif($export_type eq "use_value"){
			foreach my $ft(@{$res{$k}}){
				if($custom_label){
					print OUT $temp[1]."\tProbes\t".$custom_label."_cov\t".@{$ft}[0]."\t".@{$ft}[1]."\t".$temp[2]."\t+\t\t@{$ft}[2]\n";
				}else{
					print OUT $temp[1]."\tProbes\t".$temp[0]."_cov\t".@{$ft}[0]."\t".@{$ft}[1]."\t".$temp[2]."\t+\t\t@{$ft}[2]\n";
				}
			}
		}
	}
}elsif($export_format eq "bed"){
	print OUT "browser position\n";
	foreach my $k (keys(%res)){
		my @temp = split(/0-0/,$k);
		if($export_type eq "merge"){
			if($custom_label){
				print OUT "track name=\"".$custom_label."_til\" description=\"".$custom_label." Tiling Array regions covered\" visibility=2 color=0,128,0 useScore=1\n";
			}else{
				print OUT "track name=\"".$temp[1]."_til\" description=\"".$temp[1]." Tiling Array regions covered\" visibility=2 color=0,128,0 useScore=1\n";
			}
			foreach my $ft(@{$res{$k}}){
				print OUT $temp[1]."\t".@{$ft}[0]."\t".@{$ft}[1]."\tProbes\t1000\t+\n";
			}
		}elsif($export_type eq "count"){
			if($custom_label){
				print OUT "track name=\"".$custom_label."_cov\" description=\"".$custom_label." Tiling Array regions covered\" visibility=2 color=0,128,0 useScore=1\n";
			}else{
				print OUT "track name=\"".$temp[1]."_cov\" description=\"".$temp[1]." Tiling Array regions covered\" visibility=2 color=0,128,0 useScore=1\n";
			}
			foreach my $ft(@{$res{$k}}){
				print OUT $temp[1]."\t".@{$ft}[0]."\t".@{$ft}[1]."\tProbes\t".(@{$ft}[2]*100)."\t+\n";
			}
		}elsif($export_type eq "use_value"){
			if($custom_label){
				print OUT "track name=\"".$custom_label."_cov\" description=\"".$custom_label." Tiling Array regions covered\" visibility=2 color=0,128,0 useScore=1\n";
			}else{
				print OUT "track name=\"".$temp[1]."_cov\" description=\"".$temp[1]." Tiling Array regions covered\" visibility=2 color=0,128,0 useScore=1\n";
			}
			foreach my $ft(@{$res{$k}}){
				print OUT $temp[1]."\t".@{$ft}[0]."\t".@{$ft}[1]."\tProbes\t".$temp[2]."\t+\n";
			}
		}
	}
}elsif($export_format eq "bed_array"){
	print OUT "browser position\n";
	foreach my $k (keys(%res)){
		my @temp = split(/0-0/,$k);
		if($export_type eq "merge"){
			if($custom_label){
				print OUT "track type=\"array\" expScale=1.0 expStep=0.1 expNames=\"".$custom_label.",\" name=\"Microarray\" description=\"".$custom_label." tiling\"\n";
			}else{
				print OUT "track type=\"array\" expScale=1.0 expStep=0.1 expNames=\"".$temp[1].",\" name=\"Microarray\" description=\"".$temp[1]." tiling\"\n";
			}
			foreach my $ft(@{$res{$k}}){
				print OUT $temp[1]."\t".(@{$ft}[0]-1)."\t".@{$ft}[1]."\tProbes\t1000\t+\t".(@{$ft}[0]-1)."\t".@{$ft}[1]."\t0\t1\t".(@{$ft}[1]-(@{$ft}[0]-1)).",\t0,\t1\t1,\t0.5,\n";
			}
		}elsif($export_type eq "count"){
			if($custom_label){
				print OUT "track type=\"array\" expScale=1.0 expStep=0.1 expNames=\"".$custom_label.",\" name=\"Microarray\" description=\"".$custom_label." coverage\"\n";
			}else{
				print OUT "track type=\"array\" expScale=1.0 expStep=0.1 expNames=\"".$temp[1].",\" name=\"Microarray\" description=\"".$temp[1]." coverage\"\n";
			}
			foreach my $ft(@{$res{$k}}){
				print OUT $temp[1]."\t".(@{$ft}[0]-1)."\t".@{$ft}[1]."\tProbes\t".(@{$ft}[2]*100)."\t+\t".(@{$ft}[0]-1)."\t".@{$ft}[1]."\t0\t1\t".(@{$ft}[1]-(@{$ft}[0]-1)).",\t0,\t1\t1,\t".(@{$ft}[2]/10).",\n";
			}
		}elsif($export_type eq "use_value"){
			if($custom_label){
				print OUT "track type=\"array\" expScale=1.0 expStep=0.1 expNames=\"".$custom_label.",\" name=\"Microarray\" description=\"".$custom_label." coverage\"\n";
			}else{
				print OUT "track type=\"array\" expScale=1.0 expStep=0.1 expNames=\"".$temp[1].",\" name=\"Microarray\" description=\"".$temp[1]." coverage\"\n";
			}
			foreach my $ft(@{$res{$k}}){
				print OUT $temp[1]."\t".(@{$ft}[0]-1)."\t".@{$ft}[1]."\tProbes\t".$temp[2]."\t+\t".(@{$ft}[0]-1)."\t".@{$ft}[1]."\t0\t1\t".(@{$ft}[1]-(@{$ft}[0]-1)).",\t0,\t1\t1,\t".($temp[2]/100).",\n";
			}
		}
	}	
}elsif($export_format eq "wig"){

	foreach my $k (keys(%res)){
		my @temp = split(/0-0/,$k);
		if($export_type eq "merge"){
			if($custom_label){
				print OUT "track type=wiggle_0 name=\"".$custom_label."_til\" description=\"".$custom_label."\" visibility=full autoScale=off viewLimits=0.0:10.0 color=50,150,255 yLineMark=5.0 yLineOnOff=on priority=10\n";
			}else{
				print OUT "track type=wiggle_0 name=\"".$temp[1]."_til\" description=\"".$temp[1]."\" visibility=full autoScale=off viewLimits=0.0:10.0 color=50,150,255 yLineMark=5.0 yLineOnOff=on priority=10\n";
			}
			print OUT "variableStep chrom=".$temp[1]."\n";
			foreach my $ft(@{$res{$k}}){
				for (my $i=@{$ft}[0]; $i <= @{$ft}[1];$i++){
					print OUT $i."\t10\n";
				}
			}
		}elsif($export_type eq "count"){
			if($custom_label){
				print OUT "track type=wiggle_0 name=\"".$custom_label."_cov\" description=\"".$custom_label."\" visibility=full autoScale=off viewLimits=0.0:10.0 color=50,150,255 yLineMark=5.0 yLineOnOff=on priority=10\n";
			}else{
				print OUT "track type=wiggle_0 name=\"".$temp[1]."_cov\" description=\"".$temp[1]."\" visibility=full autoScale=off viewLimits=0.0:10.0 color=50,150,255 yLineMark=5.0 yLineOnOff=on priority=10\n";
			}
			print OUT "variableStep chrom=".$temp[1]."\n";
			foreach my $ft(@{$res{$k}}){
				for (my $i=@{$ft}[0]; $i <= @{$ft}[1];$i++){
					print OUT $i."\t".@{$ft}[2]."\n";
				}
			}
		}elsif($export_type eq "use_value"){
			if($custom_label){
				print OUT "track type=wiggle_0 name=\"".$custom_label."_cov\" description=\"".$custom_label."\" visibility=full autoScale=off viewLimits=0.0:10.0 color=50,150,255 yLineMark=5.0 yLineOnOff=on priority=10\n";
			}else{
				print OUT "track type=wiggle_0 name=\"".$temp[1]."_cov\" description=\"".$temp[1]."\" visibility=full autoScale=off viewLimits=0.0:10.0 color=50,150,255 yLineMark=5.0 yLineOnOff=on priority=10\n";
			}
			print OUT "variableStep chrom=".$temp[1]."\n";
			foreach my $ft(@{$res{$k}}){
				for (my $i=@{$ft}[0]; $i <= @{$ft}[1];$i++){
					print OUT $i."\t".$temp[2]."\n";
				}
			}
		}
	}	
}

close OUT;





=begin function

  Function    : mergeCoverageCalcul
  Description : count the coverage of each probe and provide new features associate with a coverage depth.
  Usage       : mergeCoverageCalcul(	positions => table);
  Parameters  : table -> table in two dimensions, first column is the starts positions. Second columns is a stop position
  Returns     : Table in two dimensions, first column is start, second column is stop, third column is coverage depth.
  Version     : v1.0

=end function

=cut

sub mergeCoverageCalcul {
	my %args = @_;
	my @pos = @{$args{positions}};
	
	my @count;
	# count the coverage of each base
	foreach my $t (@pos){
		for (my $i=@{$t}[0]; $i<=@{$t}[1]; $i++){
			$count[$i]++;
		}
	}
	
	# create the news features with coverage depth
	my $cur_dpth=0;
	my $cur_start;
	my @fn_cov;
	for (my $i = 1; $i < scalar(@count); $i++){
		if($cur_dpth != $count[$i]){
			# beginig of a new coverage area
			if($cur_dpth == 0){
				$cur_start = $i;
				$cur_dpth = $count[$i];
			# end of a new coverage area
			}else{
				my @temp = ($cur_start,($i-1),$cur_dpth);
				push(@fn_cov,\@temp);
				$cur_start = $i;
				$cur_dpth = $count[$i];
			}
		}
	}
	if($cur_dpth != 0){
		my @temp = ($cur_start,(scalar(@count)-1),$cur_dpth);
		push(@fn_cov,\@temp);
	}
	return \@fn_cov;
}


=begin function

  Function    : mergeContigousPositions
  Description : merge all contigous positions.
  Usage       : mergeContigousPositions(	positions => table);
  Parameters  : table -> table in two dimensions, first column is the starts positions. Second columns is a stop position
  Returns     : Table in two dimensions, first column is start and second column is stop.
  Version     : v1.0

=end function

=cut

sub mergeContigousPositions {
	my %args = @_;
	my @raw_pos = @{$args{positions}};
	
	# first, we sort this array
	my @pos = sort par_pos_asc @raw_pos;
	
	# next we create the news positions
	my @fn_pos;
	my $cr_st = @{$pos[0]}[0];
	my $cr_sp = @{$pos[0]}[1];
	my $ct;
	
	# cur                   |------------------------------|
	# A               |--------------|
	# B                                               |--------------|
	# C                               |--------------|
	# D                |--------------------------------------------|
	# E       |---------|
	# F                                                         |---------|
	foreach my $t (@pos){
		# case A
		if($cr_st >= @{$t}[0] && $cr_st <= @{$t}[1] && $cr_sp >=  @{$t}[1]){
			$cr_st = @{$t}[0];
			$ct++;
		# case B;
		}elsif($cr_st <= @{$t}[0] && $cr_sp <= @{$t}[1] &&  $cr_sp >= @{$t}[0]){
			$cr_sp = @{$t}[1];
			$ct++;
		# case C
#		}elsif($cr_st <= @{$t}[0] && $cr_sp >=  @{$t}[1]){
#			next;
		# case D
		}elsif($cr_st >= @{$t}[0] && $cr_sp <=  @{$t}[1]){
			$cr_st = @{$t}[0];
			$cr_sp = @{$t}[1];
			$ct++;
		# case E
		}elsif($cr_st >=  @{$t}[1]){
			my @temp = ($cr_st,$cr_sp,$ct);
			push(@fn_pos,\@temp);
			$ct=1;
			$cr_st = @{$t}[0];
			$cr_sp = @{$t}[1];
		# case F
		}elsif($cr_sp <=  @{$t}[0]){
			my @temp = ($cr_st,$cr_sp,$ct);
			push(@fn_pos,\@temp);
			$ct=1;
			$cr_st = @{$t}[0];
			$cr_sp = @{$t}[1];
		}else{
			$ct++;
			# print @{$t}[0]."  ".@{$t}[1]." $cr_st $cr_sp NPC\n";
		}
		
	}
	my @temp = ($cr_st,$cr_sp,$ct);
	push(@fn_pos,\@temp);
	
	return \@fn_pos;
}


=begin function

  Function    : mergeContigousValues
  Description : merge all contigous positions.
  Usage       : mergeContigousValues(	positions => table);
  Parameters  : table -> table in two dimensions, first column is the starts positions. Second columns is a stop position
  Returns     : Table in two dimensions, first column is start and second column is stop.
  Version     : v1.0

=end function

=cut

sub mergeContigousValues {
	my %args = @_;
	my @raw_pos = @{$args{positions}};

	# first, we sort this array
	my @pos = sort par_pos_asc @raw_pos;

	# next we create the news positions
	my @fn_pos;
	my $cr_st = @{$pos[0]}[0];
	my $cr_sp = @{$pos[0]}[1];

	# cur                   |------------------------------|
	# A               |--------------|
	# B                                               |--------------|
	# C                               |--------------|
	# D                |--------------------------------------------|
	# E       |---------|
	# F                                                         |---------|
	foreach my $t (@pos){
		# case A
		if($cr_st >= @{$t}[0] && $cr_st <= @{$t}[1] && $cr_sp >=  @{$t}[1]){
			$cr_st = @{$t}[0];
		# case B;
		}elsif($cr_st <= @{$t}[0] && $cr_sp <= @{$t}[1] &&  $cr_sp >= @{$t}[0]){
			$cr_sp = @{$t}[1];
		# case C
#		}elsif($cr_st <= @{$t}[0] && $cr_sp >=  @{$t}[1]){
#			next;
		# case D
		}elsif($cr_st >= @{$t}[0] && $cr_sp <=  @{$t}[1]){
			$cr_st = @{$t}[0];
			$cr_sp = @{$t}[1];
		# case E
		}elsif($cr_st >=  @{$t}[1]){
			my @temp = ($cr_st,$cr_sp);
			push(@fn_pos,\@temp);
			$cr_st = @{$t}[0];
			$cr_sp = @{$t}[1];
		# case F
		}elsif($cr_sp <=  @{$t}[0]){
			my @temp = ($cr_st,$cr_sp);
			push(@fn_pos,\@temp);
			$cr_st = @{$t}[0];
			$cr_sp = @{$t}[1];
		}else{
			print @{$t}[0]."  ".@{$t}[1]." $cr_st $cr_sp NPC\n";
		}

	}
	my @temp = ($cr_st,$cr_sp);
	push(@fn_pos,\@temp);

	return \@fn_pos;
}



=begin function

  Function    : ParseTablulateFile
  Description : Parse a Tabulate file file and return a table ref. The file must be regular (all the rown have the same number of colums). One argumant is the separator for the parsing, by defaut it is the tabulation.
  Usage       : ParseTablulateFile(	file => '/home/toto/super_annots.csv',
  					separator => '\t',
  					check_nb_coloums => 'yes');
  Parameters  : file -> path to the Tabulate file.
  Returns     : Table in two dimensions, first is row and second is columm.
  Version     : v1.0

=end function

=cut

sub ParseTablulateFile {
	my %args = @_;
	my $file = $args{file};
	my $sep = $args{separator};
	my @table;
	my $nb_col; # permettra de tester si conflits avec le separateur
	# si pas de separator, on donne la valeur par defaut
	if(!$args{separator}){
		$sep = '\t';
	}
	# ouverture du fichier tabule
	open(IN,$file) or die "impossible d'ouvrir le fichier Tabule\n";
	my $num_line;
	# pour chaque ligne du fichier
	while(<IN>){
		$num_line++;
		# on vire le retour a� la ligne
		chomp($_);
		# on vire le separateur windaub
		$_ =~ s/\r//;
		# on degage les lignes vides 
		next if(!$_);
		# on split la ligne sur le separateur
		my @line = split(/$sep/,$_);
		# on test le nombre de colonnes (si besoin)
		if(($args{check_nb_coloums} eq 'yes') && $nb_col && ($nb_col != scalar(@line))){
			print "La ligne $num_line comprend ".scalar(@line)." colonnes alors que les lignes precedentes en contenaient $nb_col.\nDonc soit le separateur de colonne qui a etet choisit est ambigu, soit les lignes n\'ont pas toutes le meme nombre de colonnes.\nSi vous souhaitez ignorer cet avertissement, relancer le script avec l\'option --check_nb_colums no\n";
			exit;
		}
		$nb_col = scalar(@line);
		# on ajoute la ligne au tableau
		push(@table,\@line);
	}
	close IN;
	# on renvoie le tableau
	return \@table;
}

# tri ascendant du tableau N*2 par rapport au contenu de la premiere colonne
sub par_pos_asc {
   my @ap = @{$a};
   my @bp = @{$b};
   if($ap[0] < $bp[0]){
      return -1;
   }	elsif($ap[0] == $bp[0]){
      return 0;
   }	elsif($ap[0] > $bp[0]){
      return 1;
   }
}

=pod

=head1 NAME

group_features.pl

=head1 SYNOPSIS

group_features.pl --feat_file <file_path> --feat_file_type <gff,bed> --out_file <file_path> --export_type <merge,count> --export_format <gff,bed> --custom_label <name> [--header <int>]

=head1 DESCRIPTION

	Allows to group fetures using grouping criteria (none/label/value).
	
	!! Warning !! this script does not take account strands ! => separate files by strand before using this tool.

Options:

	--feat_file	: path to the file containing the features

	--feat_file_type	: feature file type  (<bed>, gff)
	
	--out_file	: out file path

	--export_type	:  output file export type :
				- merge: all overlapping features are grouped in the same feaure.
				- count: at each base, the depth in feature is calculated. Each consecutive base having the same depth is grouped in a feature.
				- use_value : merge only features that have the same value

	--export_format	: 	output file type.(<bed>, gff)
	
	--header	: 	Number of header lines.

	--custom_label	:	a label that will be used to replace all the labels tracks.
	
	!! Warning !! this script does not take account strands ! It will therefore grouping features in + and -

           
=head1 AUTHORS

Sylvain Marthey <sylvain.marthey@jouy.inra.fr>

=head1 VERSION

1.1

=head1 DATE

2009

=head1 KEYWORDS

features, coverage, bedtools

=head1 EXAMPLE

group_features.pl --feat_file all_probes.bed --feat_file_type bed --out_file region_covered.bed --export_type merge

=cut



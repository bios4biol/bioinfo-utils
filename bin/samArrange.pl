#!/usr/bin/perl -w
use strict;
use Getopt::Long;
use Pod::Usage;

# the read is paired in sequencing, no matter whether it is mapped in a pair
use constant BAM_FPAIRED => 0x1;
# the read is mapped in a proper pair
use constant BAM_FPROPER_PAIR => 0x2;
# the read itself is unmapped; conflictive with BAM_FPROPER_PAIR
use constant BAM_FUNMAP => 0x4;
# the mate is unmapped
use constant BAM_FMUNMAP => 0x8;
# the read is mapped to the reverse strand
use constant BAM_FREVERSE => 0x10;
# the mate is mapped to the reverse strand
use constant BAM_FMREVERSE => 0x20;
# this is read1
use constant BAM_FREAD1 => 0x40;
# this is read2
use constant BAM_FREAD2 => 0x80;
# not primary alignment
use constant BAM_FSECONDARY => 0x100;
# QC failure
use constant BAM_FQCFAIL => 0x200;
# optical or PCR duplicate
use constant BAM_FDUP => 0x400;

# just to point to the right column
my @Fields = qw(QNAME FLAG RNAME POS MAPQ CIGAR RNEXT PNEXT TLEN SEQ QUAL TAG);
my %Fields=();
for(my $i=0;$i<=$#Fields;$i++){ $Fields{$Fields[$i]}=$i;}

#------------------------------------------------------------
# read a fastq file entry
sub read_fastq ( $ ) {
  my ($FH)=@_;
  # read sequence header
  my $line='';
  while ($line=<$FH>) {
    last if ($line=~/^@/);
    next if ($line=~/^\s*$/);
  }
  return () unless (defined($line));
  my %fq=();
  chomp $line;
  $line=~/^@(\S+)/ || die "not a valid fastq header line: $line";
  $fq{ac}=$1;
  $fq{ac}=~s/\/[12]$//;
  $fq{def}=$line;
  $fq{seq}=$fq{qual}='';
  # loop on sequence lines
  while ($line=<$FH>) {
    next if ($line=~/^\s*$/);
    last if ($line=~/^\+/);
    chomp $line;
    $fq{seq}.=$line;
  }
  defined($line) || die "bad formated fastq file: can't locate quality for AC=$fq{ac}";
  my $lg1=length($fq{seq});
  my $lg2=0;
  while($lg2<$lg1 && defined($line=<$FH>)) {
    next if ($line=~/^\s*$/);
    chomp $line;
    $fq{qual}.=$line;
    $lg2+=length($line);
  }
  $lg1==$lg2 || die "bad formated fastq file: quality and sequence lengthes differ for AC=$fq{ac}";
  return %fq;
}

#------------------------------------------------------------
# read SAM alignment lines corresponding to a same entry
# and store alignments lines uniquely for each location
sub read_aln ( $ * ) {
  my ($FH,$aln) = @_;
  return() if (!defined($$aln)); # no alignment given
  my @F=split/\t/,$$aln;
  $F[$Fields{QNAME}]=~s/\/[12]$//; # remove read numbering
  my $qname=$F[$Fields{QNAME}];
  my %ALN=();
  $ALN{qname}=$qname;
  @{$ALN{mapping}{$F[$Fields{RNAME}]}{$F[$Fields{POS}]}}=@F;
  while($$aln=<$FH>) {
    chomp $$aln;
    @F=split/\t/,$$aln;
    $F[$Fields{QNAME}]=~s/\/[12]$//; # remove read numbering
    last if ($qname ne $F[$Fields{QNAME}]);
    @{$ALN{mapping}{$F[$Fields{RNAME}]}{$F[$Fields{POS}]}}=@F;
  }
  # clean up unmapped lines
  foreach my $chr (keys %{$ALN{mapping}}) {
    foreach my $pos (keys %{$ALN{mapping}{$chr}}) {
      delete $ALN{mapping}{$chr}{$pos} if ($ALN{mapping}{$chr}{$pos}[$Fields{FLAG}]& BAM_FUNMAP);
    }
    delete $ALN{mapping}{$chr} unless (scalar keys %{$ALN{mapping}{$chr}});
  }
  delete $ALN{mapping} unless (scalar keys %{$ALN{mapping}});
  return %ALN;
}

#------------------------------------------------------------
# sort to alignment lines accordint to their chr pos
sub sort_aln ( * * ) {
  my ($a,$b)=@_;

  return ($a->[$Fields{POS}] <=> $a->[$Fields{POS}]) if ($a->[$Fields{RNAME}] eq $b->[$Fields{RNAME}]);
  return ($a->[$Fields{RNAME}] cmp $b->[$Fields{RNAME}]);
}

#------------------------------------------------------------
# print SAM alignment line
sub print_entry ( * ) {
  my ($entry)=@_;
  if ($entry->{TAG}eq'') {
     printf "%s\n",join("\t",map{$entry->{$_}} @Fields[0..$#Fields-1]);
  }
  else {
    printf "%s\n",join("\t",map{$entry->{$_}} @Fields);
  }
}

#------------------------------------------------------------
# CIGAR related routine copied from bam.c and bam_import.c

use constant BAM_CIGAR_SHIFT => 4;
use constant BAM_CIGAR_MASK => ((1 << BAM_CIGAR_SHIFT ) -1 );
# CIGAR: match
use constant BAM_CMATCH => 0;
# CIGAR: insertion to the reference
use constant BAM_CINS => 1;
# CIGAR: deletion from the reference
use constant BAM_CDEL => 2;
# CIGAR: skip on the reference (e.g. spliced alignment)
use constant BAM_CREF_SKIP => 3;
# CIGAR: clip on the read with clipped sequence present in qseq
use constant BAM_CSOFT_CLIP => 4;
# CIGAR: clip on the read with clipped sequence trimmed off
use constant BAM_CHARD_CLIP => 5;
# CIGAR: padding
use constant BAM_CPAD => 6;

sub bam_calend( * * ) {
  my ($pos,$cigar_string)=@_;
  my (@cigar)=($cigar_string=~/(\d+\S)/g);
  length($cigar_string)==length(join('',@cigar)) || die "invalid CIGAR string: $cigar_string";
  
#   # compute binary cigar from string (from bam_import.c: sam_read1) 
#   for(my $i=0;$i<=$#cigar;$i++) {
#     my ($x,$t)=($cigar[$i]=~/^(\d+)(\S)$/);
#     my $op=uc($t);
#     if ($op eq 'M' || $op eq '=' || $op eq 'X') { $op = BAM_CMATCH; }
#     elsif ($op eq 'I') { $op = BAM_CINS; }
#     elsif ($op eq 'D') { $op = BAM_CDEL; }
#     elsif ($op eq 'N') { $op = BAM_CREF_SKIP; }
#     elsif ($op eq 'S') { $op = BAM_CSOFT_CLIP; }
#     elsif ($op eq 'H') { $op = BAM_CHARD_CLIP; }
#     elsif ($op eq 'P') { $op = BAM_CPAD; }
#     else { die "invalid CIGAR operation: $op"; }
#     $cigar[$i]=(($x << BAM_CIGAR_SHIFT ) | $op);}

   my $end = $pos;
#   for (my $k=0; $k <= $#cigar; ++$k) {
#     my $op = $cigar[$k] & BAM_CIGAR_MASK;
#     if ($op == BAM_CMATCH || $op == BAM_CDEL || $op == BAM_CREF_SKIP) {
#       $end += $cigar[$k] >> BAM_CIGAR_SHIFT;
#     }
#   }
  foreach my $c (@cigar) {
    my ($x,$t)=($c=~/^(\d+)(\S)$/);
    $end += $x if ($t=~/[M=XDN]/i);
  }
  return $end;
}

#============================================================

#---------- Options definitions (name,format,default,required)
my @Options = ( ['help',undef,undef,0],
		['man',undef,undef,0],
		['f1','s',undef,1],
		['f2','s',undef,1],
		['s1','s',undef,1],
		['s2','s',undef,1],
		['S',undef,0,0],
		['remove_numbering',undef,0,0],
		['lmin','i',100,0],
		['lmax','i',500,0],
		['same_strand',undef,0,0],
	      );
# defaults values
my %Values=();
my %Required=();
map {$Values{$_->[0]}=$_->[2];$Required{$_->[0]}=$_->[3];} @Options;
# build options list
my %Options=();
map {$Options{defined($_->[1])?$_->[0].'='.$_->[1]:$_->[0]}=\$Values{$_->[0]};} @Options;
# retrieve options
GetOptions(%Options) || pod2usage(2);
pod2usage(1) if ($Values{'help'});
pod2usage(-exitstatus => 0, -verbose => 2) if ($Values{'man'});

# check required
map {pod2usage("$_ is required") if ($Required{$_}&&!defined($Values{$_}));} keys %Values;

map {
  -e $Values{$_} || die "no such file $Values{$_}";
} ('f1','f2','s1','s2');

my $S= $Values{S}?'-S':'';

my $read1_flag='';
my $read2_flag='';
if ($Values{s1} eq $Values{s2}) { # same BAM/SAM as input
  $read1_flag='-f 64';
  $read2_flag='-f 128';
}

#---------- generates SAM header
open(SAM_HEADER,">/tmp/$$.txt") || die "Can't create temp sam header file";
open(S1,"samtools view -H $S $Values{s1} 2>/dev/null|grep '^\@SQ'|") || die "Can't open file $Values{s1}";
print SAM_HEADER <S1>;
close(S1);
open(S2,"samtools view -H $S $Values{s2} 2>/dev/null|grep '^\@SQ'|") || die "Can't open file $Values{s2}";
print SAM_HEADER <S2>;
close(S2);
close(SAM_HEADER);
open(SAM_HEADER,"sort -u /tmp/$$.txt|") || die "Can't reopen temp sam header file";
print <SAM_HEADER>;
close(SAM_HEADER);

# open fastq files
open(F1,$Values{f1}) || die "Can't open file $Values{f1}";
open(F2,$Values{f2}) || die "Can't open file $Values{f2}";

# open sam/bam files and read alignments for first entry
open(S1,"samtools view $read1_flag $S $Values{s1} 2>/dev/null|") || die "Can't open file $Values{s1}";
my $aln1=<S1>;
chomp $aln1 if(defined($aln1));
my %ALN1=read_aln('S1',\$aln1);

open(S2,"samtools view $read2_flag $S $Values{s2} 2>/dev/null|") || die "Can't open file $Values{s2}";
my $aln2=<S2>;
chomp $aln2 if(defined($aln2));
my %ALN2=read_aln('S2',\$aln2);

#---------- loop on fastq entries
while (1) {
  my %fastq1=read_fastq('F1');
  last unless (%fastq1);

  my %fastq2=read_fastq('F2');
  die "fastq files haven't same number of entries" unless(%fastq2);

  $fastq1{ac} eq $fastq2{ac} || die "fastq files haven't same ordered entries: $fastq1{ac}\t$fastq2{ac}";

  my %entry1=(QNAME => $fastq1{ac},
	      FLAG => BAM_FPAIRED|BAM_FREAD1, # paired read, first read
	      RNAME => '*',
	      POS => 0,
	      MAPQ => 0,
	      CIGAR => '*',
	      RNEXT => '*',
	      PNEXT => 0,
	      TLEN => 0,
	      SEQ => $fastq1{seq},
	      QUAL => $fastq1{qual},
	      TAG => '');
  my %entry2=(QNAME => $fastq2{ac},
	      FLAG => BAM_FPAIRED|BAM_FREAD2, # paired read, second read
	      RNAME => '*',
	      POS => 0,
	      MAPQ => 0,
	      CIGAR => '*',
	      RNEXT => '*',
	      PNEXT => 0,
	      TLEN => 0,
	      SEQ => $fastq2{seq},
	      QUAL => $fastq2{qual},
	      TAG => '');

  if (!exists($ALN1{qname}) || $fastq1{ac} ne $ALN1{qname} || !exists($ALN1{mapping})) { # first fragment is not mapped
    $entry1{FLAG}|=BAM_FUNMAP; # read unmapped
    if (!exists($ALN2{qname}) || ($fastq2{ac} ne $ALN2{qname}) || !exists($ALN2{mapping})) { 
      #---------- first & second fragments not mapped
      $entry1{FLAG}|=BAM_FMUNMAP; # next read unmapped
      $entry2{FLAG}|=BAM_FUNMAP; # read unmapped
      $entry2{FLAG}|=BAM_FMUNMAP; # next read unmapped
      print_entry(\%entry1);
      print_entry(\%entry2);
    }
    else { 
      #---------- first segment unmapped, second one mapped
      # loop on second read alignments
      foreach my $chr (keys %{$ALN2{mapping}}) {
	foreach my $pos (keys %{$ALN2{mapping}{$chr}}) {
	  my %entry1_bis = %entry1;
	  my %entry2_bis = %entry2;	  	  
	   
	  if ($ALN2{mapping}{$chr}{$pos}[$Fields{FLAG}] & BAM_FREVERSE) { 
	    $entry2_bis{FLAG}|= BAM_FREVERSE; # read reversed
	    $entry1_bis{FLAG}|= BAM_FMREVERSE; # next read reversed
	  }
	  $entry1_bis{RNEXT} = $ALN2{mapping}{$chr}{$pos}[$Fields{RNAME}];
	  $entry1_bis{PNEXT} = $ALN2{mapping}{$chr}{$pos}[$Fields{POS}];
	  
	  # keep mapping info
	  foreach my $f qw(RNAME POS MAPQ CIGAR SEQ QUAL) {
	    $entry2_bis{$f}=$ALN2{mapping}{$chr}{$pos}[$Fields{$f}];
	  }
	  # keep tags
	  my $n=scalar(@{$ALN2{mapping}{$chr}{$pos}})-1;
	  $entry2_bis{TAG}=join("\t",@{$ALN2{mapping}{$chr}{$pos}}[$Fields{TAG}..$n]);
	  
	  # remove unwanted flags 
	  $entry2_bis{FLAG} &= ~(BAM_FPROPER_PAIR | BAM_FMREVERSE | BAM_FREAD1 | BAM_FSECONDARY);
	  $entry2_bis{FLAG} |= BAM_FMUNMAP; # next fragment unmapped
	  print_entry(\%entry1_bis);
	  print_entry(\%entry2_bis);
	}
      }
    }
  }
  elsif (!exists($ALN2{qname}) || ($fastq2{ac} ne $ALN2{qname}) || !exists($ALN2{mapping})) { 
    #---------- first mapped, second not
    foreach my $chr (keys %{$ALN1{mapping}}) {
      foreach my $pos (keys %{$ALN1{mapping}{$chr}}) {
	my %entry1_bis=%entry1;
	my %entry2_bis=%entry2;

	$entry2_bis{FLAG}|= BAM_FUNMAP; # read unmapped;
		 
	if ($ALN1{mapping}{$chr}{$pos}[$Fields{FLAG}] & BAM_FREVERSE) {
	    $entry2_bis{FLAG}|= BAM_FMREVERSE; # next read reversed
	    $entry1_bis{FLAG}|= BAM_FREVERSE; # read reversed
	}
	$entry2_bis{RNEXT} = $ALN1{mapping}{$chr}{$pos}[$Fields{RNAME}];
	$entry2_bis{PNEXT} = $ALN1{mapping}{$chr}{$pos}[$Fields{POS}];
	
	# keep mapping info
	foreach my $f qw(RNAME POS MAPQ CIGAR SEQ QUAL) {
	  $entry1_bis{$f}=$ALN1{mapping}{$chr}{$pos}[$Fields{$f}];
	}
	# keep tags
	my $n=scalar(@{$ALN1{mapping}{$chr}{$pos}})-1;
	$entry1_bis{TAG}=join("\t",@{$ALN1{mapping}{$chr}{$pos}}[$Fields{TAG}..$n]);
	
	# remove unwanted flags
	$entry1_bis{FLAG}&= ~(BAM_FPROPER_PAIR | BAM_FMREVERSE | BAM_FREAD2 | BAM_FSECONDARY);

	$entry1_bis{FLAG}|= BAM_FMUNMAP; # next fragment unmapped
	print_entry(\%entry1_bis);
	print_entry(\%entry2_bis);
      }
    }
  }
  else { 
    #---------- both mapped, try to find properly aligned fragments
    # sort fragments according to their chr and pos
    my @ALN1=();
    foreach my $chr (keys %{$ALN1{mapping}}) {
      foreach my $pos (keys %{$ALN1{mapping}{$chr}}) {
	push @ALN1,\@{$ALN1{mapping}{$chr}{$pos}};
      }
    }
    @ALN1 = sort { sort_aln($a,$b) } @ALN1;

    my @ALN2=();
    foreach my $chr (keys %{$ALN2{mapping}}) {
      foreach my $pos (keys %{$ALN2{mapping}{$chr}}) {
	push @ALN2,\@{$ALN2{mapping}{$chr}{$pos}};
      }
    }
    @ALN2 = sort { sort_aln($a,$b) } @ALN2;
    
    # loop on alignments to well paired reads and generates all mate pairs (even on different chromosomes)
    foreach my $aln1 (@ALN1) {
      foreach my $aln2 (@ALN2) {
	# here we have to compatible alignments
	my %entry1_bis=%entry1;
	my %entry2_bis=%entry2;
	# keep mapping info
	foreach my $f qw(RNAME POS MAPQ CIGAR SEQ QUAL) {
	  $entry1_bis{$f}=$aln1->[$Fields{$f}];
	  $entry2_bis{$f}=$aln2->[$Fields{$f}];
	}
	# keep tags
	my $n=scalar(@{$aln1})-1;
	$entry1_bis{TAG}=join("\t",@{$aln1}[$Fields{TAG}..$n]);
	$n=scalar(@{$aln2})-1;
	$entry2_bis{TAG}=join("\t",@{$aln2}[$Fields{TAG}..$n]);
	
	# check strand
	my $reverse1=$aln1->[$Fields{FLAG}] & BAM_FREVERSE;
	my $reverse2=$aln2->[$Fields{FLAG}] & BAM_FREVERSE;
	
	$entry1_bis{FLAG}|= BAM_FMREVERSE if ($reverse2); # next read reversed
	$entry2_bis{FLAG}|= BAM_FMREVERSE if ($reverse1); # next read reversed
	$entry1_bis{FLAG}|= BAM_FREVERSE if ($reverse1); # read reversed
	$entry2_bis{FLAG}|= BAM_FREVERSE if ($reverse2); # read reversed

	if ($aln1->[$Fields{RNAME}] eq $aln2->[$Fields{RNAME}]){ # same chromosomes
	  $entry1_bis{RNEXT} = '=';
	  $entry2_bis{RNEXT} = '=';

	  # compute TLEN (from bam_mate.c)
	  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	  #cur5 = (cur->core.flag&BAM_FREVERSE)? bam_calend(&cur->core, bam1_cigar(cur)) : cur->core.pos;
	  my $cur5 = $reverse2? bam_calend($aln2->[$Fields{POS}], $aln2->[$Fields{CIGAR}]) : $aln2->[$Fields{POS}];
	  #pre5 = (pre->core.flag&BAM_FREVERSE)? bam_calend(&pre->core, bam1_cigar(pre)) : pre->core.pos;
	  my $pre5 = $reverse1? bam_calend($aln1->[$Fields{POS}], $aln1->[$Fields{CIGAR}]) : $aln1->[$Fields{POS}];
	  #cur->core.isize = pre5 - cur5; pre->core.isize = cur5 - pre5;
	  $entry2_bis{TLEN} = $pre5 - $cur5;
	  $entry1_bis{TLEN} = $cur5 - $pre5;
	  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	  my $isize=abs($entry1_bis{TLEN});
	  if (($Values{lmin} <= $isize) && ($isize <= $Values{lmax}) && ($Values{same_strand}?$reverse1 == $reverse2:$reverse1 != $reverse2)) {
	    # insert size compatible with what expected and reads in correct strands
	    $entry1_bis{FLAG}|=BAM_FPROPER_PAIR; # each fragment aligned properly
	    $entry2_bis{FLAG}|=BAM_FPROPER_PAIR;
	  }
	}
	else { # different chromosomes
	  $entry1_bis{RNEXT} = $aln2->[$Fields{RNAME}];
	  $entry2_bis{RNEXT} = $aln1->[$Fields{RNAME}];
	}

	$entry1_bis{PNEXT} = $aln2->[$Fields{POS}];
	$entry2_bis{PNEXT} = $aln1->[$Fields{POS}];
	
	print_entry(\%entry1_bis);
	print_entry(\%entry2_bis);
      }
    }
  }

  #---------- load next alignments if matched current sequence
  %ALN1=read_aln('S1',\$aln1) if (exists($ALN1{qname}) && ($fastq1{ac} eq $ALN1{qname}));
  %ALN2=read_aln('S2',\$aln2) if (exists($ALN2{qname}) && ($fastq2{ac} eq $ALN2{qname}));
}


# close files
close(F1);
close(F2);
close(S1);
close(S2);

#============================================================

=head1 NAME

samArrange.pl

=head1 SYNOPSIS

samArrange.pl [-h|options]

=head1 OPTIONS

=over 8

=item B<-help>

Print a brief help message and exits.

=item B<-man>

Prints the manual page and exits.

=item B<-f1>

Left fragment fastq file

=item B<-f2>

Right fragment fastq file

=item B<-s1>

Left fragment BAM/SAM alignment file. Sequences in alignments lines must be in the SAME ORDER than in the fastq file. Some sequences can be absent from alignment file, and thus a unmapped alignment line will be generated.

=item B<-s2>

Right fragment BAM/SAM alignment file. Can be same BAM/SAM as -s1, but reads must have read number flag info. Same remark for sequence ORDER than for s1.

=item B<-S>

Input alignment files are in SAM format (default is considered to be BAM)

=item B<-remove_numbering>

Remove /1 or /2 at the end of accession number in fastq files

=item B<-lmin>

Expected min size for insert (default 100)

=item B<-lmax> 

Expected max size for insert (default 500)

=item B<-same_strand>

Two fragments have to be both on the same strand to be considered as properly aligned (plus insert size consideration).
The default is to have oposite strands.

=back

=head1 DESCRIPTION

Gather two SAM file and try to produce paired alignment.
When paired sequences have been mapped with a software not allowing paired alignment,
we have two SAM alignment files that have to be combined to find paired sequences compatible alignments.
This program takes the two fastq initial files and the two resulting SAM alignment files.
It tries to produce a SAM output aligment where flags are set when compatible alignments exist.
Two fragments are properly aligned if they are on the same chromosome, and if the insert length is between lmin and lmax,
and the two fragments are oposite strand (default) or same strand (option same_strand)

=head1 AUTHORS

Patrice DEHAIS

=head1 CONTACT

Questions can be posted to the sigenae mailing list:
sigenaesupport@jouy.inra.fr

=head1 DATE

2013

=cut

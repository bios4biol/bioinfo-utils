#!/usr/local/bioinfo/bin/python2.5
# -*- coding: utf-8 -*-

#
# Counting 
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'beta'

import re, os, time, sys
from Bio import SeqIO
from tempfile import NamedTemporaryFile


def parseCond(filename):
    """return the content of a given file as a table
    split with " " for Subini, no matter for the others
    """
    t=[]
    f=open(filename,'r')
    for l in f:
        t.append(l.split(' ')[0].replace('\n',''))
    return t

def parseFastaCond(filename):
    """return the content of a given file as a table
    split with " " for Subini, no matter for the others
    """
    t=[]
    f=open(filename,'r')
    for seq in SeqIO.parse(f,"fasta"):
        t.append(seq.id)
    return t

def findCond(sequence, condF, condE):
    """search the given sequence in the condE dict of regular expression, then in the condF matrix of the EST.
    Then return the name of the found condition.
    """
    for condition in condE.keys():
        for exp in condE[condition]:
            if exp.search(sequence)!=None:
                return condition #name of the condition
    if condF.has_key(sequence):
        return condF[sequence]
    else:
        return "Other" #if not founded
    
def strTab(table):
    for i in range(len(table)):
        table[i]=str(table[i])
    return '\t'.join(table)

def main(contigs,params,annotations,output,verbose=True,othersFile=None):
    """(contigs, params)
    params : condition experiences
    Comptage par contig du nombre d'ESTs des différentes conditions :
    - soit donnees par des fichiers
    - soit donnees par les nom d'EST 
    sortie: dictionnaire cle=contig valeur=table des comptes"""
    checkpoint=28000 #prompt a dot for each 28 000 lines
    conditions=[] #table of the condition names
    condE={} #dict of regular expressions nom:[regexp]
    condF={} #dict of tables of EST for each condition from file
    if verbose:
        start=time.time()
        print"_\nparsing parameters...", ;sys.stdout.flush()
    f=open(params,'r')
    totalParams=len(f.readlines())
    f.seek(0)
    line=f.readline() #ignore first line
    cptr=0
    for line in f:
        cptr+=1
        if verbose and cptr%(totalParams/9)==0:
            sys.stdout.write('.') ; sys.stdout.flush()
        try:
            [nom,type,valeur]=line.split("\t")
        except ValueError:
            print"\n\nParameter file not formatted as:\n\nHeader \nCondition_1_name\tfile\tpath_filename\nCondition_2_name\texp    ^FZZC9SK01"
            sys.exit(1)
        valeur=valeur.replace('\n','') 
        if type=="exp" and not condE.has_key(nom):
            condE[nom]=[re.compile(valeur)] #regexp object
        elif type=="exp" and condE.has_key(nom):
            condE[nom].append(re.compile(valeur)) #regexp object
            
        elif type=="file": #condition from file
            for seq in parseCond(valeur): #list
                condF[seq]=nom #dict of {EST:condition_name}
        elif type=="fasta":
            for seq in parseFastaCond(valeur): #list
                condF[seq]=nom #dict of {EST:condition_name}
        else:
            print "\n\nBad parameters.txt format: type",type,"uncorrect"
            sys.exit(1)
    f.close()
    conditions=condE.keys() + list(set(condF.values())) + ["Other"]    
    if verbose:
        step1=time.time()
        print "done in",step1-start,"seconds\n_\nparsing annotations...", ;sys.stdout.flush()
    annotationFile=open(annotations,'r')
    annotationData={}
    for line in annotationFile:
        l=line.split('\t')
        annotationData[l[1]]='\t'.join(l[2:-1]) #rm "index" and contig_name
    try:
        headAnnotation=annotationData['name']
    except ValueError:
        if verbose:
            print "\n\nIncorrect format for the annotation file,",annotations,"Header: index\tname\tannotations\nid1\tContig1\tannotations about contig1"
        sys.exit(1)

    annotationFile.close()
    if verbose:
        step2=time.time()
        print "done in",step2-step1,"seconds\n_\npreprocess the file linking contig and EST...", ;sys.stdout.flush()
    
    contigSeqTmp=NamedTemporaryFile(mode='w+b', prefix='tmp')
    os.system("tail -n +1 "+contigs+"| sort -o "+contigSeqTmp.name) #remove header, then sort

    if verbose:
        step3=time.time()
        print "done in",step3-step2,"seconds\nparsing contig_seq file", ;sys.stdout.flush()
    result=open(output,'w')    
    if othersFile:
        other=open(othersFile,'w')
    #header
    header='Contig_name\t'
    for c in conditions:
        header+=c+'\t'
    result.write(header+headAnnotation+'\n')
    #initialization for a new line
    table=[] ; current=""
    for i in conditions: 
        table.append(0)   
        
    if verbose:
        ptr=0
    for line in contigSeqTmp.file:
        if verbose:
            ptr+=1
            if ptr%checkpoint==0:
                sys.stdout.write('.')

        try:
            [contig,EST]=line.split('\t')
        except ValueError:
            print"\n\nUncorrect file format for",contigs,"\nSHould be:\nHeader\ncontig_name\tEST_name"
            sys.exit(1)
        if contig=="name": continue #ignore header
            
        EST=EST.replace('\n','') ; contig=contig.replace('\n','')
        condition=findCond(EST, condF, condE)
        
        if condition=="Other" and othersFile: other.write(contig+'\t'+EST+'\n')

        if current!=contig: #new block
            if current!="": #NOT at the beginning              
                result.write(current+'\t'+strTab(table)+'\t'+annotationData[current]+'\n')
            #then, initialize
            current=contig ; table=[]               #
            for i in conditions: table.append(0)    # initialization
        
        for i in range(len(conditions)):
            if condition==conditions[i]:
                try:
                    table[i]+=1
                except IndexError:
                    print "\n\nIndex error on: table",table, "est",EST,"contig",contig,"condition",condition
    result.write(current+'\t'+strTab(table)+'\t'+annotationData[current]) #write the last one
    result.close()
    if othersFile: 
        other.close()
    if verbose:
        print'\n_\nend, total time: ',time.time()-start,"seconds"
    return 0

def which (program):
    """
    Return if the asked program exist in the user path
      @param options : the options asked by the user
    """
    import os
    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None
def counting_depts_error():
    """
    Return the list of dependencies missing to run the pyrocleaner
    """
    error = ""
#    if which("blastall") == None:
#        error += " - blastall is missing\n"
    if error != "":
        error = "Cannot execute counting.py, following dependencies are missing :\n" + error + "Please install them!"
    return error

if __name__ == "__main__":

    from optparse import OptionParser, OptionGroup
    parser = OptionParser(usage="usage: %prog -c filename -p filename -a filename -r filename")
    parser.description="This program counts the contig occurences in the given conditions:\nContigs\tCond1\tCond2\t...\tAnnotations\ncontig1\tint\tint\t...\tannotations of the contig1\n\nUsing the parameters file to identify the condition's sequences and contig_seq file formatted as follow:"
    required=OptionGroup(parser, "Required options")
    required.add_option("-c", "--contig_seq", dest="contigFile",
                        help="tabulated file linking contig to sequence in 2 columns : contig_name <tabulation> sequence_name ; with 1 line of header", metavar="FILE")
    required.add_option("-p", "--parameters", dest="paramFile",
                        help="tabulated file giving for each condition the way to keep their sequences.Tabulated in 3 columns (name, type, value) and 1 line of header. Types allowed: 'file', 'fasta' and 'exp'", metavar="FILE")
    required.add_option("-a", "--annotations", dest="annotFile",
                        help="tabulated file of annotations about the contigs: name,index,annotations. Only 1 line of header starting by 'name'", metavar="FILE")
    required.add_option("-r", "--results", dest="resultFile",
                        help="write the result matrix in FILE", metavar="FILE")
    facultative=OptionGroup(parser, "Facultative options")
    facultative.add_option("-v", "--verbose",
                           action="store_true", dest="verbose", default=False,
                           help="print all status messages to stdout")   
    facultative.add_option("-o", "--others",
                           action="store", dest="otherFile", metavar="FILE",
                           help="write sequences of unknown condition in FILE")
    parser.add_option_group(required) ; parser.add_option_group(facultative)
    
    (options, args) = parser.parse_args()
    if not counting_depts_error():
    	contigs=options.contigFile ; parameters=options.paramFile ; annotations=options.annotFile
    	results=options.resultFile ; verbose=options.verbose ; others=options.otherFile
    	if not contigs or not parameters or not annotations or not results:
            print"Please give all the required fields"
            parser.print_help()
            sys.exit(1)
    	main(contigs,parameters,annotations,results,verbose,others)
                                    

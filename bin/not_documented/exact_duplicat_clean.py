#!/usr/bin/python
##!/usr/local/bioinfo/bin/python2.5

#
# duplicat_analyze 
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'

import os, sys, string, gzip, tempfile
from Bio import SeqIO
from optparse import *


def version_string ():
    """
    Return the exact_duplicat_clean version
    """
    return "exact_duplicat_clean " + __version__


def get_seqs (options):
    """
    Converts input seqs in a BioPython seq table
      @param options : the options asked by the user
    """
    # First get fasta or/and qual input files
    if options.format == "sff":
        sff_file = options.input_file
        
        if sff_file.endswith(".gz"):
            '''Gunzip the given file and then remove the file.'''
            r_file = gzip.GzipFile(sff_file, 'r')
            write_file = string.rstrip(sff_file, '.gz')
            w_file = open(write_file, 'w')
            w_file.write(r_file.read())
            w_file.close()
            r_file.close()
            sff_file = write_file
            
        base = os.path.basename(sff_file)
        fasta_file = os.path.join(options.output, base + '.fasta')
        qual_file = os.path.join(options.output, base + '.qual')
        xml_file = os.path.join(options.output, base + '.xml')
        format = "fasta"
        if not os.path.isfile(fasta_file) or not os.path.isfile(qual_file) or not os.path.isfile(xml_file):
            #First extract the sff file to fasta, qual and xml file
            cmd = "sff_extract.py -c -s " + fasta_file + " -q " + qual_file + " -x " + xml_file + " " + sff_file + " >> " + options.log_file
            os.system(cmd)
        else :
            log = open(options.log_file, "a+")
            log.write(fasta_file + ", " + qual_file + ", " + xml_file + " already exist, working on existing files\n")
            log.close()
    elif options.format == "fasta" or options.format == "fastq":
        format = options.format
        fasta_file = options.input_file
    else :
        print "Error : " + options.format + " is not a supported format!"
        sys.exit(1)

    # If the fasta file is gziped
    if fasta_file.endswith(".gz"):
        seqs = []
        for record in SeqIO.parse(gzip.open(fasta_file), format) :
            seqs.append(record)
    else :
        seqs = []
        for record in SeqIO.parse(open(fasta_file), format) :
            seqs.append(record)
     # Returns the table of sequences
    return [seqs]


def clean_tmp_files (options):
    """
    Clean the output directory
      @param options : the options asked by the user
    """
    for files in glob.glob(os.path.join(options.output, "*")):
        if not os.path.splitext(files)[0].endswith(".clean") and not files.endswith("log") and files != os.path.join(options.output, options.input_file):
            os.remove(files)
        if files.endswith(".cross_match_input.fasta.log"):
            os.remove(files)

def which (program):
    """
    Return if the asked program exist in the user path
      @param options : the options asked by the user
    """
    import os
    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

def reads_to_sff (sff_file, seqs, output_file):
    """
    Extract seqs reads from the sff_file
      @dependences sfffile : the sff software is required to execute this function
      @param sff_file      : the input sff file
      @param seqs          : table of seqs to extract   
      @param output_file   : the name of the output sff file
    """
    # First creates the to_write file
    tmp_file = os.path.join(os.path.dirname(output_file), "reads_to_sff.txt")
    to_write_file = open(tmp_file, 'wr')
    for read in seqs:
        to_write_file.write(read.id + "\n")
    to_write_file.close()
    # Use the sfffile tools
    cmd = "sfffile -i " + tmp_file + " -o " + output_file + " " + sff_file
    os.system(cmd)
    # Clean temporary files
    try: os.remove(tmp_file)
    except: pass
    
def get_duplicat_analyze_depts_error():
    """
    Return the list of dependencies missing to run get_duplicat_analyze
    """
    error = ""
    if which("sff_extract.py") == None:
        error += " - sff_extract.py\n" 
    if error != "":
        error = "Cannot execute get_duplicat_analyze, following dependencies are missing :\n" + error + "Please install them before to run get_duplicat_analyze!"
    return error

def get_sort_sequences_file(options,seqs):
    """
        return a file which contain one sequence pair line and sorted with unix.
        @param options :  the options asked by the user
    """
    f = NamedTemporaryFile()
    fh = open (f, "w")
    for read_record in seqs :
        if options.length == 0 :
            fh.write(read_record.seq + "\n" + read_record.id)
        else :
            fh.write(read_record.seq[0:options.length] + "\n" + read_record.id)
    fh.close()
    fo = NamedTemporaryFile(delete=False)
    os.system("sort " + f + " > " + fo )
    return fo
            
def get_duplicate_seq (temp_file) :
    """
        return a list of duplicates sequence.
        @param temp_file :  the tempory sorted file
    """             
    previous_seq=""
    to_del={}
    for line in open (temp_file, "r"):
        (seq, id)=line.split(" ")
        if seq == previous_seq :
            to_del[id]=1
        previous_seq=seq
    return to_del

if __name__ == "__main__":

    if not get_duplicat_analyze_depts_error():
        parser = OptionParser(usage="Usage: duplicat_analyze.py -i FILE [options]")

        usage = "usage: %prog -i file -o output -f format"
        desc = " TODO \n"\
               " ex : exact_duplicat_clean.py -i file.sff -o /path/to/output -f sff"
        parser = OptionParser(usage = usage, version = version_string(), description = desc)

        igroup = OptionGroup(parser, "Input files options","")
        igroup.add_option("-i", "--in", dest="input_file",
                          help="The file to clean, can be [sff|fastq|fasta]", metavar="FILE")
        igroup.add_option("-f", "--format", dest="format",
                          help="The format of the input file [sff|fastq|fasta] default is sff", type="string", default="sff")
        igroup.add_option("-q", "--qual", dest="qual_file",
                          help="If format is fasta you can specified fasta file", type="FILE")

        parser.add_option_group(igroup)
        
        ogroup = OptionGroup(parser, "Output files options","")
        ogroup.add_option("-o", "--out", dest="output",
                          help="The output directory")
        ogroup.add_option("-g", "--log", dest="log_file",
                          help="The log file name (default:exact_duplicat_clean.log)", metavar="FILE", default="exact_duplicat_clean.log")
        parser.add_option_group(ogroup)
        
        pgroup = OptionGroup(parser, "Processing options","")
        pgroup.add_option("-l", "--length", dest="length",
                          help="Length to check if sequence is identical", type="int", default=0)
        parser.add_option_group(pgroup)
        (options, args) = parser.parse_args()
     
        if options.input_file == None or options.output == None and  options.format == None:
            parser.print_help()
            sys.exit(1)    
        else:
            
            global log
            log = open(os.path.join(options.output,options.log_file), "w")
            log.write("## Start processing (" + str(datetime.datetime.now()) + ")\n")
            log.write("## Length parameter : "+ options.length +" \n")
            
            [seqs] = get_seqs(options)
            before_cleaning = len(seqs) 
            # 1 - set sequence in one line and sort it with unix
            temp_file = get_sort_sequences_file(options, seqs)

            # 2 - identifie the duplicates sequences 
            list_seq_to_delete = get_duplicate_seq(temp_file)

            # write output
            for seq_record in seqs:
                if list_seq_to_delete.has_key(seq_record.id) :
                    seqs.pop(seq_record)

            after_cleaning = len(seqs) 
            if options.input_file.endswith(".gz"):
                ifile = string.rstrip(options.input_file, '.gz')
            else : ifile = options.input_file
            if options.clean_pairends: output = os.path.join(options.output, os.path.splitext(os.path.basename(ifile))[0]+".pairends.clean")
            else : output = os.path.join(options.output, os.path.splitext(os.path.basename(ifile))[0]+".clean")
            output += "." + options.format
            if options.format == "sff" :
                if options.input_file.endswith(".gz"):
                    sfffile = os.path.join(options.output, string.rstrip(os.path.basename(options.input_file), '.gz'))
                else :
                    sfffile = options.input_file
                if which("sfffile") == None:
                    fastq = open(output, "w")
                    SeqIO.write(seqs, fastq, "fastq")
                    fastq.close()
                else : 
                    reads_to_sff(sfffile, seqs, output)
                    # Clean temporary files
                    if options.input_file.endswith(".gz"):
                        os.remove(sfffile)
                        
            elif options.format == "fasta" or options.format == "fastq":
                fasta = open(output, "w")
                SeqIO.write(seqs, fasta, options.format)
                fasta.close()
                if options.format == "fasta" and options.qual_file != None:
                    qual = open(os.path.splitext(output)[0]+".qual", "w")
                    SeqIO.write(seqs, qual, "qual")
                    qual.close()
            
            log.write("## Before cleaning :  (" + str(before_cleaning) + ")\n") 
            log.write("## After cleaning :  (" + str(after_cleaning) + ")\n")
            log.write("## Ended with code 0 (" + str(datetime.datetime.now()) + ")\n")
            log.close()
            clean_tmp_files(options)
            sys.exit(0)
    else : 
        print get_duplicat_analyze_depts_error()
        sys.exit(1)
#! /usr/bin/perl
# $Id$

=pod
=head1 NAME

 completeBedCoverage.pl

=head1 DESCRIPTION

 Afficher la couverture pour chaque base dans le genome. Ce programme a pour 
 vocation de completer les positions manquantes dans genomeCoverageBed.
 En effet si un chromosome/region est totalement absent des reads d'un BAM,
 cette region ne sera pas conservee dans genomeCoverageBed.

=head1 SYNOPSIS

 completeBedCoverage.pl COVERAGE REF_SIZES

=head1 ARGUMENTS

 1-    Fichier de couverture (genomeCoverageBed).
 2-    Fichier tabule contenant la taille de chaque region du genome.
       (ex: chr1<tab>1250)

=head1 OPTIONS

=head1 VERSION

 1.2

=head1 DEPENDENCIES

 Aucune.

=head1 AUTHOR

 Escudie Frederic - Plateforme genomique Toulouse

=cut


#############################################################################################################################
#
#		LIBRAIRIES
#
#############################################################################################################################
use strict ;
use warnings ;
use Getopt::Long ;
use Pod::Usage ;


#############################################################################################################################
#
#		FONCTION PRINCIPALE
#
#############################################################################################################################
MAIN:
{
	my $coverage_file = $ARGV[0] ;
	my $sizes_file    = $ARGV[1] ;
	my $help          = 0 ;

	#Gestion des param�tres
	GetOptions(
	            "help|h"            => \$help
	);

	if( $help || @ARGV != 2 )
	{
		pod2usage(
		           -sections => "SYNOPSIS|ARGUMENTS|DESCRIPTION|VERSION",
		           -verbose => 99
		) ;
	}

	open(my $FH_SIZES, $sizes_file) or die "Impossible d'ouvrir le fichier ".$sizes_file." : ".$! ;
	open(my $FH_COVERAGE, $coverage_file) or die "Impossible d'ouvrir le fichier ".$coverage_file." : ".$! ;

	#R�cup�rer les informations sur la premi�re position du fichier de couverture
	my $coverage_line = <$FH_COVERAGE> ;
	chomp $coverage_line ;
	my ($coverage_region, $coverage_pos, $coverage) = split(/\t/, $coverage_line) ;

	#Pour chaque r�gion de la r�f�rence
	while( my $sizes_line = <$FH_SIZES> )
	{
       		chomp $sizes_line ;
        	my ($region_name, $region_size) = split(/\t/, $sizes_line) ;
        	#Si la r�gion courante sur le fichier de tailles n'est pas la r�gion courante sur le fichier de couverture
        	if( $coverage_region ne $region_name )
        	{
                	#Ins�rer les lignes de couverture nule de la r�gion
                	for( my $i = 1 ; $i <= $region_size ; $i++ )
                	{
                 	       print $region_name."\t".$i."\t0\n" ;
                	}
        	}
        	#Si la r�gion courante sur le fichier de tailles correspond � la r�gion courante sur le fichier de couverture
        	else
        	{
                	#Tant que l'on ne change pas de r�gion
                	while( $coverage_region eq $region_name )
                	{
                        	print $coverage_line."\n" ;

                        	$coverage_line = <$FH_COVERAGE> ;
                        	if(defined $coverage_line)
                        	{
                                	chomp $coverage_line ;
                                	($coverage_region, $coverage_pos, $coverage) = split(/\t/, $coverage_line) ;
                        	}
                        	else
                        	{
                                	$coverage_region = "" ;
                                	$coverage_pos    = "" ;
                                	$coverage        = "" ;
                        	}
                	}
        	}
	}

	close( $FH_SIZES ) ;
	close( $FH_COVERAGE ) ;
}
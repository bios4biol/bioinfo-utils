#!/usr/bin/perl -I../lib


BEGIN
{
        my ($dir,$file) = $0 =~ /(.+)\/(.+)/;
        unshift (@INC,"$dir/../lib");
}


use strict;
use warnings;
use ParamParser;
use Bio::Seq;
use Bio::SeqIO;


MAIN:
{
        # L
        my $o_param = New ParamParser('GETOPTLONG',"contig=s","output=s","max_len=s");
        $o_param->SetUsage(my $usage=sub { &Usage(); } );

	$o_param->AssertFileExists('contig');
	$o_param->SetUnlessDefined('output',$o_param->Get("contig").".fasta");
	$o_param->SetUnlessDefined('max_len',200);
	my $max =$o_param->Get('max_len');

        open (OUT, ">".$o_param->Get("output")) or die("Erreur decriture >".$o_param->Get("output")."<\n");
        # read fasta file
        my $seq = Bio::Seq->new();
        my $in = Bio::SeqIO->new( -file => $o_param->Get("contig"), '-format' => 'Fasta' );
	
        while( $seq = $in->next_seq())
        {
		my $id=$seq->id();
		my $len=len($seq->seq());

		if ( $len < $max )
			{
				print OUT ">".$id."\n";
				print OUT $seq->seq()."\n";
				
			}
        }
       close (OUT);
}

=item Usage

        $o_param->Usage();
        Print the usage of the program

=cut

sub Usage
{
print STDERR<<END
Usage:$0
Print in output file the sequence which len < min_len

[Mandatory]
        --contig 	filename     contig fasta file
	--max_len	integer      maximal len
[Optionnal]
        --help                       this message
        --output        filename     output file
END
}

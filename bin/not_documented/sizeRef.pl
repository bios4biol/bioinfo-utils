#!/usr/bin/perl
 

############################################################################################################################
#
#		LIBRAIRIES
#
#############################################################################################################################
use strict ;
use warnings ;
use Getopt::Long ;
use Pod::Usage ;


#############################################################################################################################
#
#		FONCTION PRINCIPALE
#
#############################################################################################################################
MAIN:
{
	my $genome_file = $ARGV[0] ;
	my $out_file    = $ARGV[1] ;
	my $separator   = "\t" ;
	my $help        = 0 ;

	#Gestion des parametres
	GetOptions(
                "separator|s=s" => \$separator,
	            "help|h"        => \$help
	);
	
	if( $help || @ARGV != 2 )
	{
		pod2usage(
		           -sections => "SYNOPSIS|ARGUMENTS|OPTIONS|DESCRIPTION|VERSION",
		           -verbose => 99
		) ;
	}

        open(my $FH_GENOME, $genome_file) or die "[ERREUR] Impossible d'ouvrir le fichier ".$genome_file." : ".$! ;
        open(my $FH_SIZES, ">", $out_file) or die "[ERREUR] Impossible de creer le fichier ".$out_file." : ".$! ;

        my $region_name = "" ;
        my $region_length = 0 ;
        while( <$FH_GENOME> )
        {
                my $line = $_ ;
                chomp $line ;

		#Si la ligne courante est l'en-tete d'une region
                if( $line =~ /^\>([^\s]+)/ )
                {
			#Si on est pas sur la premiere ligne du fichier
                        if( $region_name ne "" )
                        {
                                print $FH_SIZES $region_name.$separator.$region_length."\n" ;
                        }
                        $region_name   = $1 ;
                        $region_length = 0 ;
                }
		#Si la ligne courante est une partie d'une sequence
                else
                {
                        my @nucleotids = split( //, $line ) ;
                        $region_length += scalar( @nucleotids ) ;
                }
        }

        if( $region_name ne "" )
        {
                print $FH_SIZES $region_name.$separator.$region_length."\n" ;
        }

        close($FH_GENOME) ;
        close($FH_SIZES) ;
}

=head1 SYNOPSIS

sizeRef.pl GENOME OUT_FILE [-s SEPARATOR]

=head1 DESCRIPTION

Creer un fichier indiquant la taille de chaque regions du fichier fasta.

=head1 AUTHORS

Escudie Frederic

=head1 VERSION

1

=head1 DATE

05/2012

=head1 KEYWORDS

bidule bidou

=head1 ARGUMENTS

 1-    Fichier fasta contenant les sequences de reference.
 2-    Fichier de sortie.

=head1 OPTIONS

 -s, separator    Separateur entre le nom de la region et sa taille [Defaut : 
                  tabulation].


=cut
#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

#Importation des librairies
from optparse import *
import os, sys, gzip,string,datetime
from Bio import SeqIO
from Bio.Alphabet import generic_dna
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation, Reference
from Bio.Seq import Seq
from BCBio import GFF

# fastq_extract : Copyright (C) 2009 INRA
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'

#Fonctions

#Donne la version du programme
def version_string ():
    """
    Return the extractfeatures version
    """
    return "extractfeatures.py " + __version__

def get_sub_seq_record(record,feature,log):
    """
    Permet de recuperer la sequences entiere correspondant a la feature
      @param record : SeqRecord correspondant a la sequence complete
      @param feature: SeqFeature bornes a extraire
      @return: SeqRecord de la sous sequence sans feature
    """
    if feature.id=="<unknown id>" or feature.id=="": # Si la feature n'a pas d'id
        log.write("La feature ne possede pas d'id => On lui en cree un.\n")
        # On lui cree un id avec le numero d'accession, le debut et la fin de sa location , son type de feature
        feature.id=record.id+"_"+str(feature.location.nofuzzy_start+1)+"_"+str(feature.location.nofuzzy_end)+"_"+feature.type
    log.write("On cree un nouveau SeqRecord contenant pour sequence la sequence de la feature.\n")
    # On recupere dans seq la sequences entiere correspondant a  l'id de la feature
    seq=SeqRecord(Seq(str(record.seq[feature.location.nofuzzy_start:feature.location.nofuzzy_end]),generic_dna ), id=str(feature.id))
    # On transmet � Seq la description de record
    seq.description=record.description
    # on retourne le SeqRecord seq
    return(seq)

def MergeFeatures (record, strand,log):
    """
    Permet d'effectuer un traitement de fusion en cas de chevauchement entre 2 features et de retourner un record contenant l'ensemble des features fusionnees sur le brin specifie
      @param record : SeqRecord correspondant a la sequence complete
      @param strand: string [0|1|-1] 
      @return: SeqRecord contenant les features fusionnees 
    """
    log.write("Fusion des features de la sequence : "+ record.id  +"\n")
    couple_feature_load={}
    i=0
    j=0
    cpt=0

    while i < len(record.features):# Tant que l'on n'a pas consulte toutes les features du record
        j=i+1
        while j < len(record.features):# Tant que l'on n'a pas consulte toutes les features du record
            if i!=j:# Si les 2 indices testees ne sont pas les memes
                # Si la record.features[i] est incluse dans la record.features[j]
                if (record.features[i].location.nofuzzy_start>=record.features[j].location.nofuzzy_start) and (record.features[i].location.nofuzzy_end<=record.features[j].location.nofuzzy_end):
                    # On supprime la feature � l'indice i du record
                    log.write("DEL "+record.features[i].id+" -> include into "+record.features[j].id+"(1)\n")
                    del record.features[i]
                    i=i-1 
                    j=len(record.features)
                # Sinon si la record.features[i] chevauche par le start la record.features[j]
                elif (record.features[i].location.nofuzzy_start>=record.features[j].location.nofuzzy_start) and (record.features[i].location.nofuzzy_start<=record.features[j].location.nofuzzy_end) and (record.features[i].location.nofuzzy_end>=record.features[j].location.nofuzzy_end):
                    log.write("Start "+record.features[j].id+" end "+record.features[i].id+"; update it (2)\n")
                    # On modifie le start de la feature i
                    record.features[i]=SeqFeature(location=FeatureLocation(record.features[j].location.nofuzzy_start,record.features[i].location.nofuzzy_end),type='join_'+record.features[j].type+"_"+record.features[i].type,strand=record.features[j].strand,id='join '+record.features[j].id+"_"+record.features[i].id)
                    # On supprime la feature j
                    del record.features[j]
                    j=j-1
                # Sinon si la record.features[i] chevauche par le end la record.features[j]
                elif (record.features[i].location.nofuzzy_start<=record.features[j].location.nofuzzy_start) and (record.features[i].location.nofuzzy_end<=record.features[j].location.nofuzzy_end) and (record.features[i].location.nofuzzy_end>=record.features[j].location.nofuzzy_start):
                    log.write("Start "+record.features[i].id+" end "+record.features[j].id+"; update it (3)\n")
                    # On modifie le end de la feature i
                    record.features[i]=SeqFeature(location=FeatureLocation(record.features[i].location.nofuzzy_start,record.features[j].location.nofuzzy_end),type='join_'+record.features[i].type+"_"+record.features[j].type,strand=record.features[i].strand,id='join '+record.features[i].id+"_"+record.features[j].id)
                    # On supprime la feature j
                    del record.features[j]
                    j=j-1
                # Sinon si la record.features[j] est incluse dans la record.features[i]
                elif (record.features[i].location.nofuzzy_start<=record.features[j].location.nofuzzy_start) and (record.features[i].location.nofuzzy_end>=record.features[j].location.nofuzzy_end):
                    log.write("DEL "+record.features[j].id+" -> include into "+record.features[i].id+"(4)\n")
                    # On supprime la feature � l'indice j du record
                    del record.features[j]
                    j=j-1
                # Sinon on a aucun chevauchement
                else:
                    log.write("Aucun chevauchement pour le couple "+record.features[i].id+"_et_"+record.features[j].id+"\n")
            j=j+1
        i=i+1
    # On retourne record
    log.write("INTO MERGE "+ str(len(record.features))+"\n")
    return(record)

def sortFeatures(record): 
    """
    Trie les features a l'interieur du record en fonction de leur position de depart
      @param record : SeqRecord correspondant a la sequence complete
      @return: SeqRecord contenant les features triees
    """
    dictFeatures={} # On declare un dictionnaire de features qui contiendra la position de depart d'une feature et la feature correspondante
    Tab=[] # On declare une liste qui contiendra la liste des features dans l'ordre
    for feature in record.features: # Pour l'ensemble des features presentent dans record
        if dictFeatures.has_key(feature.location.nofuzzy_start): # Si le dictionnaire a pour cle le start de la feature
            dictFeatures[feature.location.nofuzzy_start].append(feature)# On ajoute la feature a cette cle
        else:#Sinon
            dictFeatures[feature.location.nofuzzy_start]=[feature]#On cree une cle et on lui attribut la feature
    list_i=dictFeatures.keys()#On cree une liste correspondant au cle du dictionnaire
    list_i.sort()#On effectue un trie de cette liste
    for start in list_i:#Pour chaque start present dans la liste
        Tab+=dictFeatures[start]#On insere dans Tab la feature correspondant au start correspondant dans le dictionnaire 
    new_record=SeqRecord(seq=record.seq, id=record.id,description=record.description,features=Tab)# On cree un nouveau record avec pour attribut feature la liste Tab
    return (new_record)# On retourne le record new_record
    
def MaskSequences(record,whitchtype): 
    """
    Permet le masquage des sequences qui ne font pas parti des features demandees
      @param record : SeqRecord correspondant a la sequence complete
      @return: SeqRecord avec les parties de sequences non demandees masquees
    """
    seq=SeqRecord(seq=Seq("",generic_dna),id=record.id,name=record.name,description=record.description)# On cree un nouveau record
    prev_start=0 # On initialise a 0 la variable qui permettra de garde en memoire le start de chaque sequence de X
    if whitchtype=="type": # Si type
        for feature in record.features:# Pour l'ensemble des features presentent dans record
            # On cree la sequence de seq concatenant la sequence deja en memoire + "X" fois la distance entre les 2 features + la feature
            seq.seq=seq.seq+("X"*((feature.location.nofuzzy_start)-prev_start))+record.seq[feature.location.nofuzzy_start:feature.location.nofuzzy_end]
            # On assigne a la varible la valeur du end de la feature
            prev_start=feature.location.nofuzzy_end
        # On ajoute autant de "X" qu'il faut pour arriver a la longueur total de la sequence de record
        seq.seq=seq.seq +("X"*((len(record.seq)-len(seq.seq))))
    else: # Sinon notype
        for feature in record.features:# Pour l'ensemble des features presentent dans record
            # On cree la sequence de seq concatenant la sequence deja en memoire + "X" fois la distance entre les 2 features + la feature
            seq.seq=seq.seq+record.seq[prev_start:feature.location.nofuzzy_start]+("X"*((feature.location.nofuzzy_end)-(feature.location.nofuzzy_start)))
            # On assigne a la varible la valeur du end de la feature
            prev_start=feature.location.nofuzzy_end
        # On ajoute autant de "X" qu'il faut pour arriver a la longueur total de la sequence de record
        seq.seq=seq.seq +record.seq[prev_start:len(record.seq)]
    # On retourne la sequence de seq
    return (seq)

#Programme principal
if __name__ == "__main__":
    #Declarations des variables et du programme d'affichade d'aide pouvant etre demande par lignes de commandes
    parser = OptionParser(usage="Usage: extractseq2.py -i FILE [options]")
    usage = "usage: %prog -i genbank_or_EMBL_file"
    desc = " Extract from genbank or EMBL file the fasta sequence\n"
    parser = OptionParser(usage = usage, version = version_string(), description = desc)
    parser.add_option("-i", "--input", dest="input_file", help="The genbank file or EMBL file or fasta file(if fasta need gff file)", metavar="FILE")
    parser.add_option("-o", "--output", dest="fasta", help="The output fasta file", metavar="FILE")
    parser.add_option("-f", "--format", dest="format", help="The format file,can be [gb|embl|fasta]", metavar="FILE'S FORMAT")
    parser.add_option("-g", "--gff", dest="input_gff_file", help="The gff file (only if fasta file in input", metavar="FILE")
    parser.add_option("-t", "--type", dest="type", help="The ask features type, f1,f2,...", metavar="FEATURE(S)' TYPE")
    parser.add_option("-n", "--notype", dest="notype", help="The no ask features type, f1,f2,...", metavar="FEATURE(S)' TYPE")
    parser.add_option("-l", "--list", dest="list", help="The file's features list", default=False, action="store_true")
    parser.add_option("-m", "--mask", dest="mask", help="mask feature", default=False, action="store_true")
    parser.add_option("-s", "--strand", dest="strand", help="The feature strand (-1,0 or 1)", metavar="FEATURE(S)' STRAND")
    parser.add_option("-r", "--log", dest="log", help="The log file name (default:extractfeatures.log)", metavar="FILE", default="extractfeatures.log")
    parser.add_option("-d", "--delta", dest="delta", help="extract subrange of sequence for feature locations(-d -1000..10 means 1000 bases before start and 10 bases after end)", metavar="BASE")
    parser.add_option("-e", "--subfeature", dest="subfeature", help="extract subfeature", default=False, action="store_true")
    parser.add_option("-c", "--subregion", dest="subregion", help="extract subregion", default=False, action="store_true")  
    parser.add_option("-p", "--nomerge", dest="nomerge", help="nomerge", default=False, action="store_true")  
    (options, args) = parser.parse_args()
    
    # Si l'option delta est demande
    if options.delta!=None:
        tab_delta=[] # On cree un tableau
        tab_delta = options.delta.split("..") # On split l'option delta dans le tableau
        d_start=int(tab_delta[0]) # Start prend la valeur de la premi�re colonne du tableau
        d_end=int(tab_delta[1])# End prend la valeur de la seconde colonne du tableau
    else: # Sinon l'option delta n'est pas demande le start et le end prennent la valeur 0
        d_start=0
        d_end=0
        
    # Si le strand est demande
    if options.strand!=None:
        options.strand=int(options.strand) # On tranforme l'option stand en entier
    else: # Sinon
        options.strand=0 # Strand =0
        
    authorize_format = {"embl":1,"genbank":2,"fasta":3}   # Dictionnnaire des format autorise pour le fichier d'entree
    available_feature={} # Dictionnaire de l'ensemble des types de features du fichier d'entree
    mes_sequences_recup=[] # Tableau des sequences recuperer lorsqu'on parse le fichier d'entree
    mes_sequences=[] # Tableau des sequences que l'on va imprimer dans le fichier de sortie
    ask_features={} # Dictionnaire de l'ensemble des types de features demandees
    global log # on declare la variable log en global
    
    log = open(options.log, "w") # On ouvre le fichier log en ecriture et on l'attribut � la variable log
    log.write("## Start processing (" + str(datetime.datetime.now()) + ")\n") # Le traitement commence a tel heure
    
    if options.input_file != None and os.path.isfile(options.input_file): # S'il y a un fichier en entree
        log.write("Input file :"+options.input_file+"\n")
        
        if options.fasta == None : # Si l'on a pas specifie de nom  pour le fichier de sortie
            options.fasta=options.input_file+".out" # On lui donne le nom du fichier d'entree .out
        log.write("Output file : " +options.fasta+"\n")
        
        if options.format==None: # Si le format n'est pas specifie
            options.format="genbank" # Ce sera genbank par default
        log.write("Format:  "+options.format+".\n")
        
        if not authorize_format.has_key(options.format): # Si le format specifie ne fait pas parti du dictionnaire 
            log.write("Le format specifie ne fait pas partie des format pris en compte par le programme.\nFait d'execution du programme.\n")
            sys.stderr.write("The asked format is not valid\n")#On imprime le message suivant dans stderr 
            exit (1) # Et on quitte le programme
            
        if options.input_file.endswith(".gz"): # Si le fichier d'entree est zipe
            handle=gzip.open(options.input_file) # On le dezipe et on l'ouvre
            log.write("Fichier "+options.input_file+" dezipe et ouvert.\n")
        else : # Sinon
            handle=open(options.input_file) # On l'ouvre
            log.write("Fichier "+options.input_file+" ouvert.\n")
            
        tab_ask_features=[] # Tableau des types de features qui vont etre ecrites dans le fichier de sorties
        
        if options.type!=None : # S'il y a un ou des types specifies
            tab_ask_features = options.type.split(",") # On split la ligne de commande sur la virgule et on insere les resultats dans les cases du tableau
            whitchtype="type"
            
        if options.notype!=None:
            tab_ask_features = options.notype.split(",") # On split la ligne de commande sur la virgule et on insere les resultats dans les cases du tableau
            whitchtype="notype"
        
        # Si on a type ou notype de specifie
        if options.type!=None or options.notype!=None:
            log.write("Le dictionnaire des types demandes possede les valeurs: ")
            for atype in tab_ask_features: # transforme la list en hash
                ask_features[atype]=0 # On remplie le hash avec les types auxquels on affecte la valeur 0
                log.write("("+atype+") ")
            log.write("\n")
            
        input_seqs=[] # Tableau des records
        if (options.type==None) and (options.notype==None) and (options.list==False): # Si le type n'est pas specifie et que list n'est pas demande
            log.write("Pas de type demande, Pas de notype demande ,liste des features non demandee => Sequence transformer au format fasta.\n")
            output_handle = open(options.fasta, "w") # On ouvre le fichier de sortie en mode ecriture 
            log.write("Ouverture du fichier "+options.fasta+" pour ecriture.\n")
            records = SeqIO.parse(handle, options.format) # records prend pour valeur ce qui est parser dans le fichier en input en fonction du format
            count = SeqIO.write(records, output_handle, "fasta") # On ecrit les sequences entieres dans le fichier de sortie
            output_handle.close() # On ferme le fichier de sortie
        else: # Sinon
            if options.format=="fasta": # Si le format est fasta
                log.write("Analyse du fichier fasta.\n")
                if options.input_gff_file==None: # Si on ne possede pas de fichier gff
                    sys.stderr.write("Need gff file with this fasta file") # On imprime un message d'erreur
                    log.write("Absence de fichier gff pour terminer le traitement.\nFin de l'execution.\n")
                    parser.print_help()
                    sys.exit(1) # Et on quitte le programme
                else: # Sinon
                    seq_dict = SeqIO.to_dict(SeqIO.parse(handle, "fasta")) # On parse le fichier d'entree fasta et on stock dans la variable seq_dict
                    log.write("Recuperation de la/des sequence(s) contenue(s) dans le fichier fasta.\n")
                    handle.close() # On ferme le fichier d'entree fasta
                    if options.input_gff_file.endswith(".gz"): # Si le fichier d'entree est zipe
                        log.write("Fichier "+options.input_gff_file+" dezipe et ouvert.\n")
                        handle=gzip.open(options.input_gff_file) # On le dezipe et on l'ouvre
                    else : # Sinon
                        log.write("Fichier "+options.input_gff_file+" ouvert.\n")
                        handle=open(options.input_gff_file) # On l'ouvre
                    temp_seq=[]
                    for record in GFF.parse(handle, base_dict=seq_dict, limit_info=dict(gff_source=tab_ask_features)): # Pour tout les record parser dans le fichier gff a partir de la sequence stockee dans seq_dict
                        record.seq.alphabet=generic_dna                        
                        input_seqs.append(record) # On recupere le record dans la variable input_seqs
                    handle.close()
                    log.write("Recuperation des caracteristiques de la/des sequence(s) contenue(s) dans le fichier gff."+str(len(input_seqs))  +"\n")
            else: # Sinon
                for record in SeqIO.parse(handle, options.format): # Pour tout les record parser dans le fichier d'entree
                    input_seqs.append(record) # On recupere le record dans la variable input_seqs
                log.write("Recuperation de la/des sequence(s) et des caracteristiques de la/des sequence(s) contenue(s) dans le fichier d'entree.\n")
            
            for record in input_seqs: # Pour tout les record parser dans le fichier d'entree
                log.write("Parcourt du record: "+str(record.id)+"\n")
                
                new_record=SeqRecord(seq=record.seq, id=record.id,description=record.description)# On cree un nouveau record
                
                for feature in record.features : # Pour chaque feature rencontree
                    log.write("Parcourt de la feature: "+str(feature.type)+"_"+str(feature.location)+"_"+str(feature.strand)+"\n")
                    feature.id=str(feature.type)+"_"+str(feature.location)+"_"+str(feature.strand)
                    available_feature[feature.type] = 0 # On affecte au type de la feature rencontree la valeur 0
                    log.write("Feature de type: "+feature.type+"\n")
                        
                    if ((options.type!=None) or (options.notype!=None)) and (options.list==False):# Si on possede au moins 1 type ou 1 notype et qu'on ne souhaite pas list
                        if (options.strand==feature.strand or options.strand==0):#Si le strand de la feature correspond bien au strand demande
                            if ask_features.has_key(feature.type): # Si ask_features a pour cle le type
                                ask_features[feature.type]=1# On remplace le 0 par un 1
                                if feature.strand==1: # Si la feature a pour strand 1
                                    if whitchtype=="type": # Si on est en type
                                        if options.subfeature==False: # Si on ne veut pas les subfeatures
                                            # On ajoute une nouvelle feature au nouveau record ayant les caracteristiques de la feature etudiee
                                            new_record.features.append(SeqFeature(location=FeatureLocation(feature.location.nofuzzy_start+d_start,feature.location.nofuzzy_end+d_end),type=feature.type,strand=feature.strand,id=feature.id))
                                            log.write("Ajout de la feature "+new_record.features[len(new_record.features)-1].id+" au record.\n")
                                        else: # Si on souhaite les subfeatures
                                            for subfeat in feature.sub_features: # Pour chaque subfeature pr�sente dans la feature
                                                # On ajoute une nouvelle feature au nouveau record ayant les caracteristiques de la subfeature etudiee
                                                new_record.features.append(SeqFeature(location=FeatureLocation(subfeat.location.nofuzzy_start+d_start,subfeat.location.nofuzzy_end+d_end),type="exon_"+str(feature.type),strand=feature.strand,id=str(feature.type)+"_"+str(subfeat.location)+"_exon"))
                                                log.write("Ajout de la feature "+new_record.features[len(new_record.features)-1].id+" au record.\n")
                                    else: # Si on est en notype
                                        if options.subfeature==False: # Si on ne veut pas les subfeatures
                                            # On ajoute une nouvelle feature au nouveau record ayant les caracteristiques de la feature etudiee
                                            new_record.features.append(SeqFeature(location=FeatureLocation(feature.location.nofuzzy_start-d_end,feature.location.nofuzzy_end+d_start),type=feature.type,strand=feature.strand,id=feature.id))
                                            log.write("Ajout de la feature "+new_record.features[len(new_record.features)-1].id+" au record.\n")
                                        else: # Si on souhaite les subfeatures
                                            for subfeat in feature.sub_features: # Pour chaque subfeature pr�sente dans la feature
                                                # On ajoute une nouvelle feature au nouveau record ayant les caracteristiques de la subfeature etudiee
                                                new_record.features.append(SeqFeature(location=FeatureLocation(subfeat.location.nofuzzy_start-d_end,subfeat.location.nofuzzy_end+d_start),type="exon_"+str(feature.type),strand=feature.strand,id=str(feature.type)+"_"+str(subfeat.location)+"_exon"))
                                                log.write("Ajout de la feature "+new_record.features[len(new_record.features)-1].id+" au record.\n")
                                else: # Si on est sur le brin -1
                                    if whitchtype=="type": # Si on est en type
                                        if options.subfeature==False: # Si on ne veut pas les subfeatures
                                            # On ajoute une nouvelle feature au nouveau record ayant les caracteristiques de la feature etudiee
                                            new_record.features.append(SeqFeature(location=FeatureLocation(feature.location.nofuzzy_start-d_end,feature.location.nofuzzy_end-d_start),type=feature.type,strand=feature.strand,id=feature.id))
                                            log.write("Ajout de la feature "+new_record.features[len(new_record.features)-1].id+" au record.\n")
                                        else: # Si on souhaite les subfeatures
                                            for subfeat in feature.sub_features: # Pour chaque subfeature pr�sente dans la feature
                                                # On ajoute une nouvelle feature au nouveau record ayant les caracteristiques de la subfeature etudiee
                                                new_record.features.append(SeqFeature(location=FeatureLocation(subfeat.location.nofuzzy_start-d_end,subfeat.location.nofuzzy_end-d_start),type="exon_"+str(feature.type),strand=feature.strand,id=str(feature.type)+"_"+str(subfeat.location)+"_exon"))
                                                log.write("Ajout de la feature "+new_record.features[len(new_record.features)-1].id+" au record.\n")
                                    else: # Si on est en notype
                                        if options.subfeature==False: # Si on ne veut pas les subfeatures
                                            # On ajoute une nouvelle feature au nouveau record ayant les caracteristiques de la feature etudiee
                                            new_record.features.append(SeqFeature(location=FeatureLocation(feature.location.nofuzzy_start-d_start,feature.location.nofuzzy_end-d_end),type=feature.type,strand=feature.strand,id=feature.id))
                                            log.write("Ajout de la feature "+new_record.features[len(new_record.features)-1].id+" au record.\n")
                                        else:# Si on souhaite les subfeatures
                                            for subfeat in feature.sub_features:# Pour chaque subfeature pr�sente dans la feature
                                                # On ajoute une nouvelle feature au nouveau record ayant les caracteristiques de la subfeature etudiee
                                                new_record.features.append(SeqFeature(location=FeatureLocation(subfeat.location.nofuzzy_start-d_start,subfeat.location.nofuzzy_end-d_end),type="exon_"+str(feature.type),strand=feature.strand,id=str(feature.type)+"_"+str(subfeat.location)+"_exon"))
                                                log.write("Ajout de la feature "+new_record.features[len(new_record.features)-1].id+" au record.\n")
                log.write("Features correspondantes aux criteres : "+str(len(new_record.features))+"\n" ) 
                if options.nomerge==False: # Si l'option nomerge n'est pas demand�                                               
                    log.write("Fusion des features chevauchantes : "+"\n")
                    # Appel de la fonction MergeFeatures
                    merged_record=MergeFeatures(new_record, options.strand,log)
                    log.write("Feature(s) dans le record:"+str(len(merged_record.features))+"\n") 
                else: # Sinon
                    merged_record=new_record
                log.write("Trie des features par rapport a leurs position sur le record : ")
                # Appel de la fonction sortFeatures
                sorted_record=sortFeatures(merged_record)
                log.write(str(len(sorted_record.features))+"\n")
                # On ajoute le record trie au sequences a recuperer
                mes_sequences_recup.append(sorted_record)
                
            for atype in ask_features:# Pour l'emsempble des types des features demandees
                if ask_features[atype]==0:# Si le type a pour cle 0 dans le dictionnnaire ask_features
                    sys.stderr.write(atype+" is not found \n")# On imprime le message d'erreur suivant
                    log.write(atype+" is not found\n")
                                                        
            if options.list==False: # Si la liste des features n'a pas ete demandee
                for record in mes_sequences_recup: # Pour l'ensemble des records presents dans le tableau des sequences recuperees
                    if options.mask==True: # Si on veut masquer
                        log.write("Masquage de ses sequences fusionnees et triees\n")
                        # Appel de la fonction MaskeFeatures
                        masked_record=MaskSequences(record,whitchtype) 
                        log.write("Ajout du record avec masquage "+str(record.id)+" aux sequences a extraire.\n")
                        # On ajoute le record masquer au tableau des sequences a imprimer
                        mes_sequences.append(masked_record)
                    else: # On ne souhaite pas masquer
                        log.write("Extraction des sequences fusionnees et triees\n")
                        # On cree un nouveau record
                        extract_record=SeqRecord(Seq("",generic_dna),id='')
                        if whitchtype=="notype": # Si on est en notype
                            other_record=SeqRecord(seq=record.seq, id=record.id,description=record.description)# On cree un nouveau record
                            cpt=0 # Initialisation d'un compteur
                            if len(record.features)>1: # Si le record contient plus d'une feature
                                for i in range(len(record.features)-1): # Pour i allant de 0 au nombre de feature dans le record
                                    feature=record.features[i] # feature= feature en cours
                                    next_feature=record.features[i+1] # next_feature= feature suivante
                                    if cpt==0: # Si le compteur est a 0
                                        # On ajoute une nouvelle feature au record que l'on vient de creer qui a pour start 0 et pour end le debut de la feature etudiee
                                        other_record.features.append(SeqFeature(location=FeatureLocation(0,feature.location.nofuzzy_start),type="nofeature",strand=feature.strand,id="no_avant"+feature.id))
                                        # On incremente le compteur
                                        cpt=cpt+1
                                    # Si le compteur est different de 0 et inferieur au nombre de feature present dans le record
                                    if cpt!=0 and cpt<len(record.features):
                                        # On ajoute une nouvelle feature au record que l'on vient de creer qui a pour start la fin de la feature en cours et pour end le debut de la feature suivante
                                        other_record.features.append(SeqFeature(location=FeatureLocation(feature.location.nofuzzy_end,next_feature.location.nofuzzy_start),type="nofeature",strand=feature.strand,id="no_apres"+feature.id))
                                        # On incremente le compteur
                                        cpt=cpt+1
                                    # Si le compteur est egale au nombre de features du record
                                    if cpt==len(record.features):
                                        # On ajoute une nouvelle feature au record que l'on vient de creer qui a pour start la fin de la feature en cours et pour end la longueur de la sequence
                                        other_record.features.append(SeqFeature(location=FeatureLocation(feature.location.nofuzzy_end,len(record.seq)),type="nofeature",strand=feature.strand,id="no_fin"+feature.id))
                            elif len(record.features)==1: # Sinon si le record ne contient q'une feature
                                # On ajoute une nouvelle feature au record que l'on vient de creer qui a pour start 0 et pour end le debut de la feature etudiee
                                other_record.features.append(SeqFeature(location=FeatureLocation(0,record.features[0].location.nofuzzy_start),type="nofeature",strand=record.features[0].strand,id="no_avant"+record.features[0].id))
                                # On ajoute une nouvelle feature au record que l'on vient de creer qui a pour start la fin de la feature en cours et pour end la longueur de la sequence
                                other_record.features.append(SeqFeature(location=FeatureLocation(record.features[0].location.nofuzzy_end,len(record.seq)),type="nofeature",strand=record.features[0].strand,id="no_fin"+record.features[0].id))
                            for feat in other_record.features: # Pour l'ensemble des features du record
                                # Appel de la fonction get_sub_seq_record
                                extract_record=get_sub_seq_record(other_record,feat,log)
                                log.write("Ajout d\'un record contenant la feature "+str(record.id)+"_"+str(feat.type)+"_"+str(feat.location)+"_"+str(feat.strand)+" aux sequences a extraire.\n")
                                # On ajoute le record au tableau des sequences a imprimer
                                mes_sequences.append(extract_record)
                        else: # Sinon (on est en type)
                            for feature in record.features: # Pour l'ensemble des features du record
                                # Appel de la fonction get_sub_seq_record
                                extract_record=get_sub_seq_record(record,feature,log)
                                log.write("Ajout d\'un record contenant la feature "+str(record.id)+"_"+str(feature.type)+"_"+str(feature.location)+"_"+str(feature.strand)+" aux sequences a extraire.\n")
                                # On ajoute le record au tableau des sequences a imprimer
                                mes_sequences.append(extract_record)
                log.write("Ecriture dans le fichier de sortie au format fasta\n")
                # On ouvre el fichier de sortie en ecriture
                output_handle = open(options.fasta, "w") # On ouvre le fichier de sortie en mode ecriture 
                log.write("Ouverture du fichier "+options.fasta+" pour ecriture.\n")
                # On ecrit mes_sequences dans le fichier de sortie
                count=SeqIO.write(mes_sequences, output_handle, "fasta")
                log.write("Fermeture du fichier de sortie\n")
                output_handle.close() # On ferme le fichier de sortie
            else : # Sinon
                log.write("Affichage des differentes features presentes dans le fichier d'entree\n")
                for i in available_feature: # Pour l'ensemble des elements du dictionnaire
                    print i # On affiche la feature
                    exit # On sort du programme  
        log.write("Fermeture du fichier d'entree\n")
        # On ferme le fichier d'entree
        handle.close()
    else : # Sinon on affiche l'aide
        parser.print_help()
        sys.exit(1)
    log.write("## End (" + str(datetime.datetime.now()) + ")\n") # Le traitement c'est termine a tel heure
    log.close()  # On ferme le fichier de log

#!/usr/local/bioinfo/bin/python2.5
#
# Counting 
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'beta'

import sys, time, numpy
from numpy.random import randint
from optparse import OptionParser, OptionGroup  
from os import path
    
def unsort(file,output=None,seed=None):  
    dict={}
    #seed with the file length
    f=open(file,'r')
    
    if not seed:
        r=len(f.readlines()) #file length
        f.seek(0) #get back to the beginning of the file
    else:
        r=seed
        
    for l in f:
        i=randint(r)
        if l[-1]!='\n':
            l+='\n'
        if dict.has_key(i):
            dict[i]+=l
        else:
            dict[i]=l
    
    t=dict.keys()
    t.sort() ; f.close()
    #write result
    if output:
        f=open(output,'w')
    else:
        f=open(file,'w')
    for i in t:
        #sys.stdout.write(dict[i])
        f.write(dict[i])
    return 0

if __name__ == "__main__":
    parser = OptionParser(usage="usage: %prog -f filename") 
    parser.description="'unsort', the missing unix filter.\nV1.0 written by gatopeich in 2006 (see http://gatopeichs.pbwiki.com/).\nTranslation into Python.\n" 
    parser.add_option("-f", "--file", dest="file",
                      help="file to be unsort", metavar="FILE")  
    parser.add_option("-o", "--out", dest="output",
                      help="write the result of unsort in FILE, optional", metavar="FILE")  
    parser.add_option("-s", "--seed", dest="seed",
                      help="use SEED for random number generation, otherwise use time.", metavar="INT") 

    (options, args) = parser.parse_args()
    if options.file:
        if path.exists(options.file):
            unsort(options.file,options.output,options.seed)
    else:
        print"No file input, please give a file"
        parser.print_help()
        sys.exit(1)
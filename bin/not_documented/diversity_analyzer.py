#!/usr/local/bioinfo/bin/python2.5

#
# Counting 
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'beta'

import re, os, time, sys
from Bio import SeqIO
from tempfile import NamedTemporaryFile
from optparse import OptionParser, OptionGroup
import matplotlib.pyplot as plt
import matplotlib.lines as lines
from unsort import unsort

def parseCond(filename):
    """return the content of a given file as a table
    split with " " for Subini, no matter for the others"""
    t=[]
    f=open(filename,'r')
    for l in f:
        t.append(l.split(' ')[0].replace('\n',''))
    return t

def parseFastaCond(filename):
    """return the content of a given file as a table"""
    t=[]
    f=open(filename,'r')
    for seq in SeqIO.parse(f,"fasta"):
        t.append(seq.id)
    return t

def findCond(sequence, condF, condE):
    """search the given sequence in the condE dict of regular expression, then in the condF matrix of the EST.
    Then return the name of the found condition."""
    for regexp in condE:
        exp=condE[regexp]
        if exp.search(sequence)!=None:
            return regexp #name of the condition
    if condF.has_key(sequence):
        return condF[sequence]
    else:
        return "Other" #if not founded
    
def strTab(table):
    """make a list as a string tab-separated"""
    for i in range(len(table)):
        table[i]=str(table[i])
    return '\t'.join(table)

def plotTable(title,table,filename,graphStep):
    """Plot each table of depth for one condition"""
    fig = plt.figure()
    ax = fig.add_subplot(111)
    x=range(len(table))
    for i in range(len(table)):
        x[i]=x[i]*graphStep
    y=table
    ligne = lines.Line2D(x, y)
    ax.add_line(ligne)
    plt.axis([0, max(x), 0, max(y)+graphStep])
    plt.xlabel("Sequences")
    plt.ylabel("Contigs")
    plt.title(title)
    plt.savefig(open(filename,'w'))

    
def plotAll(title,tables,filename,graphStep):
    """Plot all conditions depth tables, takes an dict of table depth"""
    #tables is a dict {condition:[y_value]}
    palette=["blue","yellow","red","magenta","#008080","#808000","green","cyan","#4B0082","#B00000","#808080","#8B4513","#98FB98","#C71585","#FFA500","#FFD700","#000080"]
    plt.subplot(111)
    maxX=0 ; maxY=0 ; j=0
    for key in tables.keys():
        j+=1 ; t=tables[key]
        maxX=max(maxX,len(t)) 
        maxY=max(maxY,max(t))
        #x = arange(0, max(t), graphStep) 
        x=range(len(t))
        for i in range(len(t)):
            x[i]=x[i]*graphStep
        y=t
        plt.plot(x,y,palette[j%len(palette)],label=key)
    #plt.axis([0, maxX, 0, maxY+graphStep])
    maxi=max(maxX,maxY)
    plt.plot(range(maxi),range(maxi),"black",label="Reference") #y=x
    plt.legend(loc='lower right', ncol=1, mode=None, 
               shadow=True, fancybox=True)
    plt.xlabel("Sequences")
    plt.ylabel("Contigs")
    plt.title(title)
    plt.savefig(open(filename,'w'))
    
    
def diversityAnalyzer(contigs,params,outputDir,verbose=False,graphStep=500,checkpoint=28000):
    """(contigs, params)
    params : condition experiences
    Comptage par contig du nombre d'ESTs des differentes conditions :
    - soit donnees par des fichiers
    - soit donnees par les nom d'EST 
    sortie: dictionnaire cle=contig valeur=table des comptes"""
    
    import re, os, time, sys
    from Bio import SeqIO
    from tempfile import NamedTemporaryFile
    from optparse import OptionParser, OptionGroup
    import numpy as np
    from numpy import arange
    import matplotlib.pyplot as plt
    import matplotlib.lines as lines
    if not graphStep:
        graphStep=100 #abscissa step for plotting
    if not checkpoint:
        checkpoint=28000 #prompt a dot for each 28 000 lines
    conditions=[] #table of the condition names
    condE={} #dict of regular expressions
    condF={} #dict of tables of EST for each condition from file
    condCont={} #{cond:[cont]} list of contigs per condition
    condSumSeq={}#{cond:TotalSeqSum} sequences occurences per condition
    condToPlot={}#{cond:[SeqSum]} list of values to plot foreach condition
    os.path.curdir=outputDir
    if outputDir[-1]!='/':
        outputDir+='/' #directory must finish by a /
    if verbose:
        start=time.time()
        print"_\nparsing parameters", ;sys.stdout.flush()
    f=open(params,'r')
    totalParams=len(f.readlines())
    f.seek(0)
    line=f.readline() #ignore first line
    cptr=0
    for line in f:
        cptr+=1
        if verbose and cptr%(totalParams/9)==0:
            sys.stdout.write('.') ; sys.stdout.flush()
        try:
            [nom,type,valeur]=line.split("\t")
        except ValueError:
            print"\n\nParameter file not formatted as:\n\nHeader \nCondition_1_name\tfile\tpath_filename\nCondition_2_name\texp    ^FZZC9SK01"
            sys.exit(1)
        valeur=valeur.replace('\n','') 
        if type=="exp":
            condE[nom]=re.compile(valeur) #regexp object
        elif type=="file": #condition from file
            for seq in parseCond(valeur): #list
                condF[seq]=nom #dict of {EST:condition_name}
        elif type=="fasta":
            for seq in parseFastaCond(valeur): #list
                condF[seq]=nom #dict of {EST:condition_name}
        else:
            print "\n\nBad parameters.txt format: type",type,"uncorrect. Only exp file and fasta allowed"
            sys.exit(1)
    f.close()
    if verbose:
        step1=time.time()
        print "done in",step1-start,"seconds\n_\npreprocess the file linking contig and EST...", ;sys.stdout.flush()
    
    contigSeqTmp=NamedTemporaryFile(mode='w+b', prefix='tmp')
    #remove header, then unsort
    #os.system("tail -n +1 "+contigs+"| ./unsort -q > "+contigSeqTmp.name+" 2> /dev/null") 
    unsort(contigs,contigSeqTmp.name)
    

    if verbose:
        step2=time.time()
        print "done in",step2-step1,"seconds\n_\nparsing the file linking contig and EST", ;sys.stdout.flush()
        
    ptr=0
    for line in contigSeqTmp.file:
        ptr+=1
        if verbose and ptr%checkpoint==0:
            sys.stdout.write('-')
        try:
            contig=line.split('\t')[0]
            EST=line.split('\t')[1]
        except ValueError:
            print"\n\nUncorrect file format for",contigs,"\nSHould be:\nHeader\ncontig_name\tEST_name"
            sys.exit(1)
            
        if contig=="name":#ignore header
            continue
        
        EST=EST.replace('\n','') ; contig=contig.replace('\n','')
        condition=findCond(EST, condF, condE)
        
        #condCont={cond:{cont:int}} ;condSumSeq={cond:TotalSeqSum}       
        if not condCont.has_key(condition):#new condition
            condSumSeq[condition]=1 #dict of total number of seq foreach condition
            condCont[condition]={contig:1} #dict listing contigs for each condition
        elif not condCont[condition].has_key(contig):#cond has new contig
            condSumSeq[condition]+=1
            condCont[condition][contig]=1      
        else: #both cond and cont are kown
            condSumSeq[condition]+=1
            condCont[condition][contig]+=1
            
        #calculations for the plot condToPlot={cond:[SeqSum]}
        if condSumSeq[condition]%graphStep==0: #step reached
            if condToPlot.has_key(condition):
                condToPlot[condition].append(len(condCont[condition]))
            else: #new dict entry and new list
                condToPlot[condition]=[len(condCont[condition])]
        sys.stdout.flush()
   #plotting
    for cond in condToPlot.keys():
        title=cond ; table=condToPlot[cond] ; filename=os.path.join(os.path.curdir,cond+".PNG")
        plotTable(title,table,filename,graphStep)
    
    plotAll("Graphe total",condToPlot,os.path.join(os.path.curdir,"global.PNG"),graphStep)
    os.system("tar -Pc -f "+os.path.join(os.path.curdir,"results.tar")+" "+os.path.join(os.path.curdir,"*.PNG")+" --remove-files")# ; rm "+outputDir+"*.PNG")
    if verbose:
        print "\nJob ended in",time.time()-start,"seconds"
    return 0

def diversity_depts_error():
    """
    Return the list of dependencies missing to run the diversity_analyzer
    """
    error = ""
    if not which("unsort") == None:
        error += " - unsort is missing"
    if error != "":
        error = "Cannot execute diversity_analyzer, following dependencies are missing :\n" + error + " Please install them!"
    return error

def which (program):
    """
    Return if the asked program exist in the user path
      @param options : the options asked by the user
    """
    import os
    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

if __name__ == "__main__":
    parser = OptionParser(usage="usage: ./diversity_analyzer -c filename -p filename -r directory -s 200 -v -t 30000")
    parser.description="This program analyze the diversity of a set of sequences: for each condition, it will plot the number of contigs if function of the number of sequences. Resulting pictures are in PNG and archived in the result directory."
    required=OptionGroup(parser, "Required options")
    required.add_option("-c", "--contig_seq", dest="contigFile",
                        help="tabulated file linking contigs to sequences in 2 columns contig_name <tabulation> sequence_name ; with 1 line of header", metavar="FILE")
    required.add_option("-p", "--parameters", dest="paramFile",
                        help="tabulated file giving for each condition the way to keep their sequences.Tabulated in 3 columns (name, type, value) and 1 line of header. Types allowed: 'file', 'fasta' and 'exp'", metavar="FILE")
    required.add_option("-r", "--results", dest="resultDirectory",
                        help="write the bitmaps results in DIRECTORY", metavar="DIRECTORY")
    facultative=OptionGroup(parser, "Facultative options")
    facultative.add_option("-v", "--verbose",
                           action="store_true", dest="verbose", default=False,
                           help="print all status messages to stdout")   
    facultative.add_option("-s", "--step", dest="graphStep",
                    help="graphical step for the result bitmaps", metavar="INTEGER")
    facultative.add_option("-t", "--tickspace", dest="checkPoint",
                    help="(if verbose) is the number of sequences between two dots of the progression bar", metavar="INTEGER")
    parser.add_option_group(required) ; parser.add_option_group(facultative)
    
    (options, args) = parser.parse_args()
    
    if not diversity_depts_error():
        contigs=options.contigFile ; params=options.paramFile 
        outputDir=options.resultDirectory ; verbose=options.verbose 
        graphStep=options.graphStep;checkPoint=options.checkPoint
        if not contigs or not params or not outputDir:
            print"Please give all the required fields"
            parser.print_help()
            sys.exit(1)
        else: #depts and args ok, launch the program
            diversityAnalyzer(contigs,params,outputDir,verbose,graphStep,checkPoint)
    else:
        print diversity_depts_error()
    
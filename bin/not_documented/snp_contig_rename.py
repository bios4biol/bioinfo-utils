#!/usr/local/bioinfo/bin/python2.5

#
# snp_library : Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'

from optparse import *
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
import os, sys, gzip, bz2, string, re

def version_string ():
    """
    Return the snp_library version
    """
    return "snp_library " + __version__

if __name__ == "__main__":
    LINE_LEN=60
    parser = OptionParser(usage="Usage: snp_library.py -d FILE -l FILE -r FILE -o FILE")
    usage = "usage: %prog -d snp_details_file -s snp_file -r rename_file -o file "
    desc = " Generate an array with expression library for each snp allele \n"
           
    parser = OptionParser(usage = usage, version = version_string(), description = desc)
    igroup = OptionGroup(parser, "Input files options","")
    igroup.add_option("-s", "--snp", dest="snp_file",
                      help="The snp file", metavar="FILE")
    igroup.add_option("-d", "--detail", dest="detail_file",
                      help="The snp detail file", metavar="FILE")
    igroup.add_option("-r", "--rename", dest="rename_file",
                      help="The contig_renaming.txt file ", metavar="FILE")
    parser.add_option_group(igroup)
    (options, args) = parser.parse_args()
    if options.rename_file == None and options.snp_file == None :
        parser.print_help()
        sys.exit(1)
    
    #read zipped input 
    rename={}
    
    f=open (options.rename_file, "r")
    for line in f.readlines():
        tab=line.split()
        rename[tab[0]]=tab[1]
    f.close()

    snp_renaming={}
    fo=open (options.snp_file+".rename", "w")
    f=open (options.snp_file, "r")
    #snp_F0F0A3T01EI843_79    snp_cluster_CL10000Contig1    77    T/A    T(5)    A(3)    ATGTGATTGATTTGTGAAGTTCTTGAATGGTGGAGTTCTTTTAAAAAATTGTTAAAAA--    TCTTTTTAACGTGTAAGATTC-CTCCCTAAAAAATATTGAAATATGGCAGAACAGCAAGA    F0F0A3T02HS4MF_33:F0F0A3T02HVRQB_77

    for line in f.readlines():
        
        tab=line.split()
        contig_name=tab[1].split("_")
        if rename.has_key(snp_name[2]) :
            tab[1]=snp_name[3]
        fo.write(tab.join("\t")+"\n")
        
    fo.close()
    f.close()

            
    sys.exit(0)
        

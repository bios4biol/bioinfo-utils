#!/usr/local/bioinfo/bin/python2.5
# -*- coding: utf-8 -*-

#
# chimer-search 
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'beta'

from optparse import OptionParser, OptionGroup  
import sys, os, time, re, warnings
#import numpy as np
#import matplotlib.pyplot as plt
#import matplotlib.lines as lines
from Bio import SeqIO
from Bio.Blast import NCBIXML  
from tempfile import NamedTemporaryFile
import random
from random import randint
##for bitmap generation, the plot
from gt.core import Range
from gt.annotationsketch import GraphicsCairoSVG, Color
from gt.annotationsketch import *
from MySQLdb import *
class Pike:
    """like a dict, but a little bit more"""
    def __init__(self,s,e):
        self.start=s ; self.end=e
        #self.names=n #protName : occurences
        self.hitList=[]#list of blast hits
        self.toPlot={} #{(startq,endq,longname,strand) : occurences}
      # only for a main hit, used to merge the others
        self.description=""#the decription of the blocks: (x1,y1)____(x2,y2)____(x3,y3)...
    def copy(self,pike):
        self.start=pike.start ; self.end=pike.end
        self.names=pike.names ; self.hitList=pike.hitList
    def equals(self,pike):
        return self.start==pike.start and self.end==pike.end and self.names==pike.names and self.hitList==pike.hitList
    def hasPoint(self,i):
        return (i>=self.start and i<=self.end)
    def setNames(self,list):
        self.hitList=list
def pikeFinder(table,threshold,names,hitList,debug=False):
    """find pikes and return them as a list of Pike, the core of the chimers detection"""
    l = max(table)*threshold #threshold in %, useless for instance
    isPike=False
    start=0 ; result=[] #result is the Pike list also used for graphicals
    for pos in range(len(table)):
        n=table[pos]
        #pike counting (geometrically)
        if n>0 and not isPike:#start of a pike at position start
            isPike=True
            start=pos
            
        elif n==0 and isPike: #end of a pike at position i
            isPike=False
            #do a new pike and build his list of names
            pike=Pike(start,pos)
            result.append(pike)
   
    if len(result)<2:
        return result,len(result)
    
    for pi in result: #foreach pikes
        for hit in hitList:
            #print pi, pi.start, pi.end,"confronted to",hit
            if pi.hasPoint(hit.start): #start or any other point will do
                pi.hitList.append(hit)
        pi.hitList=list(set(pi.hitList)) #nr list
    return [result,len(result)]
class BlastHit:
    def __init__(self, qtuple, longname, stuple, type):
        self.start = qtuple[0] #relative to
        self.end   = qtuple[1]   #the query 
        self.shortName= longname.split('_')[0]#prot name: first alphas
        self.name = longname #full prot name : protein_specie
        #descriptions
        self.startp = stuple[0] #relative to 
        self.endp   = stuple[1]   #the subject protein
        #for graphical representation
        if self.start <= self.end:    
            self.phase='+'
        else:             
            self.phase='-'
        self.typeSeq=type #when parsing blastOutput it's 'cds' 
        self.id=""     #if !="" : main sequence, for gff3
        self.parent="" #if !="" : id of the main sequence to which the current is attached
        self.occur=0
    def __str__(self):
        return self.name+'@'+str(self.start)+','+str(self.end)
    def getTuple(self):
        return (self.start,self.end)
    def addOne(self):
        self.occur+=1
def whereIsThisName(name,Pikes):
    """
    Return the list of the Pikes object which contains the name
    in their name list
    """
    l=[]
    for p in Pikes: #confront a long name with list of short names
        n=[] #temp list of names for the current pike
        for hit in p.hitList:
            n.append(hit.name)
        if " ".join(n).__contains__(name):
            l.append(p)
    return l
def groupHit(Pikes): #list of BlastHits, list of Pikes #output dict{coord:hit}
    """
    Structurates the hitlist of the current contig, according to 
    the founded pikes. Kill redundance (in name, coordinates and 
    orientation) of Blast hits and regroup them into exon/intron-
    like representation (introns /\ for gaps, exons [] for hits).
    """
    if not Pikes: 
        return None #nothing to do
    ###hitToPlot={} #{(start,end,longname,strand) : occurences}
    allTheNames=[]#nr names list of blast hits of the current contig
    
    #build nr-dict
    for pi in Pikes:
        for bH in pi.hitList:               
            allTheNames.append(bH.name)
            if bH.start < bH.end: 
                start=bH.start ; end=bH.end ; strand="+"
            else:
                start=bH.end ; end=bH.start ; strand="-"
            key=(start,end,bH.name,strand)
            if pi.toPlot.has_key(key): #    preregroup with
                pi.toPlot[key].addOne()#    absolute identity
            else:                      #    for position, 
                pi.toPlot[key]=bH      #    orientation and names
   
    #extend the dict with gaps and main    
                
    allTheNames=list(set(allTheNames))#nr list
    
    #bool for output mode : blast only or proteins (regrouped blast) only
    representOnlyRegrouped=False #True if a main is found
    
    #extend nr-dict with gaps, main and son sequences
    for name in allTheNames:
        multiplePikes=whereIsThisName(name,Pikes)#list of Pike object containing this name, 2 at least
        
        if len(multiplePikes)<2: #name not interesting
            continue             #jump to the next name     
        
        longuests=[] #list of the hits with the same name but in different pikes
        #loop only the pikes sharing the current name
        for pike in multiplePikes:
            maxlen=0 
            biggerBH=None #bigger of the current pike
            currentList=[]
          # loop the blast hits having this name
            for bH in pike.hitList: #foreach blast hit having the current name, 
                currentLength=abs(bH.end-bH.start)
                if bH.name==name and maxlen<currentLength: #eval maximum
                    biggerBH=bH
                    maxlen = currentLength
                if bH.name==name:
                    currentList.append(bH)
                    
          # search for potential bindings inside the pike
            lines=[] # list of line=[blastHit, blastHits..., end position]
            for bH in currentList:
              # try to insert the blastHit inside an existing line 
                inserted=False
                for l in lines:
                    if l[-1]<min(bH.start,bH.end): #if the line can accept this bH
                        l.pop()         #remove the position
                        l+=[bH, max(bH.end,bH.start)] # bH, bH, bH, bH, endposition
                        inserted=True
              # add a new line
                if not lines or not inserted:
                    lines.append([bH, max(bH.end,bH.start)]) #bH, bH, bH, endposition
          # search the longuest in these intrapike-binded sequences
            maxlength=0
            for l in lines:
                for i in range(len(l)-1):
                    bH=l[i]
                    length=max(bH.start,bH.end) -min(bH.start,bH.end)
                    if maxlength<length:
                        maxlength=length
                        bigger=l
          # compare the longuest bH with the longuest sequence of small bH
            if maxlength>maxlen:
                biggerBH=l[:-1] #list of bH
            ##ecraser tous ceux qui sont courts:
            for bH in pike.hitList:      #
                if not isinstance(biggerBH,list):
                    if bH!=biggerBH:         #
                        pike.hitList.pop(pike.hitList.index(bH)) #
                else: #the list wins
                    try: #find the current balst Hit in the saved ones
                        biggerBH.index(bH)
                    except: #ValueError: this is not a good 
                        pike.hitList.pop(pike.hitList.index(bH))
            try:
                longuests+=biggerBH
            except:
                longuests.append(biggerBH)
        
        id = str(   randint(   int(time.time()/3) , int(time.time())   )   ) #random id
        
        bornes=[] #list all start/end limits for the construction of the main sequence and gaps
        description=""
        for hit in longuests: 
            description+='_('+str(hit.startp)+','+str(hit.endp)+')'
            bornes+=[hit.start,hit.end]
            for pi in Pikes:
                for i in pi.toPlot.keys():     #{(startq,endq,longname,strand) : occurences}
                    if pi.toPlot[i]==hit:      #edit the longuest sequences, 
                        pi.toPlot[i].parent=id #put under same main seq
                        representOnlyRegrouped=True
                        
        #build gap in a new fictive Pike
        bornes.sort()#put it in the rigth order
        misc=Pike(bornes[0],bornes[-1])
        for i in range(1,len(bornes)-1,2):
            s=bornes[i] ; e=bornes[i+1] 
            length=e-s
            misc.toPlot[s,e," ","."]=BlastHit(s,e,name,1,length,'gap')
            misc.toPlot[s,e," ","."].parent=id #no name, no orientation
            
        #build main sequence, father of the others
        start=bornes[0] ; end=bornes[-1]
        length=end-start 
        mainSequence=BlastHit(start,end,name,1,length,'protein')
        mainSequence.id=id #set id but no orientation for main
        mainSequence.description=description #sequence of the son blocks
        misc.toPlot[start,end,name,"."]=mainSequence
        Pikes.append(misc)
        
    #check output mode:
    if representOnlyRegrouped:
        for pike in Pikes:
            tmp={}
            for coord in pike.toPlot:
                hB=pike.toPlot[coord]
                if hB.id or hB.parent: #blast hit used in protein regroup
                    tmp[coord]=hB
            pike.toPlot=tmp #erase all isolated blast hit
    #else:
        #nothing to do : keep all (there is only isolated blast hits
        
    return Pikes, representOnlyRegrouped
def python2gff3(contigName,contigLength, Pikes, onlyGroupedProt):#require contig name, contig sequence and dictHitsBlast
    """ Build a gff3 file for each input contig.
    Format definition by http://song.sourceforge.net/gff3.shtml.
    An online GFF3 validator is available at
    http://dev.wormbase.org/db/validate_gff3/validate_gff3_online. 
    It is limited to files of 3,000,000 lines or less. If you wish to
    validate larger files, please use the command-line version which
    can be downloaded from the same site.
    """
    fpath=os.path.join(os.path.curdir,contigName+".gff3")
    #edit name, inserting a 'c' for chimera or 'p' for protein at the end of the name
    if onlyGroupedProt:  #reconstructed protein
        fpath='.'.join(fpath.split('.')[:-1])+'.p.gff3'
    else:                #real chimera
        fpath='.'.join(fpath.split('.')[:-1])+'.c.gff3'
    
    gff3handle=open(fpath,'w') #write data in a string #If None, write a dot '.'
    gff3handle.write("##gff-version\t3\n")#+header to be corrected...
    gff3handle.write("##sequence-region "+contigName+" 1 "+str(contigLength+1)+"\n") 
  
  # master sequence: whole contig, force the display to not be chunked
    content=[contigName,"findChimera","protein","1",str(contigLength+1),
             ".",".",".","Name="+contigName]
    gff3handle.write("#master sequence\n"+'\t'.join(content)+'\n#\n')
    
  # First the main sequences : the parents
    for pi in Pikes:
        for coord in pi.toPlot.keys():
            if pi.toPlot[coord].id: #don't plot isolated blast hit from the same Pike
                start,end,name,strand=coord
                content=[contigName,"findChimera","protein",str(start+1),str(end+1),
                         ".",".",".","Name="+name+pi.toPlot[coord].description+";ID="+pi.toPlot[coord].id]
                ##TODO: add to Name="" the decription: (x1,y1)____(x2,y2)____(x3,y3)...
                gff3handle.write('\t'.join(content)+'\n') 
                
    gff3handle.write('#\n#\n')#pass 2 lines for readability
    
    for pi in Pikes:
        for coord in pi.toPlot.keys():
            if pi.toPlot[coord].id: 
                continue #don't write again main sequences
            (start,end,name,strand)=coord
            typeSeq=pi.toPlot[coord].typeSeq
            parent=pi.toPlot[coord].parent
            
            attributes=""
            if name:#else : we've got a problem here
                attributes+="Name="+name #attributes, name of the hit
            if pi.toPlot[coord].occur: #name (*5) means a stack of 5 sequences equals
                attributes+="(*"+str(pi.toPlot[coord].occur)+")"  
            if parent: #this is a "son" sequence, to be linked on its parent
                attributes+=";Parent="+parent 
            
            content=[contigName,       #ID of the landmark: name of the reference axe
                     "findChimera",    #source: this software
                     typeSeq,            #type
                     str(start+1),  #start of the hit (relatively to landmark sequence: current contig
                     str(end+1),    #end
                     ".",strand,".",      #score TODO: put e-value ? strand and phase
                     attributes]
            gff3handle.write('\t'.join(content)+'\n')
    return fpath
class CustomTrackCoverage(CustomTrack):
    """
    CustomTrack in the genometools programmers style, used to plot
    the coverage depth. The functions' names are not Python-like form 
    because of the bindings to the C-code of gtlib.
    """
    def __init__(self,dataset):
        super(CustomTrackCoverage, self).__init__()
        self.dataset=dataset
    def get_height(self):
        return 100
    def get_title(self):
        return "Total coverage depth"
    def render(self, graphics, ypos, rng, style, error):
        
#        #chunk 0-sequence at the beginning     
#        for pos in range(len(self.dataset)): 
#            if self.dataset[pos]>0:
#                data=self.dataset[pos:]
#                break
#        #chunk 0-sequence at the end
#        for rpos in range(len(data),0):
#            if data[rpos]>0:
#                data=data[0:rpos]
#                break
        #########
        ###soit rogne depthtable pour n'avoir que les valeurs interessantes: rogner extremites nulles
        ###soit affiche toute la séquence => + implications dans gff3ToPNG
        #########
        data=self.dataset
        #increment all values by 1 for plot
        for coord in range(len(data)):
            data[coord]+=1 #0 is not allowed for plot
        maxvalue=int(max(data))
       
        graphWidth=graphics.get_image_width()-2*graphics.get_xmargins()
        graphHeight= self.get_height()
        graphX=graphics.get_xmargins()
        graphY=ypos+graphHeight-graphHeight/(maxvalue+1)
        
        #draw graphical elements
        graphics.draw_curve_data(graphX, ypos, Color(0, 0, 1, .4),
                                 data, len(data), Range(0, maxvalue+1), graphHeight)
        graphics.draw_horizontal_line(graphX, graphY,
                                      Color(0, 0, 0, 1), graphWidth, 1)
        graphics.draw_arrowhead(graphX+graphWidth, graphY-2, #right arrowhead
                                Color(0, 0, 0, 1), 1)
        graphics.draw_arrowhead(graphX, graphY-2,  #left arrowhead
                                Color(0, 0, 0, 1), 0)
#        graphics.draw_vertical_line(  graphX, graphY-graphHeight, 
#                                      Color(0, 0, 0, 1), graphHeight, 2)
        #TODO: add vertical scale ? see CairoGraphics API
        return 0
def gff3ToPNG(fileName,depthTable,length): 
    """
    Convert the give gff3 file (fileName) into a bitmap.
    Two parts: top, the classical alignment-view ;
    down, coverage depth given by the depthTable.
    Most of the graphical customisations are in the file style
    (BlastHitView.style) 
    """#From sketch_parsed.py , gtpython , genometools
    #system call, only alignment view
    #os.system("gtool sketch -style ./BlastHitView.style --force "+fileName[:-5]+".png "+fileName)
    
    outputBitmap=fileName[:-5]+".png"
  # load style
    style=Style() ##/usr/local/bin/gtdata/sketch/default.style
    style.load_file("./BlastHitView.style")
  # create feature index and add GFF3 file
    feature_index = FeatureIndexMemory()
    feature_index.add_gff3file(fileName)

  # create diagram for first sequence ID in feature index
    seqid = feature_index.get_first_seqid()
    feat_range = Range(1,length+1) #feature_index.get_range_for_seqid(seqid) #for trnucate empty edges
    diagram = Diagram.from_index(feature_index, seqid, feat_range, style)

    ct=CustomTrackCoverage(depthTable)
    diagram.add_custom_track(ct)
    
  # set the diagram in the layout, suppress warnings
    warnings.filterwarnings("ignore")
    layout = Layout(diagram, 600, style)
    height = layout.get_height()
    
  # create canvas
    canvas = CanvasCairoFile(style, 600, height)
    
  # sketch layout on canvas
    layout.sketch(canvas)
    
  # write canvas to file
    canvas.to_file(outputBitmap)
    
    return
def parseDB(request,host,user,passwd,db):
    conn = connect(host, user, passwd, db)
    
    curs = conn.cursor() #select name, est_name, library_name from acatenellanr_sigenaecontig__contig__main a, acatenellanr_sigenaecontig__est_mrna__dm b where a.contig_id_key = b.contig_id_key limit 20;  
    curs.execute(request)
     
    return curs.fetchall()
def findChimera(outputDir,threshold,logFile,verbose,tableMain,tablemRNA,host,user,passwd,db):
    """
    Find chimers into the given contigs database,
    build a alignment depth (coverage) table for each contig,
    determinate if the contig is a chimera,
    plot and log the chimers.
    """
    toBeDeleted=[]#list of chimera ids
    numOfNoChimera=0 #number of sequences not chimeric: to be written in cleaned fasta
    numOfChimera=0 #number of sequences chimeric: to be written in the log file with their blast hits
    ALIGNMENT_MIN=40 #condition for strength of an alignment
    E_VALUE_THRESH=pow(10,-5)#threshold for e_value
    fastaLength=60 #length of the lines in the output fasta
    tickSpace=0.02 #% between ticks
    cptr=0#indicator of the contig number
    contigsInBlastOut={}#contigs without blast hit are not in the blast out file


    if logFile: 
        logs=open(logFile,'w') #logs.write(blastCommand+'\n')#header of logfile : the blast command
    if verbose: startTime=time.time() ; print"Request on database ",;sys.stdout.flush()
  
#    for t in ["acatenellanr_sigenaecontig__contig__main","acatenellanr_sigenaecontig__est_mrna__dm","acatenellanr_sigenaecontig__protein_hit__dm"]:
#        for h in parseDB("EXPLAIN "+t+";",host,user,passwd,db):
#            print h[0],
#        print""
       
    request = "SELECT DISTINCT name, length, protein_hit_database, c.protein_hit_accession, \
                    protein_hit_query_start, protein_hit_query_stop, protein_hit_subject_start, \
                    protein_hit_subject_stop \
                    FROM acatenellanr_sigenaecontig__contig__main a \
                    JOIN acatenellanr_sigenaecontig__est_mrna__dm b \
                    JOIN acatenellanr_sigenaecontig__protein_hit__dm c \
                    WHERE NOT ISNULL(protein_hit_database) AND protein_hit_evalue<1e-10 \
                    AND a.contig_id_key = b.contig_id_key AND a.contig_id_key = c.contig_id_key \
                    AND depth>1 ORDER BY name; "
                    
    dataFromDB = parseDB(request, host, user, passwd, db)
    
    if verbose: requestTime=time.time() ; print requestTime-startTime,"seconds.\nParse data... ";sys.stdout.flush()
        
    lastContig="" #the current contig name
    for datas in dataFromDB:
        contigName, contigLength, blastbase, fullName, queryStart, queryStop, sbjctStart, sbjctStop = datas
        
      # new block of contig  
        if lastContig!= contigName: 
            contigsInBlastOut[contigName]=True
            
            if verbose:
                cptr+=1 # cptr of contig
                if cptr%(len(dataFromDB)*tickSpace)<1: sys.stdout.write('=') ; sys.stdout.flush() #progression mark
                    
            if lastContig:# SAVE previous
                if len(hitList)>1: # enough hit for 2 pikes
                    Pikes, countPikes = pikeFinder(depthTable, threshold, names, hitList)  
                if len(hitList)<2 or countPikes<2:
                    numOfNoChimera+=1
                else: # generate bitmap
                    print"Pikes>2",
                    Pikes,onlyGroupedProt=groupHit(Pikes)
                    #Pikes is a list of Pikes, on the depthTable
                    #onlyGroupedProt is a boolean: 
                    #True: keep only blast hits regrouped in proteins, alternative transcription
                    #False: keep only isolated blast hits, so real chimera -> to logfile
                    gff3file = python2gff3(contig, contigLength, Pikes, onlyGroupedProt)  
                    gff3ToPNG(gff3file, depthTable, contigLength)
                    
                    if onlyGroupedProt:
                        numOfNoChimera+=1
                    else:
                        numOfChimera+=1     
                    if logFile:
                        for nameList in names.values():
                            nL=list(set(nameList))
                            nL.sort()
                        logs.write(contig+'\n'+'\t'.join(nL)+'\n')
                    
          # INITALL #in any case of changeblock
            lastContig = contigName #set current contig
            depthTable=[] #used to calculate the number of the pikes
            names={}  #local var names={(start,end):[]name}
            ##########TODO: 
            for i in range(contigLength): 
                depthTable.append(0)  # for the new contig
            hitList=[] #list blast hits         
            
            
      # INCREMENT #in any case
        shortName=fullName.split('_')[0] #name of the protein
        qtuple, stuple = ( int(queryStart), int(queryStart) ), ( int(sbjctStart), int(sbjctStop) )

        if not names.has_key(qtuple): names[qtuple]=[]
        names[qtuple].append(fullName) #should have some prots at same position
        for i in range(min(qtuple),max(qtuple)):    depthTable[i]+=1 #increment
        
        #BlastHit(query_start, query_end, shortName, longName, subject_start, subject_end)
        hitList.append(BlastHit(qtuple, fullName, stuple, 'blast_hit'))

    #EOF: PROCESS LAST HIT
    contigsInBlastOut[contigName]=True #used to remember which contig has been processed
    #SAVE last one
    Pikes,countPikes=pikeFinder(depthTable,threshold,names,hitList)                    
    if countPikes<2:
        numOfNoChimera+=1
    else:
        Pikes,onlyGroupedProt=groupHit(Pikes)
        gff3file=python2gff3(contig,contigLength,Pikes,onlyGroupedProt)                    
        gff3ToPNG(gff3file,depthTable,contigLength)
        
        if onlyGroupedProt:
            numOfNoChimera+=1
                     
        if logFile and not onlyGroupedProt:
            numOfChimera+=1
            for nameList in names.values():
                nL=list(set(nameList))
                nL.sort()
            logs.write(contig+'\n'+'\t'.join(nL)+'\n')
   
    contigList = parseDB("SELECT DISTINCT protein_hit_accession \
                        FROM acatenellanr_sigenaecontig__protein_hit__dm;",host,user,passwd,db)
    
    

    for c in contigList:
        if not contigsInBlastOut.has_key(c):#contig without blast hit: not writed in blast out
            numOfNoChimera+=1
            cptr+=1
            #TODO: verify they all had no blast output...
    if verbose: parseTime=time.time() ; print "\nin",parseTime-requestTime,"seconds.\nArchiving ", ;sys.stdout.flush()                

    if os.path.curdir==".": os.path.curdir=""    
    if numOfChimera:
        os.system("tar -Pcf "+os.path.join(os.path.curdir,"findChimera.tar")+" "+os.path.join(os.path.curdir,"*.gff3")+" "+os.path.join(os.path.curdir,"*.png")+" --remove-files") 
    
    if verbose: archivTime=time.time() ; print archivTime-parseTime,"seconds.\nTotal time ", archivTime-startTime ;sys.stdout.flush()  
    
    if verbose: print "\nBlast output",len(contigsInBlastOut), "\tTo log",numOfChimera,"\tNo chimera",numOfNoChimera,"\tTotal",len(contigList)
    if verbose: print "Get your results at",os.path.join(os.path.abspath(os.path.curdir),"findChimera.tar")
    
    #TODO: gros soucis : 0 fois on trouve des pikes... pas assez de données de BLAST

def which (program):
    """
    Return if the asked program exist in the user path
      @param options : the options asked by the user
    """
    import os
    def is_exe(fpath):
        return os.path.exists(fpath) and os.access(fpath, os.X_OK)
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None
def findChimera_depts_error():
    """
    Return the list of dependencies missing to run the findChimera
    """
    error = ""
#    if which("blastall") == None:
#        error += " - blastall is missing\n"
#    if which("gtool") == None:
#        error += " - genometools is missing (rename executable as gtool)\n" #" - genometool is missing
    try:
        import gt
    except ImportError:
        error += " - genometools is missing: import gt failed, check your installation and your PYTHONPATH\n"
    if which("tar") == None:
        error += " - tar is missing\n"
    if error != "":
        error = "Cannot execute findChimera.py, following dependencies are missing :\n" + error + "Please install them!"
    return error

if __name__ == "__main__":       
    parser = OptionParser(usage="usage: %prog -c filename -o filename")    
    parser.description="This program detects chimera in multifasta DNA contig file thanks to a blast against protein databases"
    
    required=OptionGroup(parser, "Required options")
#    parser.add_option("-c", "--contig", dest="contigFile",
#                      help="multifasta file of contigs, DNA alphabet", metavar="FILE")    
    required.add_option("-r", "--results", dest="resultDirectory",
                        help="write the bitmaps results in DIRECTORY", metavar="DIRECTORY")   
    required.add_option("-s", "--host", dest="host",
                        help="name of the database host", metavar="NAME")
    required.add_option("-u", "--user", dest="user",
                        help="name of the database user", metavar="NAME")
    required.add_option("-p", "--password", dest="password",
                        help="password of the given user for this database", metavar="PASSWORD")
    required.add_option("-d", "--database", dest="database",
                        help="name of the database", metavar="NAME")
    required.add_option("-c", "--contigs_main", dest="tableMain",
                        help="name of the table of contigs annotations", metavar="NAME")
    required.add_option("-m", "--contigs_mrna", dest="tablemRNA",
                        help="name of the table of contigs - mRNA linking", metavar="NAME")
    required.add_option("-b", "--blast_table", dest="tableHit",
                        help="table of best blast hits for the contigs", metavar="NAME")
        
    facultative=OptionGroup(parser, "Facultative options")
#    facultative.add_option("-b", "--blast_bank", dest="blastdb",
#                           help="database use for blast", metavar="NAME")   
    facultative.add_option("-l", "--log", dest="log",
                           help="log file listing for each chimera its id and list the ids of the proteins picked up with blast", metavar="FILE")    
    facultative.add_option("-t", "--threshold", dest="t",
                           help="threshold for the caracterization of a contig as chimera", metavar="N")
    facultative.add_option("-v", "--verbose", dest="verbose",
                          action="store_true", default=False,
                          help="print all status messages to stdout")   
    facultative.add_option("--demo", action="store_true", dest="demo", default=False,
                           help="print all status messages to stdout")
        
    parser.add_option_group(required) ; parser.add_option_group(facultative)
    
    (options, args) = parser.parse_args()
    outputDir = options.resultDirectory
    logFile = options.log
    t = options.t
    verbose = options.verbose #; bank = options.blastdb
    
    user = options.user
    passwd = options.password
    db = options.database
    host = options.host
    tableMain = options.tableMain
    tablemRNA = options.tablemRNA
    tableHit = options.tableHit
    demo = options.demo
         
    err=findChimera_depts_error()
    if err:
        print err
        #sys.exit(1)
    demo=True #FOR ME
    
    
    if demo:
        ##For tests
        verbose = True
        host = "intron.toulouse.inra.fr"
        user = "sigenae"
        passwd = "s1i2g3!"
        db = "sig_lel_ensembl_mart_40_1"
        tableMain = "acatenellanr_sigenaecontig__contig__main "
        tablemRNA = "acatenellanr_sigenaecontig__est_mrna__dm"
        tableHit = "acatenellanr_sigenaecontig__protein_hit__dm"
        logFile = "findChimera.log"
        outputDir = "./"
        verbose=True

    if not outputDir or not host or not user or not passwd or not db or not tablemRNA or not tableMain or not tableHit:
        print"Please give all the required fields"
        parser.print_help()
        sys.exit(1)
    
    else:
        os.path.curdir=outputDir
        if not t:
            t=0.4
        findChimera(outputDir, float(t), logFile, verbose,tableMain,tablemRNA,host,user,passwd,db)
        sys.exit(0)
#!/usr/bin/perl -I./lib


BEGIN
{
        my ($dir,$file) = $0 =~ /(.+)\/(.+)/;
        unshift (@INC,"$dir/lib");
}


use strict;
use warnings;
use ParamParser;

MAIN:
{
    
    my $o_param = New ParamParser('GETOPTLONG',"blast=s","gff=s");
	$o_param->SetUsage(my $usage=sub { &Usage(); } );
	$o_param->AssertFileExists('blast');
	$o_param->AssertDefined('gff');
	open (IN, $o_param->Get('blast')) or die "Unable to open file : >".$o_param->Get('blast')."<\n";
	open (OUT, ">".$o_param->Get('gff')) or die "Unable to open file : >".$o_param->Get('gff')."<\n";
	while (my $line =<IN>)
	{
		chomp($line);
		next if ($line=~/^#/);
		# Fields: Query id, Subject id, % identity, alignment length, mismatches, gap openings, q. start, q. end, s. start, s. end, e-value, bit score
		#SCL1Contig1	924254	100.00	236	0	0	404	639	2110	2345	2e-131	 468
		my ($est,$contig, $id,$len, $mismatch, $gap,$est_beg,$est_end,$contig_beg, $contig_end, $eval,$bit_score)=split(" " , $line);
		
		my ($beg,$end,$brin) = ($contig_beg < $contig_end ) ? ( $contig_beg,$contig_end,'+' ) : ($contig_end, $contig_beg,'-' );
		# coverti dans le format : # <seqname> <source> <feature> <start> <end> <score> <strand> <frame> Att="Attributes"
		print OUT "$contig\tblast\thsp\t$est_beg\t$est_end\t$eval\t$brin\t.\tID=\"$contig\" Note=\"$contig $beg $end $len\" \n";
#		print OUT "$contig\tblast\thsp\t$beg\t$end\t$eval\t$brin\t.\tID=\"$contig\" Note=\"$est $est_beg $est_end $len\" \n";

	}
	close IN;
	close OUT;	
	
}

=item Usage

        $o_param->Usage();
        Print the usage of the program

=cut

sub Usage
{
print STDERR<<END
Usage:$0
Convert a blast file to a gff one with the Subject as reference.

[Mandatory]

    	--blast		filename     	blast m8 ou m9 format
		--gff			filename	output gff file

[Optionnal]
        --help                       	this message

END
}

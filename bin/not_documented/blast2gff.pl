#!/usr/local/bin/perl -w

use strict;
use Bio::Tools::Run::StandAloneBlast;
use Bio::SearchIO;
use Bio::Tools::GFF;
use Bio::SeqFeature::Generic;

# STDIN : blast file 

# parameter in the line
my $feature_name = shift @ARGV; # name of the feature
my $program = shift @ARGV; # name of the program 
my $gffout = Bio::Tools::GFF->new(-fh => \*STDOUT, -gff_version => 2);

my $in = new Bio::SearchIO(-format => 'blast', -fh => \*ARGV); 
while( my $result = $in->next_result ){
	while( my $hit = $result->next_hit ){
		while( my $hsp = $hit->next_hsp ){
			#if(( $hsp->length('total') > 50 ) && ( $hsp->percent_identity >= 35 ) && ( $hsp->evalue >= 10e-15 )) {	
			# feature definition
			my $feature = new Bio::SeqFeature::Generic(-start => $hsp->start,
											-end => $hsp->end,
											-score => $hsp->score,
											-strand => $hsp->strand,
											#-source_tag => 'blast',
											-primary => $feature_name,
											-source_tag => $program,
											-display_id => $hit->name,
											-seq_name => $hit->name,
											-seq_id => $hit->name,
											-tag => { desc => $hit->description}	
										);			# feature output
			$gffout->write_feature($feature);

			#}
	  	}
	}
}
$in->close();




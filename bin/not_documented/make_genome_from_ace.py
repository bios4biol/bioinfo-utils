#!/usr/local/bioinfo/bin/python2.5
# -*- coding: utf-8 -*-

#
# make_genome_from_ace
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2010 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'beta'



if __name__ == "__main__":  
    from optparse import OptionParser, OptionGroup  
    import os, re, sys, shutil
    from Bio.Sequencing import Ace
    from tempfile import NamedTemporaryFile

    parser = OptionParser(usage="usage: %prog -i filename -o filename")   
    parser.description = "Ace to SAM converter" 
    parser.add_option("-i", "--input", dest="inputAce",
                      help="path/filename of the ace to convert", metavar="FILE")    
    parser.add_option("-o", "--output", dest="output",
                      help="path/filename of the output fasta file", metavar="FILE")    
    parser.add_option("-v", "--verbose", dest="verbose",
                      action="store_true", default=False,
                      help="print all status messages to stdout")   
    (options, args) = parser.parse_args()
    inputAce=    options.inputAce
    output=      options.output
    verbose=     options.verbose
    
    if not inputAce or not output:
        parser.print_help()
        sys.exit()
        
    # parse Ace
    try:
        acefilerecord = Ace.read(open(inputAce, 'r'))   #http://www.biopython.org/DIST/docs/api/Bio.Sequencing.Ace-module.html
    except :
        print "The given file is not an Ace:",inputAce
        parser.print_help()
        sys.exit()
        
    outhandle = open(output,"w")            
    for contig in acefilerecord.contigs:
        outhandle.write(">"+contig.name+"\n"+contig.sequence.replace("*","")+"\n")
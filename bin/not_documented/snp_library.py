#!/usr/local/bioinfo/bin/python2.5

#
# snp_library : Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme bioinformatique Midi Pyrenees'
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'support.genopole@toulouse.inra.fr'

from optparse import *
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
import os, sys, gzip, bz2, string, re

def version_string ():
    """
    Return the snp_library version
    """
    return "snp_library " + __version__

if __name__ == "__main__":
    LINE_LEN=60
    parser = OptionParser(usage="Usage: snp_library.py -d FILE -l FILE -o FILE")
    usage = "usage: %prog -d snp_details_file -l library_file -o file "
    desc = " Generate an array with expression library for each snp allele \n"
           
    parser = OptionParser(usage = usage, version = version_string(), description = desc)
    igroup = OptionGroup(parser, "Input files options","")
    igroup.add_option("-s", "--snp", dest="snp_file",
                      help="The snp file", metavar="FILE")
    
    igroup.add_option("-d", "--detail", dest="detail_file",
                      help="The snp detail file", metavar="FILE")
    igroup.add_option("-l", "--lib", dest="lib_file",
                      help="The library file , col1: EST, col2: library ", metavar="FILE")
    parser.add_option_group(igroup)
    ogroup = OptionGroup(parser, "Output file options","")
    ogroup.add_option("-o", "--output", dest="output",
                      help="The output fasta file", metavar="FILE")
    parser.add_option_group(ogroup)
    (options, args) = parser.parse_args()
    if options.detail_file == None and options.lib_file == None :
        parser.print_help()
        sys.exit(1)
    
    #initialise output filename
    if options.output == None:
        ouput = options.detail_file + ".out"
    else : ouput = options.ouput
    
    #read zipped input 
    lib={}
    all_lib={}
    f=open (options.lib_file, "r")
    for line in f.readlines():
        tab=line.split()
        lib[tab[0]]=tab[1]
        all_lib[tab[1]]=1
    all_lib["Unknown"]=1    
    f.close()

    f=open (options.detail_file, "r")
    snp={}
    #snp_F0F0A3T01BB8JI_166    F0F0A3T01EQ0L7    144    A
    for line in f.readlines():
        
        tab=line.split()
        if not snp.has_key(tab[0]) : # SNP existant
            snp[tab[0]]={}
        if not snp[tab[0]].has_key(tab[3]) : # Allele existant
            snp[tab[0]][tab[3]]={}
        if not lib.has_key(tab[1]) :
            lib[tab[1]]="Unknown"
        
        if not snp[tab[0]][tab[3]].has_key(lib[tab[1]]) :
            snp[tab[0]][tab[3]][lib[tab[1]]]=1
        else:
            snp[tab[0]][tab[3]][lib[tab[1]]]=snp[tab[0]][tab[3]][lib[tab[1]]]+1
            

            
        
    # write down seqs
    output_handle = open(ouput, "w")
    line="SNP\tAllele\t"
    for onelib in sorted(all_lib.keys()) :
        line=line+onelib+"\t"
    output_handle.write(line+"\n")
    for onesnp in sorted(snp.keys()):
        for allele in sorted(snp[onesnp].keys()) :
            line = onesnp+"\t"+allele+"\t"
            for onelib in sorted(all_lib.keys()) :
                if snp[onesnp][allele].has_key(onelib):
                    line=line+str(snp[onesnp][allele][onelib])+"\t"
                else :
                    line=line+"0"+"\t"
        output_handle.write(line+"\n")            
   
    output_handle.close()
    sys.exit(0)
        

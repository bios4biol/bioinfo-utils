#!/usr/bin/perl -I../lib


BEGIN
{
        my ($dir,$file) = $0 =~ /(.+)\/(.+)/;
        unshift (@INC,"$dir/../lib");
}


use strict;
use warnings;
use ParamParser;
use Bio::Seq;
use Bio::SeqIO;


MAIN:
{
    
    my $o_param = New ParamParser('GETOPTLONG',"input=s","nb_seq_per_file=s" ,"output_dir=s");
	$o_param->SetUsage(my $usage=sub { &Usage(); } );

	$o_param->AssertFileExists('input');
	$o_param->SetUnlessDefined('nb_seq_per_file',1);
	$o_param->SetUnlessDefined('output_dir','.');
	#$o_param->AssertDefined('output_dir');
	my $out_dir = $o_param->Get('output_dir');
	my $input_file=$o_param->Get('input');
	if ( !-e $out_dir)
	{
		mkdir ($out_dir);
		die "Unable to create >$out_dir< directory\n" if (!-e $out_dir);
	}
	my $nb_seq=	$o_param->Get('nb_seq_per_file');
	# read fasta file
    my $seq = Bio::Seq->new();
    my $in = Bio::SeqIO->new( -file =>  $input_file, '-format' => 'Fasta' );
    
	my $file = $input_file ;

	if ( $file=~/\// )
	{
	 ($file) = $input_file =~ /.+\/(.+)/;
	}
    my $cpt = 0;
    my $cpt_file = 1;
  
    my $filename;
	if ( $nb_seq > 1 ) {
		$filename = "$file.$cpt_file";
	}			
	else 
	{
		# each sequence is outputed in it own fasta, named by it id_length.fasta
		$seq = $in->next_seq();
		$cpt = 1; #to be sure that every sequence has it own fasta
		$filename = $seq->id()."_".$seq->length();
		while ( -e "$out_dir/".$filename.".fasta" )
		{
				$filename .= "_n".$cpt_file;
		}
		$filename.=".fasta";
	}

	my $out = Bio::SeqIO->new( -file =>  ">$out_dir/$filename", '-format' => 'Fasta' );
	
	if ( $nb_seq == 1 ) {
		$out->write_seq($seq);
	}
	
	while( $seq = $in->next_seq())
    {
    	
		if ($cpt >= $nb_seq)
		{
			#ouverture nouveau fichier 
			$cpt_file ++;
			if ( $nb_seq > 1 ) {
				$filename = "$file.$cpt_file";
			}			
			
			else {
				$filename = $seq->id()."_".$seq->length();
				while ( -e "$out_dir/".$filename.".fasta" )
				{
						$filename .= "_n".$cpt_file;
				}
				$filename.=".fasta";				
			}
			
			$out = Bio::SeqIO->new( -file =>  ">$out_dir/$filename", '-format' => 'Fasta' );
			$cpt=0;
		}
		$out->write_seq($seq);
		$cpt ++;
    }
    
}


=item Usage

        $o_param->Usage();
        Print the usage of the program

=cut

sub Usage
{
print STDERR<<END
Usage:$0
Split multifasta blast in N multifasta file with nb_seq_per_file sequences.

[Mandatory]

    	--input				filename     	multifasta file
		--nb_seq_per_file	integer			number of sequence per multifasta result files
		--output_dir		directory		output directory     
[Optionnal]
        --help                       	this message

END
}

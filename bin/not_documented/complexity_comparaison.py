#!/usr/local/bioinfo/bin/python2.5

from Bio import SeqIO
from optparse import *
import string
import zlib
import os
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import sys
import pylab
from matplotlib.ticker import NullFormatter, MultipleLocator, FormatStrFormatter


parser = OptionParser(usage="Usage: complexity_comparaison.py -i input -t title -s max_score")
parser.add_option("-i", "--input", dest="input", help="The distance complexity output file", metavar="FILE")
parser.add_option("-t", "--title", dest="title", help="The plot title", metavar="FILE")
parser.add_option("-s", "--score", dest="score", help="The score which indicate that sequence are identic")
(options, args) = parser.parse_args()
if options.input == None or options.score == None:
    parser.print_help()
    sys.exit(0);
file = open(options.input, 'r')
iden = []
diff = []
score= int(options.score)
for line in file:
    tab = line.rstrip().split()
    #recupere la complexit
    if int(tab[1]) == score :
	iden.append(int(tab[2])) 
    else :
	diff.append(int(tab[2]))

file.close ()
plt.clf()

n, bins, patches = plt.hist(diff, 25, normed=0, facecolor='blue')
n2, bins2, patches2 = plt.hist(iden, 20, normed=0, facecolor='blue')
plt.clf()
plt.plot(bins[:-1],n, '-')
plt.plot(bins2[:-1],n2, 'r--')
## legend
pylab.legend((r'Reads not in cluster', r'Reads in clusters'), shadow = True, loc = (0.01, 0.55))
plt.xlabel('Complexity')
plt.ylabel('Number of reads')
if options.title == None:
	options.title="Distribution of the complexity"
plt.title(options.title)
plt.grid(True)
plt.savefig( os.path.basename(options.input) + ".diff.png")


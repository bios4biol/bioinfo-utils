#!/usr/bin/perl -w
# read as stdin multi fasta
# write as stdout seq_ac length

use strict;
use Getopt::Long;
use Pod::Usage;


# Options definitions (name,format,default,required)
my @Options = ( ['help',undef,undef,0],
		['man',undef,undef,0],
	      );
# defaults values
my %Values=();
my %Required=();
map {$Values{$_->[0]}=$_->[2];$Required{$_->[0]}=$_->[3];} @Options;
# build options list
my %Options=();
map {$Options{defined($_->[1])?$_->[0].'='.$_->[1]:$_->[0]}=\$Values{$_->[0]};} @Options;
# retrieve options
GetOptions(%Options) || pod2usage(2);
pod2usage(1) if ($Values{'help'});
pod2usage(-exitstatus => 0, -verbose => 2) if ($Values{'man'});

# check required
map {pod2usage("$_ is required") if ($Required{$_}&&!defined($Values{$_}));} keys %Values;

my $ac;
my $seq=-1;
while(my $line=<STDIN>)
  {
    $line=~s/\s+$//;
    if ($line =~ /^>(\S+)/)
      {
	printf "$ac\t%d\n", $seq if ($seq != -1);
	$ac=$1;
	$seq=0;
	next;
      }
	#$line=~s/\d+ ?/a/g; # uncomment for fasta phred quality file
    $line=~s/[^a-z]//ig;
    $seq+=length($line);
  }
printf "$ac\t%d\n", $seq if ($seq != -1);

#============================================================

=head1 NAME

mfasta_length.pl

=head1 SYNOPSIS

mfasta_length.pl [--help|--man] < fasta_file.fa

=head1 OPTIONS

=over 8

=item B<-help>

Print a brief help message and exits.

=item B<-man>

Prints the manual page and exits.

=back

=head1 DESCRIPTION

Reads as STDIN a multi fasta file, and writes as STDOUT a two columns tab delimited text : AC	nb_bases
This for each fasta entry

=head1 AUTHORS

Patrice DEHAIS

=head1 CONTACT

Questions can be posted to the sigenae mailing list:
sigenaesupport@jouy.inra.fr

=cut

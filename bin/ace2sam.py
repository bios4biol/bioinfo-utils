#!/usr/local/bioinfo/bin/python2.5
# -*- coding: utf-8 -*-

#
# aceToSam
# Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


from optparse import OptionParser, OptionGroup  
import os, re, sys, shutil
from Bio.Sequencing import Ace
from tempfile import NamedTemporaryFile, TemporaryFile

__name__ = "ace2sam.py"
__synopsis__ = "ace2sam.py -i assembly.ace -o asssembly.sam --verbose"
__date__ = "06/2012"
__authors__ = "Nicolas Alias"
__keywords__ = "assembly ace sam"
__description__ = "Convert an ace file to a sam file"
__version__ = '1.0'
__copyright__ = 'Copyright (C) 2010 INRA'
__license__ = 'GNU General Public License'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'beta'


def makeCigarAndStats(seq_reference, seq_read, clipped_start, clipped_end):
    """
    Receive aligned read and reference hard clipped (both) and produce the cigar
        Contig  Read    cigar
        A        A        M (match)
        G        A        M (match)
        *        A        I (insertion)
        A        *        D (deletion)
        A        n/N/.    N (skipped)
        *        *        P (padded)
        G        a        S (soft clipped)  ! edges !
        G                 H (hard clipped) ! edges ! -> impossible in ACE
    @param    seq_reference    string, alignment reference
    @param    seq_read         string, to be aligned on the reference
    @param    clipped_start    int, relative start of the read clipped subsequence on the contig
    @param    clipped_end      int, relative end of the read clipped subsequence on the contig
    @return   [cigar, stats]   string, the cigar line, a zipped alignment information
    """
    str_cigar = "" #temporary string, like MMMPMMMM for a cigar of 3M1P4M
    matches = 0
    mismatches = 0
    
    for pos in range( len(seq_read) ):                     
        try:
            char_ref = seq_reference[pos]
        except IndexError:
            break #read overlap: ignore         
        char_read = seq_read[pos]
        
        # reference is gapped
        if char_ref=="*": 
            
            # padded in read
            if char_read == "*":
                str_cigar+="P" 
                 
            # insertion of one base
            else:                 
                str_cigar+="I" 
                
        # reference has a nucleotide here
        else:
            
            # unkown base, skipped region
            if char_read in ["n", "N", "."] :
                str_cigar+="N"
                
            # deletion
            elif char_read=="*":
                str_cigar+="D"  
            
            # read has a nucleotide too
            else:
                str_cigar+="M"  
                            
        # count match/mismatch if the last -so current- cigar operation is a M
        if str_cigar[-1]=="M":
            if char_read.upper() == char_ref.upper() and char_read!="*":
                matches +=1 #perfect hit
            else:
                mismatches+=1
             
    # final cigar: zipping from MMMMIMM to 4M1I2M
    cigar =""
    occurence = 0
    current_char = ""
    
    # force process for last part of cigar
    for char in str_cigar:
        if not current_char: #init
            occurence = 1
            current_char = char
        elif current_char == char:
            occurence += 1
        else: # change char : save value...
            cigar += str(occurence)+current_char 
            #...and init
            occurence = 1 
            current_char = char        
    cigar += str(occurence)+current_char  #process last
    
    # add the clipped parts (from read.qa info)
    clip_s="" ; clip_e=""
    
    if clipped_start:
        clip_s=str(clipped_start)+"S"    
        
    if clipped_end:
        clip_e=str(clipped_end)+"S" 
    cigar= clip_s+cigar+clip_e      

    stats = [matches, mismatches]     
    return cigar, stats

def editReadSequence(read_seq, cigar):
    """
    @param    read_seq    string    read sequence as parsed in the ACE
    @param    cigar       string    cigar of the alignment of the current read on the reference
    @return   read_seq    string    read sequence shortened (gapless and N-less, excepted for soft clipped part (as will be in SAM file)
    """
    list_read = list(read_seq)
    pos = len(list_read) -1
    # reversed is a Python fix for list deletion
    for ops in reversed(re.findall("\d+[A-Z]",cigar)):
        number=int(ops[:-1])
        type_op = ops[-1]
        #do nothing on soft clipped sequences or inserted N
        if type_op in ["S", "I"]:
            pos -= number
            continue
        else:
            for i in range(number):
                if list_read[pos] in ["*", "N", "n", "."]: #remove the elements given by the cigar
                    list_read.pop(pos)        
                pos -=1 
    return "".join(list_read)


def compute_quality(contig_seq , read_seq, mask, base):
    """
    Convolution-like computing of a quality score for each base (useful in SNP detection, samtools pileup)
    @param    contig_seq    string        contig sequence
    @param    read_seq      string        read shortenend sequence, as required in SAM
    @param    mask          list of int   convolution mask used to compute quality base per base
    @param    base          int           ASCII-base for quality in SAM (current version is 33)
    @return   quality       string        quality sequence in the ASCII-base format
    """
    if len(read_seq)<=len(mask):
        return "*" 
    
    qualities = []    
    masksize = len(mask)
    half_size = masksize/2    #only for odd mask's size
        
    for pos in range(len(read_seq)): 
         
        #gapped or skipped position: only in a clipped sequence, quality null
        if read_seq[pos] in ["*", "N", "n", "."]:
            qualities.append(0)
            continue
        
        #in the sequence: values for substring
        curr_mask = mask
        ctg_sub_start = pos -half_size ;     ctg_sub_stop  = pos +half_size +1
        rd_sub_start  = pos -half_size     ; rd_sub_stop   = pos +half_size +1
              
        #begin of the sequence: pos < 1/2 len(mask): correct values -> don't select non-existing letters
        if pos<half_size:
            ctg_sub_start = 0
            rd_sub_start  = 0
            curr_mask = curr_mask[ half_size -pos :]            
        remaining_chars = len(read_seq) -(pos+1)
        
        #end of the sequence
        if remaining_chars < half_size:            
            ctg_sub_stop  = pos +remaining_chars +1
            rd_sub_stop   = pos +remaining_chars+1
            curr_mask = curr_mask[: half_size +1 +remaining_chars]          
            
        str_ctg = contig_seq  [ctg_sub_start : ctg_sub_stop]
        str_rd =  read_seq    [rd_sub_start  : rd_sub_stop]     
        base_qual = 0
        for i in range(len(curr_mask)):                
            if i >= len(str_ctg): #contig shorter: ignore overlap      
                break 
            if str_ctg[i].upper() == str_rd[i].upper():
                base_qual += curr_mask[i]                 
        qualities.append(base_qual)
    #conversion into ASCII-33 (default)
    result = "".join([ chr(i+base) for i in qualities])
    return result


##### MAIN #####

parser = OptionParser(usage="usage: %prog -i filename -o filename")   
parser.description = "Ace to SAM converter" 
parser.add_option("-i", "--input", dest="inputAce",
                  help="path/filename of the ace to convert", metavar="FILE")    
parser.add_option("-o", "--output", dest="output",
                  help="path/filename of the output SAM file", metavar="FILE")    
parser.add_option("-m", "--mask", dest="mask",
                  help="mask used for quality computation, odd succession of integer, separated by a comma", metavar="STRING")  
parser.add_option("-q", "--quality-base", dest="opt_qual",
                  help="ASCII conversion of quality", metavar="INT")
parser.add_option("-v", "--verbose", dest="verbose",
                  action="store_true", default=False,
                  help="print all status messages to stdout")   
(options, args) = parser.parse_args()
inputAce=    options.inputAce
output=      options.output
verbose=     options.verbose
opt_qual=    options.opt_qual
mask=        options.mask    
str_error = ""

if mask: # odd mask size is not pair !
    if len(re.findall("\d+",mask))%2==0:
        print "Do not use this option unless the mask's size is odd ! Examples: 1,2,1 or 5,10,15,10,5"
        sys.exit()
    mask = [int(i) for i in re.findall("\d+",mask)]
    if sum(mask)>93:
        print "Your mask sum exceeds the maximum allowed value (decimal 93). Please use lower values"
        sys.exit()
if not mask:
    mask=[5, 10, 15, 10, 5]

if not inputAce or not output or inputAce==output:
    parser.print_help()
    sys.exit()
elif not os.path.exists(inputAce):
    print "The input file",inputAce,"doesn't exist !"
    parser.print_help()
    sys.exit()
#else: proceed

print "Starting.."
    
try:# parse Ace
    acefilerecord = Ace.read( open(inputAce, 'r') )   
    # from http://www.biopython.org/DIST/docs/api/Bio.Sequencing.Ace-module.html
except :
    print "The given file is not an Ace:",inputAce
    parser.print_help()
    sys.exit()

tmp_head=NamedTemporaryFile(mode='w+b', suffix='.sam.head')#tempfile used to store the header
tmp_body=NamedTemporaryFile(mode='w+b', suffix='.sam.body')#tempfile used to store the alignment section

# HEADER
CLI = ""         # if multiple times in an ace file,
pgr_version = "" # keep only the least of each ?
aln_program = "" #

if acefilerecord.wa:
    for annot in acefilerecord.wa:
        if annot.info: #[ "CLI" , "prog_name version" ]
            CLI=annot.info[0]
            pgr_version=annot.info[1].split()[-1] 
            aln_program=annot.info[1].split()[0]
            
iterations = 0
for contig in acefilerecord.contigs:        
    tmp_head.write("@SQ"+"\t"+"SN:"+contig.name+"\t"+"LN:"+str(len(contig.sequence))+"\n" )    

    # ALIGNMENT SECTION      
    for r in contig.reads:

        iterations+=1
        if (20*iterations)%(acefilerecord.nreads)==0 :
            sys.stdout.write(".") ; sys.stdout.flush()  

        read = r.rd         
        clip_start = r.qa.align_clipping_start
        clip_end = r.qa.align_clipping_end   
        
        # process all         
        if clip_start == clip_end == 1:
            clip_start = 0 ; clip_end = len(read.sequence)
            
        #everything's clipped    
        if clip_start == clip_end == -1:
            cigar ="*" ; seq_read = "*" ; stats = None
            
        # compute read's sequence, cigar and statistics
        else:
            # look for the corresponding read declaration (AF)
            for ctg_af in contig.af:
                if ctg_af.name == read.name :                    
                    flag = 0 #forward
                    if ctg_af.coru == "C": #reverse
                        flag = 16      
                                   
                    read_padded_start = ctg_af.padded_start
   
            #cut ends of read 's sequence (do hard clip)
            read_sequence = read.sequence[clip_start-1: clip_end]                 
            offset = clip_start -1 + read_padded_start -1                         
            seq_reference = contig.sequence[ offset : offset + len(read_sequence)]
            read_start = offset
                            
            #uncount gaps before the read
            read_start -= len( re.findall("\*", contig.sequence[:offset+1]) )

            cigar, stats = makeCigarAndStats(seq_reference, read_sequence, clip_start-1, len(read.sequence)-clip_end)
            
            # Verify the alignment starts with an insertion : not counted gap, shift to the right
            tmp_cigar = cigar
            # cigar starts with a Soft clip, jump over it and look what's next
            if re.match("\d+S", tmp_cigar):
                tmp_cigar = tmp_cigar[ len( re.match("\d+S", tmp_cigar).group() ):]
            # cigar starts with Insertions, count them (sam alignment do not show gaps in reference)
            if re.match("\d+I", tmp_cigar):
                num = re.match("[0-9]+", tmp_cigar).group() 
                read_start += 1 #shift of 1 : count first insertion
            
            # read sequence as in the SAM
            seq_read = editReadSequence(read.sequence, cigar)
            
        # Unused fields
        mapq=255 #unavailable
        rmnm = "*" ; mpos=0 ; isize =0 #no paired ends                  
        
        # Quality
        base = 33
        if opt_qual:
            base = int(opt_qual[1:])                
        qual = compute_quality(seq_reference , seq_read, mask, base)
            
        # Controls
        sum_padded_cigar=0
        sum_unpadded_cigar=0
        for i in re.findall("\d+[A-Z]", cigar):
            if i[:-1].isdigit and i[-1] not in ["D", "N", "P"]:
                sum_unpadded_cigar+=int(i[:-1])
            if i[:-1].isdigit:
                sum_padded_cigar+=int(i[:-1])                
       
        if not len(read.sequence)==sum_padded_cigar:
            print "\nERROR PADDED", cigar
            sys.exit()                
       
        if not len(seq_read)==len(qual)==sum_unpadded_cigar :
            print "\nERROR UNPADDED", cigar
            sys.exit()                            
            
        # Optional fields
        options = ""
        if stats:
            perfect, mismatches = stats
            if perfect != -1:
                options+="HO:i:"+str(perfect)
            if mismatches != -1:
                options+="\tNM:i:"+str(mismatches)
        read_start +=1
        line = [str(i) for i in [read.name, flag, contig.name, read_start, 
                mapq, cigar, rmnm, mpos, isize, seq_read, qual, options]]
            
        tmp_body.write("\t".join(line)+"\n")   
    
if aln_program and CLI and pgr_version:
    tmp_head.write("@PG"+"\t"+"ID:"+aln_program+""+"\t"+"CL:"+CLI+""+"\t"+"VN:"+pgr_version+"\n") # VN:1.0
tmp_head.write("@CO"+"\t"+"Generated from "+inputAce+" quality generated using a convolution mask of "+str(mask)+" and coded in ASCII-"+str(base)+"\n")

tmp_head.seek(0) ;  tmp_body.seek(0) # go to the beginning of these files.
destination = open(output,'w')
shutil.copyfileobj(open(tmp_head.name,'r'), destination)
shutil.copyfileobj(open(tmp_body.name,'r'), destination)
destination.close()    
print "Written",destination.name
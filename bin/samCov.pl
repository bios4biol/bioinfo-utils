#!/usr/bin/perl -w
use strict;
use Getopt::Long;
use Pod::Usage;

# Options definitions (name,format,default,required)
my @Options = ( ['help',undef,undef,0],
		['man',undef,undef,0],
		['plot',undef,undef,0],
		['S',undef,undef,0],
		['f','s',undef,0],
		['width','i',undef,0],
		['regions','s@',undef,0],
		['lw','i',1,0],
		['norepeat','i',0,0],
		['d','i',8000,undef],
		['mincov','i',undef,0],
    );
# defaults values
my %Values=();
my %Required=();
map {$Values{$_->[0]}=$_->[2];$Required{$_->[0]}=$_->[3];} @Options;
# build options list
my %Options=();
map {$Options{defined($_->[1])?$_->[0].'='.$_->[1]:$_->[0]}=\$Values{$_->[0]};} @Options;
# retrieve options
GetOptions(%Options) || pod2usage(2);
pod2usage(1) if ($Values{'help'});
pod2usage(-exitstatus => 0, -verbose => 2) if ($Values{'man'});

# check required
map {pod2usage("$_ is required") if ($Required{$_}&&!defined($Values{$_}));} keys %Values;

# check reference file
if ($Values{f}) {
    pod2usage("no such genome reference file: $Values{f}") unless (-e $Values{f});
}

$#ARGV==0 || pod2usage("No SAM file given as parameter");


#------------------------------------------------------------
# retrieve wanted regions coordinates
my %Regions=(); # coordinates of wanted regions for reference sequences
if ($Values{regions}) {
    foreach my $region (split(/,/,join(',',@{$Values{regions}}))) {
	my ($chr,$start,$end);
	if (($chr,$start,$end)=($region=~/^(\S+):(\d+)-(\d+)$/)) {
	    $start<$end || die "invalid region $chr:$start:$end";
	    die "Only one region per reference: $chr" if (exists($Regions{$chr}));
	    $Regions{$chr}{start}=$start;
	    $Regions{$chr}{end}=$end;
	}
	elsif (($chr,$start)=($region=~/^(\S+):(\d+)$/)) {
	    die "Only one region per reference: $chr" if (exists($Regions{$chr}));
	    $Regions{$chr}{start}=$start;
	    $Regions{$chr}{end}=undef;
	}
	elsif (($chr)=($region=~/^(\S+)$/)) {
	    die "Only one region per reference: $chr" if (exists($Regions{$chr}));
	    $Regions{$chr}{start}=undef;
	    $Regions{$chr}{end}=undef;
	}
	else {
	    die "Bad region definition $region";
	}
    }
}

#------------------------------------------------------------
# read BAM/SAM header
my $sam= $Values{S}?'-S':'';
my %Ref=(); # length of reference sequences
open(FIN,"samtools view $sam -H $ARGV[0]|")||die "Can't open file $ARGV[0] with samtools view";
while(my $line=<FIN>) {
    if($line=~/SN:(\S+).+LN:(\d+)/) {
	$Ref{$1}=$2;
    }
}
close(FIN);
scalar keys %Ref || die "not header found if $ARGV[0]";

#------------------------------------------------------------
# read reference file
my %Seq=();
if ($Values{f}) {
    open(FIN,$Values{f}) || die "Can't open reference file $Values{f}";
    my $seq='';
    my $ac=undef;
    while (my $line=<FIN>) {
	if($line=~/^>(\S+)/) {
	    if ($ac) {
		#@{$Seq{$ac}}=split//,$seq;
		$Seq{$ac}=$seq;
		my $l=length($seq);
		$l==$Ref{$ac} || die "header and reference lengthes differ:\t$Ref{$ac}\t$l"
	    }
	    exists($Ref{$1}) || die "reference $1 not in sam/bam header";
	    $seq='';
	    $ac=$1;
	    next;
	}
	$line=~s/\s+//g;
	$seq.=$line;
    }
    close(FIN);
    if ($ac) {
	$Seq{$ac}=$seq;
    }
}

map {
    exists($Ref{$_}) || die "unknown reference in given regions: $_";
    !defined($Values{f}) || exists($Seq{$_}) || die "reference sequence not provided: $_";
} keys %Regions;

#------------------------------------------------------------
# return the point on the graphic for a given pos
sub get_point ( $ $ $ $ ) {
    my ($pos,$start,$width,$ref_length)=@_;
    return sprintf("%.0f",($pos-$start+1)*$width/$ref_length);
}

#------------------------------------------------------------
# return mean pos for a point on the graphic
sub get_pos ( $ $ $ $ ) {
    my ($point,$start,$width,$ref_length)=@_;
    return sprintf("%.0f",$point*$ref_length/$width+$start-1);
}

#------------------------------------------------------------
# return RGB int encoded
sub rgb ( $ $ $ ) {
    my ($r,$g,$b)=@_;
    return 65536 * int($r) + 256 * int($g) + int($b);
}

#------------------------------------------------------------
# plot data and display summary
my %GlobCov=(nb => 0,
	     min_depth => -1,
	     avg_depth => 0,
	     max_depth => 0,
	     Nzone => 0,
	     lg => 0,
    );
sub display_cov( $ $ * * * * $ $ * ) {
    my ($ref,$fname,$cov_ptr,$rep_ptr,$Nzones_ptr,$stats_ptr,$last_pos,$last_point,$depth_ptr)=@_;
    my $start=$stats_ptr->{start};
    my $end=$stats_ptr->{end};
    my $lg=$stats_ptr->{lg};
    my $width=$stats_ptr->{width};
    
    printf STDOUT ("%s_$start-$end\t%d (%.0f%%)\t%s\t%.0f\t%.0f\t%.0f\n",
		   $ref,$stats_ptr->{nb},
		   $stats_ptr->{nb}*100/$lg,
		   $Values{f}?sprintf("%.0f%% (N:%d)",$stats_ptr->{nb}*100/($lg-$stats_ptr->{Nzone}),$stats_ptr->{Nzone}):'NA',
		   $stats_ptr->{min_depth},
		   $stats_ptr->{nb}==0?0:$stats_ptr->{avg_depth}/$stats_ptr->{nb},
		   $stats_ptr->{max_depth});
    
    # compute global Coverage
    $GlobCov{nb}+=$stats_ptr->{nb};
    $GlobCov{min_depth}=$stats_ptr->{min_depth} if ($GlobCov{min_depth}==-1 || 
						    $GlobCov{min_depth}>$stats_ptr->{min_depth});
    $GlobCov{max_depth}=$stats_ptr->{max_depth} if($GlobCov{max_depth}<$stats_ptr->{max_depth});
    $GlobCov{avg_depth}+=$stats_ptr->{avg_depth};
    $GlobCov{lg}+=$stats_ptr->{lg};
    $GlobCov{Nzone}+=$stats_ptr->{Nzone};
    
    if ($Values{plot}) {
	# compute N and repeats plotting for data between last pos and end of region
	for (my $i=$last_pos;$i<=$end;$i++) {
	    next unless exists($Seq{$ref});
	    next if ($i>=$Ref{$ref});
	    my $b=substr($Seq{$ref},$i-1,1);
	    next unless defined($b);
	    my $point=get_point($i,$start,$width,$lg);
	    $Nzones_ptr->{$point}++ if ($b=~/N/i);
	    $rep_ptr->{$point}++ if ($b=~/[a-z]/);
	}
	# compute depth
	my $mean_depth=0;
	my $nb_reads=0;
	if (%{$depth_ptr}) {
	    map {
		$mean_depth+= ($_*$depth_ptr->{$_});
		$nb_reads+= $depth_ptr->{$_};
	    } keys %{$depth_ptr};
	    $mean_depth/=$nb_reads;
	}
	$cov_ptr->{$last_point}=$mean_depth;
	
	# init new plot
	open(PLOT,"|gnuplot 2>/dev/null") || die "Can't generate coverage graph with gnuplot";
	printf PLOT "set terminal jpeg size %.0f,400\n",$width;
	printf PLOT "set xrange [$start:$end]\n";
	printf PLOT "set output '%s_%d-%d_%s.jpeg'\n",$ref,$start,$end,$fname;
	if ($Values{f}) {
	    print PLOT "set tmargin 0\n";
	    print PLOT "set bmargin 0\n";
	    print PLOT "set lmargin 10\n";
	    printf PLOT "set multiplot title '$fname Chr $ref coverage'\n";
	    print PLOT "unset xtics\n";
	    print PLOT "set logscale y\n";
	    print PLOT "set origin 0,0.15\n";
	    print PLOT "set size 1,0.80\n";
	    print PLOT "plot '-' with impulses lc rgbcolor 'blue' lw $Values{lw} title ''\n";
	}
	else {
	    print PLOT "set title '$fname Chr $ref coverage'\n";
	    print PLOT "set logscale y\n";
	    printf PLOT "set xtics out %d format '%%.2g'\n",($end-$start+1)/100;
	    print PLOT "plot '-' with impulses lc rgbcolor 'blue' lw $Values{lw} title ''\n";
	}
	
	# start with coverage print
	for (my $point=1;$point<$width;$point++) {
	    printf PLOT "%d\t%d\n",get_pos($point,$start,$width,$lg),exists($cov_ptr->{$point})?$cov_ptr->{$point}:0.5;
	}
	print PLOT "e\n";
	
	# continue with N and repeats if needed
	if ($Values{f}) {
	    # create track for N zones
	    print PLOT "unset logscale y\n";
	    print PLOT "unset ytics\n";
	    print PLOT "set origin 0,0.1\n";
	    print PLOT "set size 1,0.05\n";
	    print PLOT "set yrange [-5:0]\n";
	    print PLOT "plot '-' using 1:2:3 with impulses lc rgb variable lw $Values{lw} title 'N zones'\n";
	    # for each point the corresponding region has a mean length equal to 2*lg/width-1
	    my $delta=2*$lg/$width-1;
	    $delta=int($delta)+1 if($delta!=int($delta));
	    for (my $point=1;$point<$width;$point++) {
		my $N_density=0;
		if (exists($Nzones_ptr->{$point})) {
		    $N_density= $Nzones_ptr->{$point}/$delta;
		    my $black=sprintf("%.0f",(1-$N_density)*255);
		    printf PLOT "%d\t-5\t%d\n",get_pos($point,$start,$width,$lg),rgb($black,$black,$black);
		}
		else {
		    printf PLOT "%d\t0\t%d\n",get_pos($point,$start,$width,$lg),rgb(255,255,255);
		}
	    }
	    print PLOT "e\n";
	    
	    # create track for repeats
	    printf PLOT "set xtics out %d format '%%.2g'\n",$lg/100;
	    print PLOT "set origin 0,0.06\n";
	    print PLOT "set size 1,0.04\n";
	    print PLOT "set yrange [-5:0]\n";
	    print PLOT "plot '-' using 1:2:3 with impulses lc rgb variable lw $Values{lw} title 'Repeats'\n";
	    for (my $point=1;$point<$width;$point++) {
		my $R_density=0;
		if (exists($rep_ptr->{$point})) {
		    # for each point the corresponding region has a length equal to 2*lg/width-1
		    # thus the density of repeats is
		    $R_density= $rep_ptr->{$point}/$delta;
		    my $red=sprintf("%.0f",(1-$R_density)*255);
		    printf PLOT "%d\t-5\t%d\n",get_pos($point,$start,$width,$lg),rgb(255,$red,$red); 
		}
		else {
		    printf PLOT "%d\t0\t%d\n",get_pos($point,$start,$width,$lg),rgb(255,255,255);
		}
	    }
	    print PLOT "e\n";
	    print PLOT "unset multiplot\n";
	}
	print PLOT "quit\n";
	close(PLOT);
    } # end plot
}

#------------------------------------------------------------
# loop on mpileup output
my $ref='';
my $width=0;
my $lg=0;
my $current_pos=0;
my $last_pos=0;
my $point=0;
my $last_point=0;
my %Depth=();
my %Stats =();
my ($fname)=($ARGV[0]=~/([^\/]+)$/);
my %Repeats=();
my %Nzones=();
my %Coverage=();

my $regions = join(' ',map {sprintf("%s%s%s",$_,$Regions{$_}{start}?":$Regions{$_}{start}":'',$Regions{$_}{end}?"-$Regions{$_}{end}":'');} keys %Regions);
$regions eq '' || $sam eq '' || die "Can't use regions with sam";
$regions eq '' || -e "$ARGV[0].bai" || die "no $ARGV[0].bai found, bam needs to be indexed";
open(FIN,"samtools view $sam -b $ARGV[0] $regions |samtools mpileup -A -B -d $Values{d} - 2>/dev/null |") || die "problem with mpileup";
print STDOUT "chr\tcoverage\tno_N_coverage\tmin_depth\tavg_depth\tmax_depth\n";
while (my $line=<FIN>){
    chomp $line;
    my @F=split/\t/,$line;

    # check that position is in the given regions, if provided
    my $pos_ok=1;
    if(exists($Regions{$F[0]})) { # a region was specified for this chr
	$pos_ok=0;
	if(!defined($Regions{$F[0]}{start})){
	    $pos_ok=1; # full chr specified, all pos are ok
	}
	elsif($Regions{$F[0]}{start}<=$F[1]){ # current pos inside region ?
	    if(!defined($Regions{$F[0]}{end}) || $F[1]<=$Regions{$F[0]}{end}){
		$pos_ok=1;
	    }
	}
    }
    $pos_ok || next;

    $current_pos=$F[1];
    
    if ($F[0] ne $ref) { # new reference
	# display previous one
	display_cov($ref,$fname,\%Coverage,\%Repeats,\%Nzones,\%Stats,$last_pos,$last_point,\%Depth) if ($ref ne '');
	
	# reset counters
	$ref=$F[0];
	%Stats = (
	    nb => 0,
	    min_depth => 0,
	    avg_depth => 0,
	    max_depth => 0,
	    Nzone => 0,
	    start => 0,
	    end => 0,
	    lg => 0,
	    width => 0,
	    );
	# compute graph width
	$Stats{start}=$Regions{$ref}{start}?$Regions{$ref}{start}:1;
	$Stats{end}=$Regions{$ref}{end}?$Regions{$ref}{end}:$Ref{$ref};
	$Stats{lg}=$Stats{end}-$Stats{start}+1;
	$Stats{width}= defined($Values{width})?$Values{width}:sprintf("%.0f",log($Stats{lg})/log(10)*1000);
	
	# compute nb N on region 
	if (exists($Seq{$ref})) {
	    $Stats{Nzone}= substr($Seq{$ref},$Stats{start}-1,$Stats{end}-$Stats{start}+1)=~s/N/N/ig;
	}
	($Stats{Nzone}+=1)--; # to have 0 if no N

	if ($Values{plot}) {
	    if ($Values{f} && !exists($Seq{$ref})) {
		die "Sequence not provided for reference $ref";
	    }
	    
	    %Depth=();
	    %Repeats=();
	    %Nzones=();
	    %Coverage=();
	    	    
	    # last pos and points
	    $last_pos=$Stats{start};
	    $last_point=get_point($Stats{start},$Stats{start},$Stats{width},$Stats{lg});
	}
    }
    
    if ($Values{plot}) {
	# compute N and repeats plotting for data between last and current pos
	for (my $i=$last_pos;$i<$current_pos;$i++) {
	    next unless exists($Seq{$ref});
	    next if ($i>=$Ref{$ref});
	    next if ($i>=length($Seq{$ref}));
	    my $b=substr($Seq{$ref},$i-1,1);
	    next unless defined $b;
	    $point=get_point($i,$Stats{start},$Stats{width},$Stats{lg});
	    $Nzones{$point}++ if ($b=~/N/i);
	    $Repeats{$point}++ if ($b=~/[a-z]/);
	}
	$last_pos=$current_pos;
	
	# compute depth
	$point=get_point($current_pos,$Stats{start},$Stats{width},$Stats{lg});
	if ($point!=$last_point) { # compute mean depth on previous point
	    my $mean_depth=0;
	    my $nb_reads=0;
	    if (%Depth) {
		map {
		    $mean_depth+= ($_*$Depth{$_});
		    $nb_reads+= $Depth{$_};
		} keys %Depth;
		$mean_depth/=$nb_reads;
	    }
	    %Depth=();
	    $Coverage{$last_point}=$mean_depth;
	    $last_point=$point;
	}
	$Depth{$F[3]}++ unless (($Values{norepeat} && substr($Seq{$ref},$current_pos,1)=~/[a-z]/)||
				(defined($Values{mincov}) && $F[3]<$Values{mincov}));
    } # end plot
    
    # count position in the 
    if (!defined($Values{mincov}) || $F[3]>=$Values{mincov}) {
	$Stats{nb}++;
	if ($Stats{max_depth}==0) { $Stats{min_depth}=$F[3]; } 
	elsif ($Stats{min_depth} > $F[3]) { $Stats{min_depth}=$F[3]; }
	$Stats{avg_depth}+=$F[3];
	if ($Stats{max_depth} < $F[3]) { $Stats{max_depth}=$F[3]; }
    }
    
} # end while <FIN>

close(FIN);

display_cov($ref,$fname,\%Coverage,\%Repeats,\%Nzones,\%Stats,$last_pos,$last_point,\%Depth) if ($ref ne '');

printf STDOUT ("Whole genome\t%d (%.0f%%)\t%s\t%.0f\t%.0f\t%.0f\n",
	       $GlobCov{nb},
	       $GlobCov{nb}*100/$GlobCov{lg},
	       $Values{f}?sprintf("%.0f%% (N:%d)",$GlobCov{nb}*100/($GlobCov{lg}-$GlobCov{Nzone}),$GlobCov{Nzone}):'NA',
	       $GlobCov{min_depth},
	       $GlobCov{avg_depth}/$GlobCov{nb},
	       $GlobCov{max_depth});


#============================================================

=head1 NAME
    
    samCov.pl
    
=head1 SYNOPSIS
    
    samCov.pl [options] <sam|bam file_name>
    
=head1 OPTIONS
    
=over 8
    
=item B<-help>
    
    Print a brief help message and exits.
    
=item B<-man>
    
    Prints the manual page and exits.
    
=item B<-plot>
    
    Generate for each reference sequence files a log(depth+1)/log(10) coverage graph with gnuplot.
    Files generated are name according the sam|bam file and the reference sequence name ref: ref-file_name.jpeg
    
=item B<-f reference>
    
    If genome reference file is provided, a two extra tracks are displayed to show
    - N regions on the reference
    - masqued regions (lower case) on the reference
    
=item B<-S>
    
    Input is a SAM file
    
=item B<-d>
    
    Set the max per-BAM depth to avoid excessive memory usage (default 8000)
    
=item B<-width>
    
    By default the width of the graphic is log10(Lg)*1000.
    where Lg is the length of the chromozome or (end-start+1) if these parameters are used.
    This can fix the width of the graphic with this option for all reference sequences.
    
=item B<-regions>
    
    Defines references regions ([chr|chr:start|chr:start-end]) to be extracted.
    Several regions options can be specified, or even several regions delimited by comma per option,
    BUT ONLY ONE regions per reference must be provided.
    If this option is used, a bam indexed file is required.
    
=item B<-lw>
    
    Defines line width for graphics (default 1)
    
=item B<-norepeat>
    
    Do not plot in repeated regions
    
=item B<-mincov>
    
    Set the minimal depth at a position to be considered as covered
    
=back
    
=head1 INPUTS
    
=over 8
    
=item B<sam|bam file_name>
    
    The path of BAM/SAM file.
    This file must have a header line and be ordered by coordinates.
    
=back
    
=head1 DESCRIPTION
    
    Compute coverage for each reference sequence of a BAM file, or the regions specified.
    Coverage = id (id/lg) where lg is the reference length or the length of the specified reference region.
    If the reference sequences are provided, a coverage "without N" is also computed (NA otherwise).
    Minimum, average, and maximum depth are computed.
    Graphic (jpeg) can be generated, and if a reference fasta file is provided, N and lower case regions can be displayed too.
    The graphic file is generated in the current directory, named C_S-E_F.jpeg where:
    - C is the reference sequence name
    - S and E are start and end region
    - F is input file base name
    
    It is possible to gather generated graphics using ImageMagick convert tool:
    convert -append *.jpeg all_graphics.jpeg
    
=head1 AUTHORS
    
    Patrice DEHAIS
    
=head1 CONTACT
    
    Questions can be posted to the sigenae mailing list:
    sigenaesupport@jouy.inra.fr
    
=cut

#!/usr/bin/perl
#
#       addHetero.pl 
#
#       Copyright 2012 Sylvain Marthey <sylvain.marthey@jouy.inra.fr> && Jordi Estelle <jordi.estelle@jouy.inra.fr>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, see <http://www.gnu.org/licenses/>.


use strict;
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;

my ($help,$fasta_het,$check);

# on recupere les options
GetOptions("help|?" => \$help,
           "fasta_het:s" => \$fasta_het,
           "check:s" => \$check,
           )
or
pod2usage(-message=>"Try `$0' for more information.", -exitval => 2,-verbose=> 0);
pod2usage(-exitval =>1, -verbose => 2) if ($help);

$check ||= 'yes';
# on test si les parametres ont bien ete rentres
if ($fasta_het eq "" || !-f($fasta_het))
  {
     pod2usage(-message=>"--fasta_het parameter missing or is not a valid file path.\nTry `$0 --help' for more information.",
              -exitval => 2,-verbose=> 0);
  }

my %het = (M=>'AC', R=>'AG', W=>'AT', M=>'CA', S=>'CG', Y=>'CT', R=>'GA', S=>'GC', K=>'GT', W=>'TA', Y=>'TC', K=>'TG');

# reads original file file 
print STDERR "parseFasta\n";
my %new_chrs = parseFasta($fasta_het);
# retrieve heterologous locis
my %hetLocis = %{getHeterologousLocis(\%new_chrs)};

my (@temp_seq,$temp_id, %seqs);
my $i=0;
print STDERR "read STDIN\n";
while(<STDIN>){
	chomp();
	$i++;
	# si la ligne est vide on passe
	next if(!$_);
	# si on est dans le cas d'un header
	if($_ =~ /^>/){
		# si c'est le premier alors rien de special
		# sinon on ecrit la sequence precedente
		if($temp_id){
			if(scalar(@temp_seq)>0){
				print STDERR "# $temp_id\n";
				printFasta($temp_id,setHeterologousLocis(join("",@temp_seq),$hetLocis{$temp_id}));
				$seqs{$temp_id} = join("",@temp_seq);
			}else{
				die "Error in STDIN faste file line $i : no seqeunece found for ID ".$temp_id."\n";
			}
		}elsif($i > 1){
			die "probleme seq ".$temp_id.", sequence ".join("",@temp_seq)."\n   -> sequence retiree\n"; 
		}
		# on efface des donnees precedentes
		@temp_seq = ();
		$_ =~ /^>([^\s]*)\s?(.*)/;
		if($1){
			$temp_id = $1;
		}else{
			die "Error in STDIN fasta file line $i : $_\n";
		}
	# sinon c'est qu'on est dans une sequence
	}else{
		push(@temp_seq,$_);
	}
}
print STDERR "# $temp_id\n";
printFasta($temp_id,setHeterologousLocis(join("",@temp_seq),$hetLocis{$temp_id}));

sub setHeterologousLocis {
	my $seq = shift();
	my %hets = %{shift()};
	my $pb;
	foreach my $k (keys(%hets)){
		foreach my $pos (@{$hets{$k}}){
			next if($pos>=length($seq));
			# if heterologous code is compatible with observed nucleotide
			my $b = uc(substr($seq,$pos,1));
			if($b eq 'N'){
				substr($seq,$pos,1) = $k;
			}elsif($check ne 'yes'){
				substr($seq,$pos,1) = $k;
			}elsif($het{$k} =~ $b){
				substr($seq,$pos,1) = $k;
			}else{
				print STDERR "  $pos : $b -> ".$het{$k}." ($k)\n";
				$pb++;
			}
		}
		print STDERR "  $k : $pb/".scalar(@{$hets{$k}})." conflict in heterologous sites\n";
	}
	return $seq;
}

sub getHeterologousLocis {
	my %chr = %{shift()};
	
	my %hets;
	foreach my $k (keys(%chr)){
		# pour chaque nuc de la sequence
		foreach my $i(0..length($chr{$k})){
			# si c'est un heterozygote
			if($het{substr($chr{$k},$i,1)}){
				# on ajoute sa position à l'heterozygote
				push(@{$hets{$k}{substr($chr{$k},$i,1)}},$i);
			}
		}
	}
	return \%hets;
}

sub parseFasta {
	my $file  = shift();
	my (@temp_seq,$temp_id, %seqs);
	my $i=0;
	# on lit le fichier d'entree
	open (IN,$file) or die "unable to openfasta file : $file\n";
	# pour chaque ligne 
	while(<IN>){
		$i++;
		chomp();
		# si la ligne est vide on passe
		next if(!$_);
		# si on est dans le cas d'un header
		if($_ =~ /^>/){
# 			print STDERR $_."\n";
			# si c'est le premier alors rien de special
			# sinon on ecrit la sequence precedente
			if($temp_id){
				if(scalar(@temp_seq)>0){
					$seqs{$temp_id} = join("",@temp_seq);
				}else{
					die "Error in fasta file $file line $i : no seqeunece found for ID ".$temp_id."\n";
				}
			}elsif($i > 1){
				die "probleme seq ".$temp_id.", sequence ".join("",@temp_seq)."\n   -> sequence retiree\n"; 
			}
			# on efface des donnees precedentes
			@temp_seq = ();
			$_ =~ /^>([^\s]*)\s?(.*)/;
			if($1){
				$temp_id = $1;
			}else{
				die "Error in fasta file $file line $i : $_\n";
			}
		# sinon c'est qu'on est dans une sequence
		}else{
			push(@temp_seq,$_);
		}
	}
	close(IN);
	# on ecrit la derniere sequence
	if($temp_id){
# 		print STDERR $temp_id."\n";
		if(scalar(@temp_seq)>0){
			$seqs{$temp_id} = join("",@temp_seq);
		}
	}
	return %seqs;
}


sub printFasta {
	my $id = shift();
	my $sequences = shift();
	my $i = 0;
	print ">".$id."\n";
	while ($i*60+59 < length($sequences)){
		print substr($sequences,($i*60),60)."\n";
		$i++;
	}
	print substr($sequences,($i*60),(length($sequences)-($i*60)))."\n";
}


=head1 NAME

 addHetero.pl

=head1 SYNOPSIS

 STDIN | addHetero.pl --fasta_het <file_path> [--check <yes|no>]

=head1 OPTIONS

	--fasta_het	: fasta file containig hetrerologous sites
	
	--check	: check if heterologous site is compatible with observed nucleotide before replacing it [yes].
	
=over

=back

=head1 DESCRIPTION

 This script search for hetrologous sites in --fasta_het file and transferts theses sites to the input fasta file.

=cut
           
=head1 AUTHORS

Sylvain Marthey <sylvain.marthey@jouy.inra.fr>

=head1 VERSION

1.1

=head1 DATE

2012

=head1 KEYWORDS

heterologous, fasta

=head1 EXAMPLE

cat standard_ref.fasta | addHetero.pl --fasta_het my_heterologous_ref.fasta 

=cut

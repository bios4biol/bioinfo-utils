#! /usr/bin/perl
#
#       extract_genes_infos_from_Ensembl_ID.pl
#
#       Copyright 2012 Sylvain Marthey <sylvain.marthey@jouy.inra.fr>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, see <http://www.gnu.org/licenses/>.


use strict;
use DBI;
use Getopt::Long;
use Pod::Usage;

use Bio::EnsEMBL::Utils::Slice qw(split_Slices);
use Bio::EnsEMBL::Registry;


my $specie;
my $mode;
my $infile;
my $introns;
my $flanking_5;
my $db;
my $help;
my $debug;

GetOptions(	 "help|?" => \$help,
	'--file_ids=s' => \$infile,
	'--mode=s' => \$mode,
	'--introns=s' => \$introns,
	'--flanking_5=s' => \$flanking_5,
	'--specie=s'  => \$specie ,
	'--debug=s'  => \$debug ,
	'--db_version=s' => \$db)
or
pod2usage(-message=>"Try `$0' for more information.", -exitval => 2,-verbose=> 0);

pod2usage(-exitval =>1, -verbose => 2) if ($help);

if($infile && !-f$infile){
	pod2usage(-message=> "error:$infile  is not a valid file path\n",
              -exitval => 2,-verbose=> 0);
}    
if(!$specie){
	pod2usage(-message=> "error: --specie parameter wasn't define\n",
              -exitval => 2,-verbose=> 0);
}   
if(!$db){
	pod2usage(-message=> "error: --specie parameter wasn't define\n",
               -exitval => 2,-verbose=> 0);
}     
if(!$flanking_5){
	$flanking_5 = 2000;
}  



############################ PROGRAM ############################

my $registry = 'Bio::EnsEMBL::Registry';

$registry->load_registry_from_db(
    -host => 'ensembldb.ensembl.org',
    -user => 'anonymous',
    -db_version => $db
);

# create slice and transcript adaptors
my $slice_adaptor = $registry->get_adaptor( "$specie", "core", "slice" );
my $tr_adaptor    = $registry->get_adaptor( "$specie", 'Core', 'Transcript' );
my $gn_adaptor    = $registry->get_adaptor( "$specie", 'Core', 'gene' );
my %features;
 
open(IN,$infile) or die "unable to read file $infile\n";
if(uc($mode) ne "COORDINATES"){
	print "Gene_ID\tGene_Name\tHGNC\tTranscript_ID\tPR2000\tPR1000\t5UTR\t3UTR\tCDS\tINTRONS\tnbARN\tnb 5UTR\t2nd_Transcript_ID\t2ndPR2000\t2nd5UTR\n";
}
# foreach line
while(<IN>){
	chomp;
	my $id = $_;
	my $temp_seq;
	my $gene = $gn_adaptor->fetch_by_stable_id($id);
	my @dblist = @{$gene->get_all_DBLinks()};
	my $HGNC;
	foreach my $db (@dblist){
		if($db->dbname() =~ m/HGNC/){
			$HGNC = $db->display_id();
		}
	}
	my $transcripts = $tr_adaptor->fetch_all_by_Gene($gene);
	my $longest_trscpt;
	my $second_trscpt;
	my $nb_trscpt ;

	# select the longest transcript
	foreach  my $tr (@{$transcripts} ) {
		if(!$longest_trscpt || ($longest_trscpt->length()) < ($tr->length())){
			$longest_trscpt = $tr;
		}
		$nb_trscpt++;
# 		print $id ," ", $tr->display_id() , " ",  $tr->start_Exon->start() , ":" , $tr->start_Exon->end() ,"\t" ,$tr->end_Exon->start(),":",$tr->end_Exon->end(), "\n";
	}
	my $trsc_sl = $slice_adaptor->fetch_by_transcript_stable_id($longest_trscpt->stable_id() , 2e3 );
	# retrieve 5' and 3' UTR regions for this transcript (according to the transcript strand)
	my $UTR5 ;
	if($longest_trscpt->five_prime_utr){
		$UTR5 = $longest_trscpt->five_prime_utr->seq();
	}

	my @exons = @{$longest_trscpt->get_all_Exons()};
	my $nb_exs = scalar(@exons);


	my $UTR3 ;
	if($longest_trscpt->three_prime_utr){
		$UTR3 = $longest_trscpt->three_prime_utr->seq();
	}

	# retrieve 5' Flanking region for this transcript (according to the transcript strand)
	my $FL5_2000;
	if($longest_trscpt->strand()==-1){
		# retrieve gene object and 5' and 3' 2000 flanking regions
		$temp_seq = Bio::Seq->new(-seq => $trsc_sl->seq())->revcom->seq();
		$FL5_2000 = Bio::Seq->new(-seq => substr($trsc_sl->seq(),-2000))->revcom->seq();
	}else{
		$temp_seq = $trsc_sl->seq();
		$FL5_2000 = substr($trsc_sl->seq(),0,2000);
	}
	my $FL5_1000 = substr($FL5_2000,-1000);

	# create the list of CDS exons
	my $CDS ;
	my @translateable_exons = @{$longest_trscpt->get_all_translateable_Exons};
	my $nb_exs_cds = scalar(@translateable_exons);

	for ( my $i = 0; $i < $nb_exs_cds;$i++){
		$CDS = $CDS.($i+1).$translateable_exons[$i]->seq->seq();
		# add the CDS coordinnates to the feature table
	}
	# create the list of introns
	my @introns = @{$longest_trscpt->get_all_Introns()};
# 	print "nb Introns ".scalar(@introns)."\n";
	my $INTRONS ;
# 	print "Introns\n";	
	for ( my $i = 0; $i < scalar(@introns);$i++){
# 		print $introns[$i]->start , "\t" ,  $introns[$i]->end , "\n";
		$INTRONS = $INTRONS.($i+1).$introns[$i]->seq;

	}

	# put all exons in the feature table of the corresponding chromosome
	# if we retrieve introns
	if(!$introns || uc($introns) eq 'Y' || uc($introns) eq 'YES'){
		if($longest_trscpt->strand()==-1){
			push(@{$features{$gene->slice->seq_region_name}},[($exons[scalar(@exons)-1]->start()),$exons[0]->end()+$flanking_5]);
		}else{
			push(@{$features{$gene->slice->seq_region_name}},[($exons[0]->start()-$flanking_5),$exons[scalar(@exons)-1]->end()]);
		}
	# if we retrive only exons
	}else{
		for (my $i=0; $i<scalar(@exons); $i++ ){
			if($i==0){
				if($longest_trscpt->strand()==-1){
					push(@{$features{$gene->slice->seq_region_name}},[$exons[$i]->start(),$exons[$i]->end()+$flanking_5]);
				}else{
					push(@{$features{$gene->slice->seq_region_name}},[$exons[$i]->start()-$flanking_5,$exons[$i]->end()])
				}
			}else{
				push(@{$features{$gene->slice->seq_region_name}},[$exons[$i]->start(),$exons[$i]->end()]);
			}
		}
		
	}

	# select second transcript if necessary
	my $second_UTR5;
	my $second_FL5_2000;
	my @UTR5_sizes;
	if($nb_trscpt > 1){
		my $UTR5_max;
		while ( my $tr = shift @{$transcripts}) {
			# test the 5UTR_size
			if($tr->five_prime_utr){
				my $deja_vu;
				foreach my $UTR(@UTR5_sizes){
					if(length($tr->five_prime_utr->seq) == $UTR){
						$deja_vu =1;
					}
				}
				# add new utr if we never seen it before
				if(scalar(@UTR5_sizes) == 0 || $deja_vu != 1) {
					push(@UTR5_sizes,length($tr->five_prime_utr->seq));
				}
				# select the second transcript if we have a new 5UTR
				if(!$second_trscpt || (length($second_trscpt->five_prime_utr->seq) < length($tr->five_prime_utr->seq)) && ($tr->display_id ne $longest_trscpt->display_id)){
					$second_trscpt = $tr;
				}
			}
		}
		if($second_trscpt){
			my $sd_trsc_sl = $slice_adaptor->fetch_by_transcript_stable_id($second_trscpt->stable_id() , 2e3 );
			# retrieve 5' region for this transcript (according to the transcript strand)
			# retrieve 5' Flanking region for this transcript (according to the transcript strand)
			if($second_trscpt->strand()==-1){
				$second_FL5_2000 = Bio::Seq->new(-seq => substr($sd_trsc_sl->seq(),-2000))->revcom->seq();
			}else{
				$second_FL5_2000 = substr($sd_trsc_sl->seq(),0,2000);
			}
			$second_UTR5 = $second_trscpt->five_prime_utr->seq();
			my @sd_exons = @{$second_trscpt->get_all_Exons()};
			# put all sd_exons in the feature table of the corresponding chromosome
			# if we retrieve introns
			if(!$introns || uc($introns) eq 'Y' || uc($introns) eq 'YES'){
				if($second_trscpt->strand()==-1){
					push(@{$features{$gene->slice->seq_region_name}},[($sd_exons[scalar(@sd_exons)-1]->start()),$sd_exons[0]->end()+$flanking_5]);
				}else{
					push(@{$features{$gene->slice->seq_region_name}},[($sd_exons[0]->start()-$flanking_5),$sd_exons[scalar(@sd_exons)-1]->end()]);
				}
			# if we retrive only sd_exons
			}else{
				for (my $i=0; $i<scalar(@sd_exons); $i++ ){
					if($i==0){
						if($second_trscpt->strand()==-1){
							push(@{$features{$gene->slice->seq_region_name}},[$sd_exons[$i]->start(),$sd_exons[$i]->end()+$flanking_5]);
						}else{
							push(@{$features{$gene->slice->seq_region_name}},[$sd_exons[$i]->start()-$flanking_5,$sd_exons[$i]->end()])
						}
					}else{
						push(@{$features{$gene->slice->seq_region_name}},[$sd_exons[$i]->start(),$sd_exons[$i]->end()]);
					}
				}
				
			}
		}
	}elsif(length($UTR5) > 0){
		push(@UTR5_sizes,length($UTR5));
	}

	# print results
	if(lc($mode) eq "size"){
		print "$id\t";
		print $gene->external_name() , "\t";
		print "$HGNC\t";
		print $longest_trscpt->display_id() , "\t";
		print length($FL5_2000), "\t";
		print length($FL5_1000), "\t";
		print length($UTR5), "\t";
		print length($UTR3), "\t";
		print (length($CDS) - $nb_exs_cds);
		print "\t";
		print (length($INTRONS) - $nb_exs_cds +1);
		print "\t";
		print "$nb_trscpt\t";
		print scalar(@UTR5_sizes) , "\t";
		if($second_trscpt){
			print $second_trscpt->display_id() , "\t";
			print length($second_FL5_2000), "\t";
			print length($second_UTR5), "\n";
		}else{
			print "\n";
		}
	}elsif(lc($mode) ne "coordinates"){
		if(!$debug){
			print "$id\t";
			print $gene->external_name() , "\t";
			print "$HGNC\t";
			print $longest_trscpt->display_id() , "\t";
			print "$FL5_2000\t";
			print "$FL5_1000\t";
			print "$UTR5\t";
			print "$UTR3\t";
			print "$CDS\t";
			print "$INTRONS\t";
			print "$nb_trscpt\t";
			print scalar(@UTR5_sizes) , "\t";
			if($second_trscpt){
				print $second_trscpt->display_id() , "\t";
				print "$second_FL5_2000\t";
				print "$second_UTR5\n";
			}else{
				print "\n";
			}
		}else{
			print "id: $id\n";
			print "gene name : ", $gene->external_name() , "\t";
			print "HGNC : $HGNC\t";	
			print 'longest_trscpt->display_id(): ', $longest_trscpt->display_id() , "\n";
			print "FL5_2000: $FL5_2000\n";
			print "FL5_1000: $FL5_1000\n";
			print "UTR5: $UTR5\n";
			print "UTR3: $UTR3\n";
			print "CDS: $CDS\n";
			print "INTRONS: $INTRONS\n";
			print "nb_trscpt: $nb_trscpt\n";
			print 'scalar(@UTR5_sizes)' , scalar(@UTR5_sizes) , "\n";
			if($second_trscpt){
				print '$second_trscpt->display_id(): ' , $second_trscpt->display_id() , "\n";
				print "second_FL5_2000: $second_FL5_2000\n";
				print "second_UTR5: $second_UTR5\n";
			}else{
				print "\n";
			}
		}
	}
# 	exit 0;
}

if(lc($mode) eq "coordinates"){
		# add coordinates to global features array
		# FL5_2000   
# 		print "Seq + fl\t", "$temp_seq\n";
		foreach my $chr (keys(%features)){
			my @pos = @{mergeContigousPositions( 'positions' =>$features{$chr})};
			foreach my $ref_pos (@pos){
				print $chr , "\t", "capture design\t" , "target region\t" , @{$ref_pos}[0] , "\t",  @{$ref_pos}[1] , "\t", "1\t+\t.\t.\n";
			}
		}
}
close IN;

############################ FUNCTIONS ##############################

sub usage {
  exec "pod2text $0";
  exit( 1 );
}

=begin function

  Function    : mergeContigousPositions
  Description : merge all contigous positions.
  Usage       : mergeContigousPositions(	positions => table);
  Parameters  : table -> table in two dimensions, first column is the starts positions. Second columns is a stop position
  Returns     : Table in two dimensions, first column is start and second column is stop.
  Version     : v1.0

=end function

=cut

sub mergeContigousPositions {
	my %args = @_;
	my @raw_pos = @{$args{positions}};
	
	# first, we sort this array
	my @pos = sort par_pos_asc @raw_pos;
	
	# next we create the news positions
	my @fn_pos;
	my $cr_st = @{$pos[0]}[0];
	my $cr_sp = @{$pos[0]}[1];
	my $ct;
	
	# cur                   |------------------------------|
	# A               |--------------|
	# B                                               |--------------|
	# C                               |--------------|
	# D                |--------------------------------------------|
	# E       |---------|
	# F                                                         |---------|
	foreach my $t (@pos){
		# case A
		if($cr_st >= @{$t}[0] && $cr_st <= @{$t}[1] && $cr_sp >=  @{$t}[1]){
			$cr_st = @{$t}[0];
			$ct++;
		# case B;
		}elsif($cr_st <= @{$t}[0] && $cr_sp <= @{$t}[1] &&  $cr_sp >= @{$t}[0]){
			$cr_sp = @{$t}[1];
			$ct++;
		# case C
#		}elsif($cr_st <= @{$t}[0] && $cr_sp >=  @{$t}[1]){
#			next;
		# case D
		}elsif($cr_st >= @{$t}[0] && $cr_sp <=  @{$t}[1]){
			$cr_st = @{$t}[0];
			$cr_sp = @{$t}[1];
			$ct++;
		# case E
		}elsif($cr_st >=  @{$t}[1]){
			my @temp = ($cr_st,$cr_sp,$ct);
			push(@fn_pos,\@temp);
			$ct=1;
			$cr_st = @{$t}[0];
			$cr_sp = @{$t}[1];
		# case F
		}elsif($cr_sp <=  @{$t}[0]){
			my @temp = ($cr_st,$cr_sp,$ct);
			push(@fn_pos,\@temp);
			$ct=1;
			$cr_st = @{$t}[0];
			$cr_sp = @{$t}[1];
		}else{
			$ct++;
			# print @{$t}[0]."  ".@{$t}[1]." $cr_st $cr_sp NPC\n";
		}
		
	}
	my @temp = ($cr_st,$cr_sp,$ct);
	push(@fn_pos,\@temp);
	
	return \@fn_pos;
}


# tri ascendant du tableau N*2 par rapport au contenu de la premiere colonne
sub par_pos_asc {
   my @ap = @{$a};
   my @bp = @{$b};
   if($ap[0] < $bp[0]){
      return -1;
   }	elsif($ap[0] == $bp[0]){
      return 0;
   }	elsif($ap[0] > $bp[0]){
      return 1;
   }
}


=pod

=head1  NAME

extract_genes_infos_from_Ensembl_ID.pl
 
=head1  SYNOPSIS

extract_genes_infos_from_Ensembl_ID.pl --file_ids <file path> --specie <string> --db_version <int> --mode <defaut,size,coordinates> --introns <y|n> --flanking_5 <int>

=head1  OPTIONS

  --file_ids :	fle containing the Ensembl genes ID (ENSG...)

  --specie :	specie

  --db_version :	Ensembl Release (look at http://www.ensembl.org/ to find the last release number)

  --mode :	default: retrieve sequence
		size: retrieve the size of eah sequence
		coordinates: generate a gff file containing the features (exons + introns + flanking)
  --introns :	y (defaut value) include introns coordinates

  --flanking_5 : 	include N nucleotide in 5' coordiantes
   

=head1 DESCRIPTION

extract_genes_infos_from_Ensembl_ID.pl - This program uses Ensembl core API to generate the folowing sequences from a list of gene ID:
	-  transcript ID : ID of the longest transcript for this gene
	- 2000 flank 5'	: 2000 nu amount of the first exon
	- 1000 flank 5'	: 1000 nu amount of the first exon
	- 5'UTR	: first non-coding exon
	- 3' UTR	: last non-coding exon
	- cds	: list of coding exons. Each exon is preced by is number in the transcrit.
	- introns	: list of non coding exons. Each exon is preced by is number in the transcrit.
           
=head1 AUTHORS

Sylvain Marthey <sylvain.marthey@jouy.inra.fr>

=head1 VERSION

1.1

=head1 DATE

10/10/2010

=head1 KEYWORDS

Ensembl ID, Gene, Exon, transcript

=head1 EXAMPLE

extract_genes_infos_from_Ensembl_ID.pl -file_ids my_genes.txt -specie chicken -db_version 71

=cut
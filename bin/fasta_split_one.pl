#!/usr/bin/perl -w

use strict;
use Getopt::Long;
use Pod::Usage;


# Options definitions (name,format,default,required)
my @Options = ( ['help',undef,undef,0],
		['man',undef,undef,0],
	      );
# defaults values
my %Values=();
my %Required=();
map {$Values{$_->[0]}=$_->[2];$Required{$_->[0]}=$_->[3];} @Options;
# build options list
my %Options=();
map {$Options{defined($_->[1])?$_->[0].'='.$_->[1]:$_->[0]}=\$Values{$_->[0]};} @Options;
# retrieve options
GetOptions(%Options) || pod2usage(2);
pod2usage(1) if ($Values{'help'});
pod2usage(-exitstatus => 0, -verbose => 2) if ($Values{'man'});

# check required
map {pod2usage("$_ is required") if ($Required{$_}&&!defined($Values{$_}));} keys %Values;

#------------------------------------------------------------

$#ARGV==0||die"need as parameter an extention for naming generated files, ie: .tfa";

$/="\n>";

while (my $entry=<STDIN>)
  {
    chomp $entry;
    $entry=~s/^>//;
    $entry=~/(\S+)/;
    my $file_name=$1;
    open(FOUT,">$file_name$ARGV[0]") || die "Can't create file $file_name";
    print FOUT ">$entry\n";
    close(FOUT);
  }

#============================================================

=head1 NAME

fasta_split_one.pl

=head1 SYNOPSIS

fasta_split_one.pl [--help|--man] .extension < multi_fasta.fa

=head1 OPTIONS

=over 8

=item B<-help>

Print a brief help message and exits.

=item B<-man>

Prints the manual page and exits.

=back

=head1 DESCRIPTION

  Reads a multi fasta file as STDIN and split it into single entry fasta file 
  File is named using accession number of the entry and extension provided as parameter

=head1 AUTHORS

Patrice DEHAIS

=head1 CONTACT

Questions can be posted to the sigenae mailing list:
sigenaesupport@jouy.inra.fr

=cut

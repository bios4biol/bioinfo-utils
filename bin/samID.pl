#!/usr/bin/perl -w
use strict;
use Getopt::Long;
use Pod::Usage;

#============================================================
# Notes
#============================================================
# When flag says reversed sequence, then the sequence stored in the SAM is the reverse complemented one
# TAG NM is the distance = nb mismatches + nb insertions + nb deletions (gaps denoted by N are not considered)
#============================================================

#------------------------------------------------------------
# options and parameters
#------------------------------------------------------------
# Options definitions (name,format,default,required)
my @Options = ( ['help',undef,undef,0],
		['man',undef,undef,0],
		['keepNMMD',undef,undef,0],
	      );
# defaults values
my %Values=();
my %Required=();
map {$Values{$_->[0]}=$_->[2];$Required{$_->[0]}=$_->[3];} @Options;
# build options list
my %Options=();
map {$Options{defined($_->[1])?$_->[0].'='.$_->[1]:$_->[0]}=\$Values{$_->[0]};} @Options;
# retrieve options
GetOptions(%Options) || pod2usage(2);
pod2usage(1) if ($Values{'help'});
pod2usage(-exitstatus => 0, -verbose => 2) if ($Values{'man'});

# check required
map {pod2usage("$_ is required") if ($Required{$_}&&!defined($Values{$_}));} keys %Values;

# read input parameters
$#ARGV==1 || pod2usage("missing input files");

my $SAM= $ARGV[0] eq '-'?'STDIN':'SAM';
my $REF= $ARGV[1] eq '-'?'STDIN':'REF';
$SAM ne $REF || pod2usage('sam and reference should differ');

#------------------------------------------------------------
# read reference sequence and store it into a hash
#------------------------------------------------------------
my %REF=();
if ($REF ne 'STDIN') {
  open(REF,$ARGV[1]) || die "Can't open file $ARGV[1]";
}
my $ref='';
while(my $line=<$REF>) {
  chomp $line;
  if($line=~/^>(\S+)/){
    if(!exists($REF{$1})){
      $ref=$1;
      $REF{$ref}='';
    }
    else {
      die "Sequence $1 already seen";
    }
  }
  else {
    $line=~s/\s//g;
    $REF{$ref}.=uc($line);
  }
}
close(REF) if ($REF ne 'STDIN');

#------------------------------------------------------------
# read SAM file
#------------------------------------------------------------
if ($SAM ne 'STDIN') {
  open(SAM,$ARGV[0]) || die "Can't open file $ARGV[0]";
}
while(my $line=<$SAM>){
  # header line and check reference names and lengthes
  if ($line=~/^\@/) { 
    if ($line=~/^\@SQ\s+SN:(\S+)\s+LN:(\S+)/) {
      my ($SN,$LN)=($1,$2);
      exists($REF{$SN}) || die "reference sequence not found: $SN";
      my $lg=length($REF{$SN});
      $LN == $lg || die "reference sequence $SN size=$lg, see $LN in SAM";
    }
    print $line;
    next;
  }

  # split into columns
  chomp $line;
  my @F=split/\t/,$line;
  
  # unmapped sequence are just print out
  if ($F[1] & 0x4) {
    print "$line\n";
    next;
  }
  
  exists($REF{$F[2]})||die "Unknown reference sequence $F[2]";

  # build TAG index
  my %TAG=();
  for (my $i=11;$i<=$#F;$i++){
    $TAG{substr($F[$i],0,2)}=$i;
  }

  # expend copy of cigar line
  my $CIGAR=$F[5];
  $CIGAR=~s/(\d+)([^\d])/$2x$1/ge;
  # compute nb deletions D, insertions I, gaps N
  my $D=$CIGAR=~s/D/D/g;
  $D=defined($D)?$D:0;
  my $I=$CIGAR=~s/I/I/g;
  $I=defined($I)?$I:0;
  my $N=$CIGAR=~s/N/N/g;
  $N=defined($N)?$N:0;
  # CIGAR modification for alignment computation purpose
  $CIGAR=~s/N/D/g; # consider N as D
  $CIGAR=~s/[=X]/M/g; # consider X and = as M

  my $CIGAR_CONTROL='';

  # compute NM and MD if needed (if one is missing, compute both)
  if (!exists($TAG{NM}) || !exists($TAG{MD}) || !defined($Values{keepNMMD})) {
    my @SEQ=split(//,uc($F[9]));
    my @CIGAR=split(//,$CIGAR);
    my @REF=split(//,substr($REF{$F[2]},$F[3]-1,$#SEQ+1-$I-$N+$D));
    my $MD='';
    my $NM=0;
    my $deletion=0;
    my $mismatch=0;
    # check alignment for mismatches
    for (my ($c,$r,$s)=(0,0,0);$c<=$#CIGAR;$c++) {
      # deletion
      if ($CIGAR[$c] eq 'D'){
	if (!$deletion) {
	  $deletion=1;
	  $mismatch=0;
	  $MD.='^';
	}
	$MD.= exists($REF[$r])?$REF[$r]:'X'; # Sometimes bwa map read outside chromosome boundary
	$r++;
	next;
      }
      $deletion=0;
      # hard clip or paddind: not in alignment
      next if ($CIGAR[$c] =~ '[HP]');
      # soft clip and insertion: only in seq, not in ref
      if ($CIGAR[$c] =~ '[SI]'){
	$s++;
	next;
      }
      # match or mismatch
      if (exists($REF[$r]) && $REF[$r] eq $SEQ[$s]) { # match
	$MD.='=';
	$mismatch=0;
      }
      else { # mismatch
        # only one mismatch letter is allowed in regexpr MD=~/^[0=9]+(([A-Z]|\^[A-Z]+)[0-9]+)*/
	# thus if we have two consecutive mismatches, the second one must be preceded by 0 (0 match before)
      	$MD.='0' if($mismatch);
	$mismatch=1; 
	$MD.= exists($REF[$r])?$REF[$r]:'X'; # Sometimes bwa map read outside chromosome boundary
	$NM++;
      }
      $s++;
      $r++;
    }
    # compute MD NM
    $MD=~s/(\=+)/length($1)/ge;
    $NM+=$I+$D;
    if (exists($TAG{NM})){
      $F[$TAG{NM}]="NM:i:$NM";
    }
    else {
      push @F,"NM:i:$NM";
      $TAG{NM}=$#F;
    }
    if (exists($TAG{MD})){
      $F[$TAG{MD}]="MD:Z:$MD";
    }
    else {
      push @F,"MD:Z:$MD";
      $TAG{MD}=$#F;
    }
  }
  
  # compute XI
  $TAG{XI}=$#F+1 unless exists($TAG{XI});
  my $nbM=$CIGAR=~s/M/M/g;
  $nbM=defined($nbM)?$nbM:0;
  my ($distance)=($F[$TAG{NM}]=~/NM:i:(\d+)/);
  $distance=defined($distance)?$distance:0;

  # compute identity based on MD instead of CIGAR distance insertion and deletion
  # $F[$TAG{XI}]=sprintf("XI:i:%d",$nbM-$distance+$I+$D);
  my $i=0;
  map{$i+=$_}$F[$TAG{MD}]=~/(\d+)/g;
  $F[$TAG{XI}]=sprintf("XI:i:%d",$i);

  printf "%s\n",join("\t",@F);
}
close(SAM) if ($SAM ne 'STDIN');



#============================================================

=pod

=head1 NAME

samID.pl

=head1 SYNOPSIS

samID.pl [options] <aln.sam> <ref.fasta> 

=head1 OPTIONS

=over 8

=item B<-help>

Print a brief help message and exits.

=item B<-man>

Prints the manual page and exits.

=item B<-keepNMMD>

Keep NM et MD if present

=back

=head1 INPUTS

=over 8

=item B<aln.sam>

The SAM aligment file. Can be - for STDIN.

=item B<ref.fasta>

The genome reference file. Can be - for STDIN.

=back

=head1 DESCRIPTION

 Compute NM, MD, and XI fields if not present. (XI=identities=M-NM).
 
 Read reference sequences FASTA files and a SAM file, 
 and compute for each alignment the NM MD and XI tags (XI=identities).

 It's the same computation than calmd in samtools, but the whole reference genome is loaded into memory.
 That allows to have good computation speed even with unsorted SAM file (useful if you want to keed original sequence ordering).

=head1 AUTHORS

Patrice DEHAIS

=head1 CONTACT

Questions can be posted to the sigenae mailing list:
sigenaesupport@jouy.inra.fr

=head1 DATE

2013

= cut


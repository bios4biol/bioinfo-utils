#!/usr/bin/perl 

use strict;
use Getopt::Long;
use Pod::Usage;
use Bio::SearchIO;


=pod

=head1 NAME

parser_blast1.pl

=head1 SYNOPSIS

parser_blast.pl -blast <filename> > outputfile

=head1 DESCRIPTION

This script takes as input a file blast. It retains only the name of the sequense the best hit, its description,hsp length, bits,evalue, the length of the hit,num_identical,num_conserved and if there are gaps.

=head1 AUTHORS

Barrilliot K.

=head1 VERSION 

1

=head1 DATE

01/2013

=head1 KEYWORDS

parser blast

=cut

# this is the input file name
my $blast_output2parse;
GetOptions( "blast=s" => \$blast_output2parse );


# these constants go into the blast_search database fields
my $blast_kind;

# verbose thingy
my $verbose;
my $help;


print "getting started... \n" . "  input:  $blast_output2parse"
 if $verbose;

## configure input
#
my $in = new Bio::SearchIO(
 -format => 'blast',
 -file   => $blast_output2parse
);

 
print "query_name	name	description	hsp_length	bits	evalue		length('hit')	num_identical	num_conserved	gaps	from\n";

# $result is a Bio::Search::Result::ResultI compliant object
while ( my $result = $in->next_result ) {

 #print STDERR "processing Query " . $result->query_name . "\n";
FOO: {
  # $hit is a Bio::Search::Hit::HitI compliant object, iterate over these
  while ( my $hit = $result->next_hit ) {

   # $hsp is a Bio::Search::HSP::HSPI compliant object
   while ( my $hsp = $hit->next_hsp ) {

    # check if we are above the threshold

    print $result->query_name . "\t"
      . $hit->name . "\t"
      . $hit->description . "\t"
      . $hsp->hsp_length . "\t"
      . $hsp->bits . "\t"
      . $hsp->evalue ."\t"
      . $hsp->length('hit') ."\t"
      . $hsp->num_identical ."\t"
      . $hsp->num_conserved ."\t"
      . $hsp->gaps . "\t";

    last FOO;
   }
  }

 }

}




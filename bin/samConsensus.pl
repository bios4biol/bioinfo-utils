#!/usr/bin/perl -w
use strict;
use Getopt::Long;
use Pod::Usage;

#------------------------------------------------------------
# return IUPAC code
sub IUPAC ( @ ) {
  my @Bases=@_;
  my $alleles=join('',sort{$a cmp $b}@Bases);
  if (length($alleles)==1) {return $alleles;}
  if($alleles eq 'AG') {return 'R';}
  if($alleles eq 'CT') {return 'Y';}
  if($alleles eq 'CG') {return 'S';}
  if($alleles eq 'AT') {return 'W';}
  if($alleles eq 'GT') {return 'K';}
  if($alleles eq 'AC') {return 'M';}
  if($alleles eq 'CGT') {return 'B';}
  if($alleles eq 'AGT') {return 'D';}
  if($alleles eq 'ACT') {return 'H';}
  if($alleles eq 'ACG') {return 'V';}
  return 'N';
}

#------------------------------------------------------------
# options and parameters
#------------------------------------------------------------
# Options definitions (name,format,default,required)
my @Options = ( ['help',undef,undef,0],
		['man',undef,undef,0],
		['noIUPAC',undef,undef,0],
	      );
# defaults values
my %Values=();
my %Required=();
map {$Values{$_->[0]}=$_->[2];$Required{$_->[0]}=$_->[3];} @Options;
# build options list
my %Options=();
map {$Options{defined($_->[1])?$_->[0].'='.$_->[1]:$_->[0]}=\$Values{$_->[0]};} @Options;
# retrieve options
GetOptions(%Options) || pod2usage(2);
pod2usage(1) if ($Values{'help'});
pod2usage(-exitstatus => 0, -verbose => 2) if ($Values{'man'});

# check required
map {pod2usage("$_ is required") if ($Required{$_}&&!defined($Values{$_}));} keys %Values;

# read input parameters
$#ARGV==0 || pod2usage("missing reference file as parameter");

#------------------------------------------------------------
# read reference sequence and store it into a hash
#------------------------------------------------------------
my %REF=();
open(REF,$ARGV[0]) || die "Can't open file $ARGV[0]";

my $ref='';
while(my $line=<REF>) {
  chomp $line;
  if($line=~/^>(\S+)/){
    if(!exists($REF{$1})){
      $ref=$1;
      $REF{$ref}='';
    }
    else {
      die "Sequence $1 already seen";
    }
  }
  else {
    $line=~s/\s//g;
    $REF{$ref}.=uc($line);
  }
}
close(REF);

#------------------------------------------------------------
# read mpileup output
#------------------------------------------------------------

$ref='';
my $refseq='';
my $last_base=0;
srand(444); # initialize rand seed to have same consensus with same input data

while (my $line=<STDIN>) {
  chomp $line;
  my @F=split/\s+/,$line;
  
  if ($F[0] ne $ref) { # next reference
    if ($ref ne '') { # print modified reference
      $refseq.=lc(substr($REF{$ref},$last_base)); # add remaining of reference sequence
      $refseq=~s/(.{60})/$1\n/g; # split sequence into smaller lines
      chomp $refseq;
      print ">$ref\n$refseq\n";
    }
    $ref=$F[0];
    exists($REF{$ref})||die "unknown reference $ref";
    $refseq='';
    $last_base=0;
  }
  
  # gather seen alleles
  my $alleles='';
  for (my $i=4;$i<$#F;$i+=3) {
    $alleles.=uc($F[$i]);
  }
  
  # compute consensus base
  my @B=split//,$alleles;
  my %Allele=();
  map {$Allele{$_}=0;} qw ( A T G C N ); # DO NOT CONSIDER IN/DEL
  for (my $i=0;$i<=$#B;$i++) {
    my $b=$B[$i];
    if ($b eq '.' || $b eq ',') { $b=uc($F[2]); }
    
    $Allele{$b}++ if (exists($Allele{$b}));
    
    if ($B[$i] eq '^') {$i++;} 
    elsif ($B[$i] eq '+' || $B[$i] eq '-'){ # handle insertion and long deletion
      my ($l)=(substr($alleles,$i+1)=~/^(\d+)/);
      $i+=length($l)+$l;
    }
  }
  my @Allele=sort{$Allele{$b}<=>$Allele{$a}} keys %Allele;
  my $base;
  # several alleles => keep major alleles. If no allele seen, all count are 0, thus N consensus is reported
  my $i=0;
  while ($i<$#Allele) {
    last if ($Allele{$Allele[$i]}!=$Allele{$Allele[$i+1]});
    $i++;
  }
  # if IUPAC then use ambiguity code, else choose random based from these having same highest occurrence.
  $base=$Values{noIUPAC}?$Allele[int(rand($i+1))]:IUPAC(@Allele[0..$i]);

  $refseq.=lc(substr($REF{$ref},$last_base,$F[1]-$last_base-1)); # add uncovered ref part
  $last_base=$F[1];
  $refseq.= substr($REF{$ref},$F[1]-1,1)eq $base ? lc($base) : $base;
}

if ($ref ne '') { # print modified reference
  $refseq.=lc(substr($REF{$ref},$last_base)); # add remaining of reference sequence
  $refseq=~s/(.{60})/$1\n/g;
  chomp $refseq;
  print ">$ref\n$refseq\n";
}

#============================================================

=head1 NAME

samConsensus.pl - replace bases of reference sequences with consensus one based on alleles seen in mpileup output

=head1 SYNOPSIS

samConsensus.pl [options] refseq.fa < mpileup.out

=head1 OPTIONS

=over 4

=item B<-help>

Print a brief help message and exits.

=item B<-man>

Prints the manual page and exits.

=item B<-noIUPAC>

Replace IUPAC base consensus by one of the most numerous allele

=back

=head1 DESCRIPTION

Print as STDOUT the references sequences whith based modified to be the consensus of alleles seen in the mpuleup output

=head1 AUTHORS

Patrice DEHAIS

=head1 VERSION

1

=head1 DATE

06/2012

=head1 CONTACT

Questions can be posted to the sigenae mailing list:
sigenaesupport@jouy.inra.fr

=cut


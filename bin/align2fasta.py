#!/usr/local/bioinfo/bin/python2.5

#
# align2fasta : Copyright (C) 2009 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


from optparse import *
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
import os, sys, gzip, bz2, string, re

__name__ = "align2fasta.py"
__synopsis__ = "align2fasta.py -i input.align -o output.fasta"
__date__ = "05/2012"
__authors__ = "Celine Noirot"
__email__ = 'support.genopole@toulouse.inra.fr'
__keywords__ = "cap3 align convert multifasta"
__description__ = "Convert a cap3 output file (.align zipped, gz or bz2 or not) to an align multifasta file \n"
__copyright__ = 'Copyright (C) 2009 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'


def version_string ():
    """
    Return the align2fasta version
    """
    return "align2fasta " + __version__


##### MAIN ####

LINE_LEN=60
parser = OptionParser(usage="Usage: align2fasta.py -i FILE [options]")
usage = "usage: %prog -i align_file -o fasta_file -a "
desc = " Convert a cap3 output file (.align) to an multifasta file (could be also align with option -a) \n"
       
parser = OptionParser(usage = usage, version = version_string(), description = desc)
igroup = OptionGroup(parser, "Input files options","")
igroup.add_option("-i", "--in", dest="input_file",
                  help="The align file : output of cap3", metavar="FILE")
igroup.add_option("-a", "--align", dest="align", default=False, action="store_true",
                  help="align the fasta file")
parser.add_option_group(igroup)
ogroup = OptionGroup(parser, "Output files options","")
ogroup.add_option("-o", "--output", dest="fasta",
                  help="The output fasta file", metavar="FILE")
parser.add_option_group(ogroup)
(options, args) = parser.parse_args()
if options.input_file == None :
    parser.print_help()
    sys.exit(1)

#initialise output filename
if options.fasta == None:
    fasta = options.input_file + ".fasta"
else : 
    fasta = options.fasta
seqs = []
f= None
if options.align == None:
    options.align=False

#read zipped input 
if options.input_file.endswith(".gz"):
    f=gzip.open(options.input_file, "r")
elif options.input_file.endswith(".bz2") :
    f=bz2.BZ2File(options.input_file, "r")
else :
    f=open (options.input_file, "r")

contig=""
hash_seq={}
i=0

# use to add as '-' as align line where it wasn't
align_index=0
for line in f.readlines():
    if (line.startswith("consensus")):
        align_index=align_index+1
    if (re.match("^[^\s]+\s+[^\s]+\s*$", line)):
        try :
            m = re.search("^([^\s]+)\s+([^\s]+)\s*$", line)
            id  = m.group(1)
            seq = m.group(2)
        except :
           print "erreur expression reg" 
           id = None
           seq= None
        if (hash_seq.has_key(str(id))) :
            if not( options.align ):
                seq=seq.replace("-","")
            hash_seq[str(id)]+=str(seq)
        else:
        #ajout des "-" pour l'alignement dans le multifasta 
            if len(seq) <= LINE_LEN :
                if options.align :
                    seq="-"*LINE_LEN*align_index+"-"*(LINE_LEN-len(seq))+seq
                else: 
                    seq=seq.replace("-","")
           	hash_seq[str(id)]=str(seq)
f.close()
seqs = []
# creation des objets Sequence
for my_id in hash_seq:
	if my_id != "consensus":
        	seqs.append(SeqRecord(Seq( hash_seq[my_id] ), id=str(my_id)))

# write down seqs
output_handle = open(fasta, "w")
SeqIO.write(seqs, output_handle, "fasta")
output_handle.close()
sys.exit(0)
        


        

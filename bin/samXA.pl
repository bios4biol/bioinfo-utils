#!/usr/bin/perl -w
use strict;
use Getopt::Long;
use Pod::Usage;

#------------------------------------------------------------
# Display alignment line and remove extra XA
sub display_aln ( * * )
  {
    my ($prev_seq_ptr,$prev_xa_ptr) =@_;

    foreach my $fragment (keys %{$prev_xa_ptr}) {
      my @F=@{$prev_seq_ptr->{$fragment}};
      my %TAG=();
      # build index of tags
      for (my $i=11;$i<=$#F;$i++){
	$TAG{substr($F[$i],0,2)}=$i;
      }
      if (exists($TAG{XA})) {
	my ($XA)=($F[$TAG{XA}]=~/^XA:Z:(.+)$/);
	my %XA=(); # non redundant XA
	foreach my $xa (split/;/,$XA) {
	  my ($chr,$pos,$cigar,$NM)=split/,/,$xa;
	  # skip XA which is equal to main location
	  next if($chr eq $F[2] && 
		  abs($pos) == $F[3] && 
		  $cigar eq $F[5] &&
		  $NM == substr($F[$TAG{NM}],5) &&
		  substr($pos,0,1) eq ($F[1]&0x10?'-':'+'));
	  $XA{$xa}=undef;
	}
	if(%XA) {
	  $F[$TAG{XA}]=sprintf("XA:Z:%s;",join(';',keys %XA));
	}
	else { # no more XA, remove tag
	  @F=(@F[0..$TAG{XA}-1],@F[$TAG{XA}+1..$#F]);
	}
      }
      printf "%s\n",join("\t",@F);
    } 
  }

#============================================================

# Options definitions (name,format,default,required)
my @Options = ( ['help',undef,undef,0],
		['man',undef,undef,0],
	      );
# defaults values
my %Values=();
my %Required=();
map {$Values{$_->[0]}=$_->[2];$Required{$_->[0]}=$_->[3];} @Options;
# build options list
my %Options=();
map {$Options{defined($_->[1])?$_->[0].'='.$_->[1]:$_->[0]}=\$Values{$_->[0]};} @Options;
# retrieve options
GetOptions(%Options) || pod2usage(2);
pod2usage(1) if ($Values{'help'});
pod2usage(-exitstatus => 0, -verbose => 2) if ($Values{'man'});

# check required
map {pod2usage("$_ is required") if ($Required{$_}&&!defined($Values{$_}));} keys %Values;

# check file.sam
my $sam='SAM';
$#ARGV==0 || die "Need file.sam as parameter";
if ($ARGV[0] eq '-') {
  $sam='STDIN';
}
else {
  open(SAM,$ARGV[0])||die "Can't open file $ARGV[0]";
}

# read SAM file
my %prev_seq=(qname=>'');
my %prev_xa=();
while (my $line=<$sam>) {
  # skip header lines
  if ($line=~/^@/) {
    print $line;
    next;
  }

  # skip blank lines
  next if ($line=~/^\s*$/);

  chomp $line;
  my @F=split/\t/,$line;
  
  # print unmapped sequences
  if ($F[1]&0x4) {
    print "$line\n";
    next;
  }
  
  # build TAG index
  my %TAG=();
  for (my $i=11;$i<=$#F;$i++){
    $TAG{substr($F[$i],0,2)}=$i;
  }
  
  # check required NM TAG
  exists($TAG{NM}) || die "No NM TAG in SAM alignment line";
  
  my $fragment = $F[1] & 0x40;
  if ($prev_seq{qname} ne $F[0]) { # new sequence
    display_aln(\%prev_seq,\%prev_xa) if ($prev_seq{qname} ne '');
    %prev_seq=(qname=>$F[0]);
    @{$prev_seq{$fragment}} = @F;
    %prev_xa=($fragment=>$TAG{XA});
    next;
  }

  if (exists($prev_seq{$fragment})) {
    my $XA=sprintf("%s,%s%s,%s,%s;",$F[2],$F[1]&0x10?'-':'+',$F[3],$F[5],substr($F[$TAG{NM}],5));
    if (!defined($prev_xa{$fragment})) {
      $prev_xa{$fragment}=scalar @{$prev_seq{$fragment}};
      $prev_seq{$fragment}[$prev_xa{$fragment}]='XA:Z:';
    }
    if (exists($TAG{XA})) {
      $prev_seq{$fragment}[$prev_xa{$fragment}].=$XA.substr($F[$TAG{XA}],5);
    }
    else {
      $prev_seq{$fragment}[$prev_xa{$fragment}].=$XA;
    }
  }
  else {
    @{$prev_seq{$fragment}} = @F;
    $prev_xa{$fragment}=$TAG{XA};
  }
}
close(SAM) if ($ARGV[0] ne '-');
if (%prev_seq) {
  display_aln(\%prev_seq,\%prev_xa) if ($prev_seq{qname} ne '');
}

#============================================================
=pod

=head1 NAME

samXA.pl 

=head1 SYNOPSIS

samXA.pl [options] file.sam

=head1 OPTIONS

=over 8

=item B<-help>

Print a brief help message and exits.

=item B<-man>

Prints the manual page and exits.

=back

=head1 INPUTS

=over 8

=item B<file.sam>

SAM file order by query name (can be - for STDIN)

=back

=head1 DESCRIPTION

Read a SAM file and gather multi location SAM lines into a unique alignment line with XA tag.
The SAM file must be sorted on query name and have NM TAG.

=head1 AUTHORS

Patrice DEHAIS

=head1 CONTACT

Questions can be posted to the sigenae mailing list:
sigenaesupport@jouy.inra.fr

=cut
